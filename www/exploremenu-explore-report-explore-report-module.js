(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["exploremenu-explore-report-explore-report-module"],{

/***/ "./src/app/page/mainmenu/exploremenu/explore-report/explore-report.module.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/page/mainmenu/exploremenu/explore-report/explore-report.module.ts ***!
  \***********************************************************************************/
/*! exports provided: ExploreReportPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExploreReportPageModule", function() { return ExploreReportPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");
/* harmony import */ var _explore_report_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./explore-report.page */ "./src/app/page/mainmenu/exploremenu/explore-report/explore-report.page.ts");








var routes = [
    {
        path: '',
        component: _explore_report_page__WEBPACK_IMPORTED_MODULE_7__["ExploreReportPage"]
    }
];
var ExploreReportPageModule = /** @class */ (function () {
    function ExploreReportPageModule() {
    }
    ExploreReportPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_6__["PipesModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_explore_report_page__WEBPACK_IMPORTED_MODULE_7__["ExploreReportPage"]]
        })
    ], ExploreReportPageModule);
    return ExploreReportPageModule;
}());



/***/ }),

/***/ "./src/app/page/mainmenu/exploremenu/explore-report/explore-report.page.html":
/*!***********************************************************************************!*\
  !*** ./src/app/page/mainmenu/exploremenu/explore-report/explore-report.page.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button color=\"navback\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>{{locationName}}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n\n  <ion-item>\n    <ion-label>Predicted Wave Size</ion-label>\n    <ion-input type=\"text\" disabled value=\"{{reportWaveSize | round: false}}ft\"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label>Predicted Wave Quality</ion-label>\n    <ion-input type=\"text\" disabled [value]=\"waveQualities[reportWaveQuality]\"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-icon slot=\"start\" name=\"camera\" (click)=\"onClickCameraIcon()\"></ion-icon>\n    <ion-textarea [(ngModel)]=\"reportTxt\" placeholder=\"Comment on surf conditions\"></ion-textarea>\n  </ion-item>\n\n  <div *ngIf=\"postFile!=''\" class=\"explore-report-postimg\">\n    <ng-container *ngIf=\"fileType==0; else videoContainer\">\n      <img src=\"{{postFile}}\" />\n    </ng-container>\n    <ng-template #videoContainer>\n      <!-- <p (click)=\"onClickPlayVideo()\">{{videoFile.name}}</p> -->\n      <p>{{videoFile.name}}</p>\n    </ng-template>\n  </div>\n\n  <div id=\"explore-report-btnpost\">\n    <ion-button (click)=\"postReport()\">Post</ion-button>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/page/mainmenu/exploremenu/explore-report/explore-report.page.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/page/mainmenu/exploremenu/explore-report/explore-report.page.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-header {\n  --ion-background-color: #84A1B3; }\n\nion-toolbar {\n  --background: #84A1B3; }\n\nion-toolbar p {\n    color: #CAE9F7;\n    font-weight: bold;\n    padding-right: 5px;\n    position: relative; }\n\nion-title {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  color: #CAE9F7;\n  font-weight: bold; }\n\n#explore-report-waveheight,\n#explore-report-wavequality {\n  display: flex;\n  padding: 0 10vw; }\n\n#explore-report-waveheight div,\n  #explore-report-wavequality div {\n    flex: 1; }\n\n#explore-report-waveheight > div:nth-child(2) > p,\n#explore-report-wavequality > div:nth-child(2) > p {\n  color: white; }\n\n#explore-report-wavequality {\n  margin-top: 2vh; }\n\n#explore-report-waveheight > div:first-child,\n#explore-report-wavequality > div:first-child {\n  margin-left: 20px; }\n\n#explore-report-waveheight > div:nth-child(2),\n#explore-report-wavequality > div:nth-child(2) {\n  background-color: #77a0b3;\n  border-radius: 6px;\n  margin: 10px; }\n\n#explore-report-waveheight > div:nth-child(2) p,\n  #explore-report-wavequality > div:nth-child(2) p {\n    margin: 5px;\n    text-align: center; }\n\n#explore-report-btnpost {\n  text-align: center;\n  margin-top: 3vh; }\n\n.explore-report-postimg {\n  margin-top: 2vh;\n  width: 100%;\n  height: 80px;\n  overflow: hidden;\n  text-align: center; }\n\n.explore-report-postimg img {\n    width: 80px;\n    min-height: 100%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2xva2VzaC9saW5ldXAgc291cmNlKDA5LTMwKSAoMikvbGluZXVwKDA5LTMwKS9zcmMvYXBwL3BhZ2UvbWFpbm1lbnUvZXhwbG9yZW1lbnUvZXhwbG9yZS1yZXBvcnQvZXhwbG9yZS1yZXBvcnQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksK0JBQXVCLEVBQUE7O0FBRzNCO0VBQ0kscUJBQWEsRUFBQTs7QUFEakI7SUFHUSxjQUFjO0lBQ2QsaUJBQWlCO0lBQ2pCLGtCQUFrQjtJQUNsQixrQkFBa0IsRUFBQTs7QUFJMUI7RUFDSSxrQkFBa0I7RUFDbEIsUUFBUTtFQUNSLFNBQVM7RUFDVCxnQ0FBZ0M7RUFDaEMsY0FBYztFQUNkLGlCQUFpQixFQUFBOztBQUdyQjs7RUFFSSxhQUFhO0VBQ2IsZUFBZSxFQUFBOztBQUhuQjs7SUFNUSxPQUFPLEVBQUE7O0FBSWY7O0VBRUksWUFBWSxFQUFBOztBQUdoQjtFQUNJLGVBQWUsRUFBQTs7QUFHbkI7O0VBRUksaUJBQWlCLEVBQUE7O0FBR3JCOztFQUVJLHlCQUF5QjtFQUN6QixrQkFBa0I7RUFDbEIsWUFBWSxFQUFBOztBQUpoQjs7SUFPUSxXQUFXO0lBQ1gsa0JBQWtCLEVBQUE7O0FBSTFCO0VBQ0ksa0JBQWtCO0VBQ2xCLGVBQWUsRUFBQTs7QUFHbkI7RUFDSSxlQUFlO0VBQ2YsV0FBVztFQUNYLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsa0JBQWtCLEVBQUE7O0FBTHRCO0lBUVEsV0FBVztJQUNYLGdCQUFnQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZS9tYWlubWVudS9leHBsb3JlbWVudS9leHBsb3JlLXJlcG9ydC9leHBsb3JlLXJlcG9ydC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taGVhZGVyIHtcbiAgICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjODRBMUIzO1xufVxuXG5pb24tdG9vbGJhciB7XG4gICAgLS1iYWNrZ3JvdW5kOiAjODRBMUIzO1xuICAgIHAge1xuICAgICAgICBjb2xvcjogI0NBRTlGNztcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIH1cbn1cblxuaW9uLXRpdGxlIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiA1MCU7XG4gICAgbGVmdDogNTAlO1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuICAgIGNvbG9yOiAjQ0FFOUY3O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4jZXhwbG9yZS1yZXBvcnQtd2F2ZWhlaWdodCxcbiNleHBsb3JlLXJlcG9ydC13YXZlcXVhbGl0eSB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBwYWRkaW5nOiAwIDEwdnc7XG5cbiAgICBkaXYge1xuICAgICAgICBmbGV4OiAxO1xuICAgIH1cbn1cblxuI2V4cGxvcmUtcmVwb3J0LXdhdmVoZWlnaHQ+ZGl2Om50aC1jaGlsZCgyKT5wLFxuI2V4cGxvcmUtcmVwb3J0LXdhdmVxdWFsaXR5PmRpdjpudGgtY2hpbGQoMik+cCB7XG4gICAgY29sb3I6IHdoaXRlO1xufVxuXG4jZXhwbG9yZS1yZXBvcnQtd2F2ZXF1YWxpdHkge1xuICAgIG1hcmdpbi10b3A6IDJ2aDtcbn1cblxuI2V4cGxvcmUtcmVwb3J0LXdhdmVoZWlnaHQ+ZGl2OmZpcnN0LWNoaWxkLFxuI2V4cGxvcmUtcmVwb3J0LXdhdmVxdWFsaXR5PmRpdjpmaXJzdC1jaGlsZCB7XG4gICAgbWFyZ2luLWxlZnQ6IDIwcHg7XG59XG5cbiNleHBsb3JlLXJlcG9ydC13YXZlaGVpZ2h0PmRpdjpudGgtY2hpbGQoMiksXG4jZXhwbG9yZS1yZXBvcnQtd2F2ZXF1YWxpdHk+ZGl2Om50aC1jaGlsZCgyKSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzc3YTBiMztcbiAgICBib3JkZXItcmFkaXVzOiA2cHg7XG4gICAgbWFyZ2luOiAxMHB4O1xuXG4gICAgcCB7XG4gICAgICAgIG1hcmdpbjogNXB4O1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxufVxuXG4jZXhwbG9yZS1yZXBvcnQtYnRucG9zdCB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbi10b3A6IDN2aDtcbn1cblxuLmV4cGxvcmUtcmVwb3J0LXBvc3RpbWcge1xuICAgIG1hcmdpbi10b3A6IDJ2aDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDgwcHg7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG5cbiAgICBpbWcge1xuICAgICAgICB3aWR0aDogODBweDtcbiAgICAgICAgbWluLWhlaWdodDogMTAwJTtcbiAgICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/page/mainmenu/exploremenu/explore-report/explore-report.page.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/page/mainmenu/exploremenu/explore-report/explore-report.page.ts ***!
  \*********************************************************************************/
/*! exports provided: ExploreReportPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExploreReportPage", function() { return ExploreReportPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/ngx/index.js");
/* harmony import */ var _ionic_native_media_capture_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/media-capture/ngx */ "./node_modules/@ionic-native/media-capture/ngx/index.js");
/* harmony import */ var _ionic_native_media_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/media/ngx */ "./node_modules/@ionic-native/media/ngx/index.js");
/* harmony import */ var _ionic_native_streaming_media_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/streaming-media/ngx */ "./node_modules/@ionic-native/streaming-media/ngx/index.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _services_firebase_service_firebase_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../services/firebase-service/firebase.service */ "./src/app/services/firebase-service/firebase.service.ts");
/* harmony import */ var src_app_constants_constants__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! src/app/constants/constants */ "./src/app/constants/constants.ts");
/* harmony import */ var src_app_services_showtoast_showtoast_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! src/app/services/showtoast/showtoast.service */ "./src/app/services/showtoast/showtoast.service.ts");
/* harmony import */ var src_app_services_myutils_myutils_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! src/app/services/myutils/myutils.service */ "./src/app/services/myutils/myutils.service.ts");
/* harmony import */ var _ionic_native_video_editor_ngx__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic-native/video-editor/ngx */ "./node_modules/@ionic-native/video-editor/ngx/index.js");















var ExploreReportPage = /** @class */ (function () {
    function ExploreReportPage(route, navCtrl, firebaseService, myUtils, loadingCtrl, actionSheetCtrl, toastService, file, camera, mediaCapture, media, streamingMedia, changeDetector, videoEditor) {
        this.route = route;
        this.navCtrl = navCtrl;
        this.firebaseService = firebaseService;
        this.myUtils = myUtils;
        this.loadingCtrl = loadingCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.toastService = toastService;
        this.file = file;
        this.camera = camera;
        this.mediaCapture = mediaCapture;
        this.media = media;
        this.streamingMedia = streamingMedia;
        this.changeDetector = changeDetector;
        this.videoEditor = videoEditor;
        this.currentUserInfo = {};
        this.waveQualities = src_app_constants_constants__WEBPACK_IMPORTED_MODULE_11__["WAVEQUALITIES"];
        this.reportWaveQuality = '';
        this.reportWaveSize = '';
        this.reportTxt = '';
        this.postFile = '';
        this.postFile2 = '';
        /**
           * type
           *  0: Image
           *  1: Video
           */
        this.fileType = 0;
        this.videoFile = {
            name: '',
            path: ''
        };
    }
    ExploreReportPage.prototype.ngOnInit = function () {
        this.locationId = this.route.snapshot.paramMap.get('locationid');
        this.locationName = this.route.snapshot.paramMap.get('name');
        this.reportWaveSize = (Number(this.route.snapshot.paramMap.get('wavesize')) * src_app_constants_constants__WEBPACK_IMPORTED_MODULE_11__["MTOFRATIO"]).toString();
        this.reportWaveQuality = this.route.snapshot.paramMap.get('wavequality');
        this.initPage();
    };
    ExploreReportPage.prototype.initPage = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var pageLoader, profileInfoSnapshot, err_1;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Loading...'
                        })];
                    case 1:
                        pageLoader = _a.sent();
                        return [4 /*yield*/, pageLoader.present()];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        _a.trys.push([3, 5, , 6]);
                        return [4 /*yield*/, this.firebaseService.getUserProfile(this.firebaseService.getUID())];
                    case 4:
                        profileInfoSnapshot = _a.sent();
                        console.log(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, profileInfoSnapshot.val()));
                        this.currentUserInfo = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, profileInfoSnapshot.val());
                        pageLoader.dismiss();
                        return [3 /*break*/, 6];
                    case 5:
                        err_1 = _a.sent();
                        pageLoader.dismiss();
                        console.log(err_1);
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    ExploreReportPage.prototype.postReport = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var reportData, reportLoader, err_2;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        reportData = {
                            id: '',
                            uid: this.currentUserInfo.uid,
                            username: this.currentUserInfo.fname + ' ' + this.currentUserInfo.lname,
                            reporttxt: this.reportTxt,
                            location: {
                                lat: this.myUtils.currentUserLocation.lat,
                                long: this.myUtils.currentUserLocation.long,
                                name: this.locationName,
                            },
                            locationId: this.locationId,
                            reporterLoc: {
                                lat: this.myUtils.currentUserLocation.lat,
                                long: this.myUtils.currentUserLocation.long
                            },
                            waveHeight: this.reportWaveSize,
                            waveQuality: this.reportWaveQuality,
                            createdat: firebase_app__WEBPACK_IMPORTED_MODULE_4__["database"].ServerValue.TIMESTAMP,
                            postfile: {
                                data: '',
                                type: this.fileType,
                                thumb: ''
                            },
                            review: 0
                        };
                        console.log(reportData);
                        return [4 /*yield*/, this.loadingCtrl.create({
                                message: 'Please wait...'
                            })];
                    case 1:
                        reportLoader = _a.sent();
                        return [4 /*yield*/, reportLoader.present()];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        _a.trys.push([3, 5, , 6]);
                        return [4 /*yield*/, this.firebaseService.setWeatherReport(this.postFile, reportData, this.postFile2)];
                    case 4:
                        _a.sent();
                        this.postFile = '';
                        reportLoader.dismiss();
                        this.toastService.showToast('Report is posted');
                        this.navCtrl.pop();
                        return [3 /*break*/, 6];
                    case 5:
                        err_2 = _a.sent();
                        reportLoader.dismiss();
                        this.toastService.showToast(err_2.message);
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    ExploreReportPage.prototype.onClickCameraIcon = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var actionSheet;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.fileType = 0;
                        return [4 /*yield*/, this.actionSheetCtrl.create({
                                header: 'File',
                                buttons: [{
                                        text: 'Take Picture',
                                        icon: 'aperture',
                                        handler: function () {
                                            _this.fileType = 0;
                                            _this.takePicture();
                                        }
                                    }, {
                                        text: 'Record Video',
                                        icon: 'videocam',
                                        handler: function () {
                                            _this.fileType = 1;
                                            _this.videoFile = {
                                                name: '',
                                                path: ''
                                            };
                                            _this.captureVideo();
                                        }
                                    }, {
                                        text: 'Cancel',
                                        icon: 'close',
                                        role: 'cancel',
                                        handler: function () {
                                            console.log('Cancel clicked');
                                        }
                                    }]
                            })];
                    case 1:
                        actionSheet = _a.sent();
                        return [4 /*yield*/, actionSheet.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ExploreReportPage.prototype.takePicture = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var options, imageData, err_3;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        options = {
                            quality: 50,
                            destinationType: this.camera.DestinationType.DATA_URL,
                            encodingType: this.camera.EncodingType.JPEG,
                            mediaType: this.camera.MediaType.PICTURE,
                            correctOrientation: true
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.camera.getPicture(options)];
                    case 2:
                        imageData = _a.sent();
                        this.postFile = 'data:image/jpeg;base64,' + imageData;
                        return [3 /*break*/, 4];
                    case 3:
                        err_3 = _a.sent();
                        console.log(err_3);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ExploreReportPage.prototype.captureVideo = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var videoLoader, captureSuccess, captureError, err_4;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Loading...'
                        })];
                    case 1:
                        videoLoader = _a.sent();
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 3, , 5]);
                        captureSuccess = function (mediaFiles) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                            var i, path, len, capturedFile, _a, vid, creathumb;
                            var _this = this;
                            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        i = 0, len = mediaFiles.length;
                                        _b.label = 1;
                                    case 1:
                                        if (!(i < len)) return [3 /*break*/, 6];
                                        path = mediaFiles[i].fullPath;
                                        //   const capturedOptions: CaptureVideoOptions = {
                                        //     limit: 1,
                                        //     quality: 0
                                        //   };
                                        //   const res = await this.mediaCapture.captureVideo(capturedOptions);
                                        return [4 /*yield*/, videoLoader.present()];
                                    case 2:
                                        //   const capturedOptions: CaptureVideoOptions = {
                                        //     limit: 1,
                                        //     quality: 0
                                        //   };
                                        //   const res = await this.mediaCapture.captureVideo(capturedOptions);
                                        _b.sent();
                                        capturedFile = path;
                                        console.log(path);
                                        console.log(capturedFile.substring(capturedFile.lastIndexOf("/") + 1));
                                        console.log(capturedFile.substring(0, capturedFile.lastIndexOf("/")));
                                        this.videoFile = {
                                            name: capturedFile.substring(capturedFile.lastIndexOf("/") + 1),
                                            path: capturedFile.substring(0, capturedFile.lastIndexOf("/"))
                                        };
                                        // console.log(capturedFile);
                                        // const dir = capturedFile['localURL'].split('/');
                                        //   dir.pop();
                                        //   const fromDirectory = dir.join('/');
                                        //   // console.log(this.videoFile);
                                        this.changeDetector.detectChanges();
                                        //   const streamingOptions: StreamingVideoOptions = {
                                        //     successCallback: () => { console.log('Video played'); },
                                        //     errorCallback: (e) => { console.log('Error streaming'); },
                                        //     orientation: 'landscape',
                                        //     shouldAutoClose: true,
                                        //     controls: false
                                        //   };
                                        //   // console.log(fromDirectory);
                                        _a = this;
                                        return [4 /*yield*/, this.file.readAsDataURL(this.videoFile.path, this.videoFile.name)];
                                    case 3:
                                        //   const streamingOptions: StreamingVideoOptions = {
                                        //     successCallback: () => { console.log('Video played'); },
                                        //     errorCallback: (e) => { console.log('Error streaming'); },
                                        //     orientation: 'landscape',
                                        //     shouldAutoClose: true,
                                        //     controls: false
                                        //   };
                                        //   // console.log(fromDirectory);
                                        _a.postFile = _b.sent();
                                        return [4 /*yield*/, videoLoader.dismiss()];
                                    case 4:
                                        _b.sent();
                                        console.log(this.postFile);
                                        vid = this.videoFile.name.split(".");
                                        creathumb = {
                                            fileUri: path,
                                            width: 160,
                                            height: 206,
                                            atTime: 1,
                                            outputFileName: vid[0] + "_thumb" + vid[1],
                                            quality: 30
                                        };
                                        console.log(creathumb);
                                        this.videoEditor.createThumbnail(creathumb).then(function (result) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                                            var _a;
                                            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                                                switch (_b.label) {
                                                    case 0:
                                                        _a = this;
                                                        return [4 /*yield*/, this.file.readAsDataURL(this.file.externalDataDirectory + "files/videos", result.substring(result.lastIndexOf("/") + 1))];
                                                    case 1:
                                                        _a.postFile2 = _b.sent();
                                                        console.log(this.postFile2);
                                                        return [2 /*return*/];
                                                }
                                            });
                                        }); }).catch(function (e) {
                                            console.log(e);
                                            // alert('fail video editor');
                                        });
                                        _b.label = 5;
                                    case 5:
                                        i += 1;
                                        return [3 /*break*/, 1];
                                    case 6: return [2 /*return*/];
                                }
                            });
                        }); };
                        captureError = function (error) {
                            console.log(error);
                            // navigator.notification.alert('Error code: ' + error.code, null, 'Capture Error');
                        };
                        navigator.device.capture.captureVideo(captureSuccess, captureError, { limit: 1, quality: 0 });
                        return [3 /*break*/, 5];
                    case 3:
                        err_4 = _a.sent();
                        return [4 /*yield*/, videoLoader.dismiss()];
                    case 4:
                        _a.sent();
                        console.log(err_4);
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    ExploreReportPage.prototype.onClickPlayVideo = function () {
        var streamingOptions = {
            successCallback: function () { console.log('Video played'); },
            errorCallback: function (e) { console.log('Error streaming'); },
            orientation: 'portrait',
            shouldAutoClose: true,
            controls: false
        };
        this.streamingMedia.playVideo(this.videoFile.path, streamingOptions);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('myVideo'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ExploreReportPage.prototype, "myVideo", void 0);
    ExploreReportPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-explore-report',
            template: __webpack_require__(/*! ./explore-report.page.html */ "./src/app/page/mainmenu/exploremenu/explore-report/explore-report.page.html"),
            styles: [__webpack_require__(/*! ./explore-report.page.scss */ "./src/app/page/mainmenu/exploremenu/explore-report/explore-report.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
            _services_firebase_service_firebase_service__WEBPACK_IMPORTED_MODULE_10__["FirebaseService"],
            src_app_services_myutils_myutils_service__WEBPACK_IMPORTED_MODULE_13__["MyutilsService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"],
            src_app_services_showtoast_showtoast_service__WEBPACK_IMPORTED_MODULE_12__["ShowtoastService"],
            _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_5__["File"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_9__["Camera"],
            _ionic_native_media_capture_ngx__WEBPACK_IMPORTED_MODULE_6__["MediaCapture"],
            _ionic_native_media_ngx__WEBPACK_IMPORTED_MODULE_7__["Media"],
            _ionic_native_streaming_media_ngx__WEBPACK_IMPORTED_MODULE_8__["StreamingMedia"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _ionic_native_video_editor_ngx__WEBPACK_IMPORTED_MODULE_14__["VideoEditor"]])
    ], ExploreReportPage);
    return ExploreReportPage;
}());



/***/ }),

/***/ "./src/app/services/showtoast/showtoast.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/services/showtoast/showtoast.service.ts ***!
  \*********************************************************/
/*! exports provided: ShowtoastService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShowtoastService", function() { return ShowtoastService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var ShowtoastService = /** @class */ (function () {
    function ShowtoastService(toastCtrl) {
        this.toastCtrl = toastCtrl;
    }
    ShowtoastService.prototype.showToast = function (message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: message,
                            duration: 3000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    ShowtoastService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]])
    ], ShowtoastService);
    return ShowtoastService;
}());



/***/ })

}]);
//# sourceMappingURL=exploremenu-explore-report-explore-report-module.js.map