(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["shopmenu-home-shop-home-shop-module"],{

/***/ "./src/app/page/mainmenu/shopmenu/home-shop/home-shop.module.ts":
/*!**********************************************************************!*\
  !*** ./src/app/page/mainmenu/shopmenu/home-shop/home-shop.module.ts ***!
  \**********************************************************************/
/*! exports provided: HomeShopPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeShopPageModule", function() { return HomeShopPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _home_shop_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home-shop.page */ "./src/app/page/mainmenu/shopmenu/home-shop/home-shop.page.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");
/* harmony import */ var _activitymenu_activitypopover_activitypopover_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../activitymenu/activitypopover/activitypopover.page */ "./src/app/page/mainmenu/activitymenu/activitypopover/activitypopover.page.ts");









var HomeShopPageModule = /** @class */ (function () {
    function HomeShopPageModule() {
    }
    HomeShopPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild([{ path: '', component: _home_shop_page__WEBPACK_IMPORTED_MODULE_6__["HomeShopPage"] }])
            ],
            declarations: [_home_shop_page__WEBPACK_IMPORTED_MODULE_6__["HomeShopPage"], _activitymenu_activitypopover_activitypopover_page__WEBPACK_IMPORTED_MODULE_8__["ActivitypopoverPage"]],
            entryComponents: [_activitymenu_activitypopover_activitypopover_page__WEBPACK_IMPORTED_MODULE_8__["ActivitypopoverPage"]]
        })
    ], HomeShopPageModule);
    return HomeShopPageModule;
}());



/***/ }),

/***/ "./src/app/page/mainmenu/shopmenu/home-shop/home-shop.page.html":
/*!**********************************************************************!*\
  !*** ./src/app/page/mainmenu/shopmenu/home-shop/home-shop.page.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content no-padding>\n  <div id=\"shop-search-bar\">\n    <ion-searchbar color=\"white\" placeholder=\"Search\" no-padding [(ngModel)]=\"itemSearchQuery\" (ionChange)=\"searchItems()\"></ion-searchbar>\n  </div>\n\n  <div id=\"shop-main-container\">\n    <ion-segment [(ngModel)]=\"selectedSegment\" (ionChange)=\"segmentChanged()\">\n      <ion-segment-button value=\"0\">\n        <ion-label>All Items</ion-label>\n      </ion-segment-button>\n      <ion-segment-button value=\"1\">\n        <ion-label>My Items</ion-label>\n      </ion-segment-button>\n      <ion-segment-button value=\"2\">\n        <ion-label>Saved Items</ion-label>\n      </ion-segment-button>\n      <ion-segment-button value=\"3\">\n        <ion-label>Conversations</ion-label>\n      </ion-segment-button>\n    </ion-segment>\n\n    <ion-grid *ngIf=\"selectedSegment !== '3'\">\n      <ion-row *ngIf=\"searchResults && searchResults.length > 0\"> \n        <ion-col size=\"4\" *ngFor=\"let item of searchResults\">\n          <div class=\"home-shop-item\" (click)=\"viewItemDetailPage(item.id)\">\n            <img [src]=\"item.imgs&&item.imgs[0]?item.imgs[0]:''\" />\n            <div class=\"home-shop-item-description\">\n              <p>{{item.title}}</p>\n            </div>\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row *ngIf=\"searchResults && searchResults.length == 0\">\n        <ion-col size=\"12\">\n           <p>No data found in near by location</p>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <div *ngIf=\"selectedSegment === '3'\">\n      <ion-header no-border>     \n        <ion-toolbar>\n          <!-- <ion-title color=\"navback\">Conversations</ion-title> -->\n          <!-- <p (click)=\"onClickPopOver()\" style=\"text-decoration: underline; color: blue;text-align: end;margin: 0px 20px;\">Hidden Messages</p> -->\n          <!-- <ion-buttons slot=\"end\" style=\"background-color: rgba(0,0,0,0.1);border-radius: 5px;\" (click)=\"onClickPopOver()\">\n            <ion-button fill=\"clear\" > -->\n              <p style=\"text-align: end;margin: 0px 10px;\">\n              <ion-text (click)=\"onClickPopOver()\" style=\"text-decoration: underline; color: blue;\">Hidden Conversations</ion-text>\n              </p>\n              <!-- <p></p> -->\n            <!-- </ion-button>\n          </ion-buttons> -->\n        </ion-toolbar>\n      </ion-header>\n      <div id=\"home-activity-contact-container\">\n        <div class=\"home-activity-contact-item\" *ngFor=\"let contact of contacts; let i = index;\" (click)=\"viewChatRoom(i)\" style=\"padding: 10px;\">\n          <ion-card style=\"display: flex;margin: 0px;padding: 10px;\">\n          <div class=\"home-activity-contact-img\">\n            <div class=\"home-activity-contact-boardimg\">\n              <img [src]=\"contact.boardImg\" style=\"width:80px;height: 80px;object-fit: cover;\" />\n            </div>\n          </div>\n          <div class=\"home-activity-contact-main\" style=\"margin-left: 10px;width: calc(100% - 90px);display: flex;flex-direction: column;justify-content: center;\">\n            <div>\n              <ion-text>\n                {{contact.boardName | titlecase}}\n              </ion-text>\n              <br/>\n              <ion-text>\n                Item: {{contact.name ? (contact.name | formatname):''}}\n              </ion-text>\n              <br/>\n              <ion-text (click)=\"onClickMessageControlBtn($event, contact.itemid, 0)\" style=\"text-decoration: underline; color: blue;\">Hide Conversation</ion-text>\n              <!-- <ion-buttons slot=\"start\" style=\"background-color: rgba(0,0,0,0.1);border-radius: 5px;\">\n                <ion-button fill=\"clear\" (click)=\"onClickMessageControlBtn($event, contact.itemid, 0)\">\n                  <p>Hide Conversation</p>\n                </ion-button>\n              </ion-buttons> -->\n            </div>\n          </div>\n        </ion-card>\n        </div>\n      </div>\n    </div>\n  </div>\n</ion-content>\n\n<ion-footer *ngIf=\"selectedSegment !== '3'\">\n  <ion-toolbar>\n    <ion-button (click)=\"viewNewSurfboardPage()\">\n      Post item for Sale\n    </ion-button>\n  </ion-toolbar>\n</ion-footer>\n"

/***/ }),

/***/ "./src/app/page/mainmenu/shopmenu/home-shop/home-shop.page.scss":
/*!**********************************************************************!*\
  !*** ./src/app/page/mainmenu/shopmenu/home-shop/home-shop.page.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#shop-search-bar {\n  display: flex;\n  align-items: center;\n  padding: 16px 10px;\n  background-color: #84A1B3; }\n\nion-segment-button {\n  font-size: 12px;\n  flex: 1; }\n\n#home-shop-itemcontainer {\n  padding: 10px; }\n\n#home-shop-itemcontainer ion-col {\n    padding: 0 !important; }\n\n.home-shop-item {\n  height: 28vw;\n  overflow: hidden;\n  position: relative; }\n\n.home-shop-item img {\n    width: 100%;\n    min-height: 100%; }\n\n.home-shop-item-description {\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  width: 100%;\n  text-align: center;\n  z-index: 2;\n  background-color: rgba(0, 0, 0, 0.4); }\n\n.home-shop-item-description p {\n    color: white;\n    font-family: \"Open Sans Bold\"; }\n\nion-footer ion-toolbar {\n  text-align: center; }\n\n#home-shop-btn-myitem {\n  margin-left: 5vw; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2xva2VzaC9saW5ldXAgc291cmNlKDA5LTMwKSAoMikvbGluZXVwKDA5LTMwKS9zcmMvYXBwL3BhZ2UvbWFpbm1lbnUvc2hvcG1lbnUvaG9tZS1zaG9wL2hvbWUtc2hvcC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDSSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQix5QkFBeUIsRUFBQTs7QUFHN0I7RUFDSSxlQUFlO0VBQ2YsT0FBTyxFQUFBOztBQUlYO0VBQ0ksYUFBYSxFQUFBOztBQURqQjtJQUdRLHFCQUFxQixFQUFBOztBQUk3QjtFQUNJLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsa0JBQWtCLEVBQUE7O0FBSHRCO0lBS1EsV0FBVztJQUNYLGdCQUFnQixFQUFBOztBQUl4QjtFQUNJLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsT0FBTztFQUNQLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLG9DQUFvQyxFQUFBOztBQVB4QztJQVNRLFlBQVk7SUFDWiw2QkFBNkIsRUFBQTs7QUFJckM7RUFFUSxrQkFBa0IsRUFBQTs7QUFJMUI7RUFDSSxnQkFBZ0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2UvbWFpbm1lbnUvc2hvcG1lbnUvaG9tZS1zaG9wL2hvbWUtc2hvcC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbiNzaG9wLXNlYXJjaC1iYXIge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBwYWRkaW5nOiAxNnB4IDEwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzg0QTFCMztcbn1cblxuaW9uLXNlZ21lbnQtYnV0dG9uIHtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgZmxleDogMTtcbn1cblxuXG4jaG9tZS1zaG9wLWl0ZW1jb250YWluZXIge1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgaW9uLWNvbCB7XG4gICAgICAgIHBhZGRpbmc6IDAgIWltcG9ydGFudDtcbiAgICB9XG59XG5cbi5ob21lLXNob3AtaXRlbSB7XG4gICAgaGVpZ2h0OiAyOHZ3O1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGltZyB7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBtaW4taGVpZ2h0OiAxMDAlO1xuICAgIH1cbn1cblxuLmhvbWUtc2hvcC1pdGVtLWRlc2NyaXB0aW9uIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYm90dG9tOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHotaW5kZXg6IDI7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjQpO1xuICAgIHAge1xuICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBcIk9wZW4gU2FucyBCb2xkXCI7XG4gICAgfVxufVxuXG5pb24tZm9vdGVyIHtcbiAgICBpb24tdG9vbGJhciB7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG59XG5cbiNob21lLXNob3AtYnRuLW15aXRlbSB7XG4gICAgbWFyZ2luLWxlZnQ6IDV2dztcbn0iXX0= */"

/***/ }),

/***/ "./src/app/page/mainmenu/shopmenu/home-shop/home-shop.page.ts":
/*!********************************************************************!*\
  !*** ./src/app/page/mainmenu/shopmenu/home-shop/home-shop.page.ts ***!
  \********************************************************************/
/*! exports provided: HomeShopPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeShopPage", function() { return HomeShopPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_firebase_service_firebase_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/firebase-service/firebase.service */ "./src/app/services/firebase-service/firebase.service.ts");
/* harmony import */ var src_app_services_myutils_myutils_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/myutils/myutils.service */ "./src/app/services/myutils/myutils.service.ts");






var HomeShopPage = /** @class */ (function () {
    function HomeShopPage(router, firebaseService, loadingCtrl, event, myUtils, popoverCtrl, navCtrl) {
        var _this = this;
        this.router = router;
        this.firebaseService = firebaseService;
        this.loadingCtrl = loadingCtrl;
        this.event = event;
        this.myUtils = myUtils;
        this.popoverCtrl = popoverCtrl;
        this.navCtrl = navCtrl;
        this.itemSearchQuery = '';
        this.userInfo = {};
        this.boardLoader = null;
        /**
         * selected segments
         *  0: All
         *  1: My Items
         *  2: Saved Items
         */
        this.selectedSegment = '0';
        this.arrMyItems = [];
        this.arrMyFavItems = [];
        this.currentUserInfo = {};
        this.messageDataRef = null;
        this.messageDataListener = null;
        this.allContacts = [];
        this.contacts = [];
        this.historyLoader = null;
        this.arrHideLists = [];
        this.shopItems = [];
        this.event.subscribe('onfavchange', function (data) {
            _this.initPage();
        });
        this.event.subscribe('onchangehidelist', function (data) {
            _this.initPageC();
        });
    }
    HomeShopPage.prototype.ngOnInit = function () {
        this.currentUserId = this.firebaseService.getUID();
        console.log(this.currentUserId);
    };
    HomeShopPage.prototype.ionViewWillEnter = function () {
        this.initPage();
        this.initPageC();
    };
    HomeShopPage.prototype.initPage = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.shopItems = [];
                        this.searchResults = [];
                        this.arrMyItems = [];
                        this.arrMyFavItems = [];
                        this.selectedSegment = '0';
                        return [4 /*yield*/, this.getUserProfile()];
                    case 1:
                        _a.sent();
                        this.getAllSurfBoards();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomeShopPage.prototype.initPageC = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var strHideLists;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.messageDataRef = null;
                        this.messageDataListener = null;
                        this.arrHideLists = [];
                        if (localStorage.getItem("hidelist") !== null && localStorage.getItem("hidelist") !== "") {
                            strHideLists = localStorage.getItem("hidelist");
                            this.arrHideLists = strHideLists.split(",");
                        }
                        return [4 /*yield*/, this.getCurrentUserInfo()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.getChatHistory()];
                    case 2:
                        _a.sent();
                        this.event.publish("onReceiveNotification", { isClear: true });
                        return [2 /*return*/];
                }
            });
        });
    };
    HomeShopPage.prototype.getCurrentUserInfo = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _a, currentUserSnapshot, err_1;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.loadingCtrl.create({
                                message: "Loading..."
                            })];
                    case 1:
                        _a.historyLoader = _b.sent();
                        return [4 /*yield*/, this.historyLoader.present()];
                    case 2:
                        _b.sent();
                        _b.label = 3;
                    case 3:
                        _b.trys.push([3, 5, , 6]);
                        return [4 /*yield*/, this.firebaseService.getUserProfile(this.currentUserId)];
                    case 4:
                        currentUserSnapshot = _b.sent();
                        this.currentUserInfo = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, currentUserSnapshot.val());
                        console.log(this.currentUserInfo);
                        return [3 /*break*/, 6];
                    case 5:
                        err_1 = _b.sent();
                        this.historyLoader.dismiss();
                        console.log(err_1);
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    HomeShopPage.prototype.getChatHistory = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var self;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                self = this;
                this.messageDataRef = this.firebaseService.getAllChatHistoryRef();
                this.messageDataListener = this.messageDataRef.on("value", function (snapshot) {
                    self.contacts = [];
                    self.allContacts = [];
                    snapshot.forEach(function (childSnapshot) {
                        childSnapshot.forEach(function (valueSnapshot) {
                            var users = valueSnapshot.key.split("-");
                            var tmpCUIndex = users.findIndex(function (item) { return item == self.currentUserId; });
                            var tmpMessageObj = valueSnapshot.val();
                            var boardName = "";
                            var boardImg = "";
                            if (tmpMessageObj !== null) {
                                boardName = tmpMessageObj[Object.keys(tmpMessageObj)[0]].boardName;
                                boardImg = tmpMessageObj[Object.keys(tmpMessageObj)[0]].contentimg;
                            }
                            if (tmpCUIndex > -1) {
                                var otherUID_1 = tmpCUIndex == 0 ? users[1] : users[0];
                                console.log(otherUID_1);
                                var isExists = self.contacts.findIndex(function (item) { return item == otherUID_1; });
                                if (isExists == -1) {
                                    self.getUserData(otherUID_1, childSnapshot.key, boardName, boardImg);
                                }
                            }
                        });
                    });
                    self.myUtils.setChatHistory(_this.allContacts);
                    self.historyLoader.dismiss();
                });
                return [2 /*return*/];
            });
        });
    };
    HomeShopPage.prototype.getUserData = function (uid, itemid, boardName, boardImg) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var userInfoSnapshot, isHidden, isAllExists, isContactExists, err_2;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.firebaseService.getUserProfile(uid)];
                    case 1:
                        userInfoSnapshot = _a.sent();
                        isHidden = this.arrHideLists.indexOf(itemid);
                        isAllExists = this.allContacts.findIndex(function (x) { return x.itemid === itemid; });
                        isContactExists = this.contacts.findIndex(function (x) { return x.itemid === itemid; });
                        console.log(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, userInfoSnapshot.val()));
                        if (Object.keys(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, userInfoSnapshot.val())).length > 0) {
                            if (isAllExists == -1) {
                                this.allContacts.push(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, userInfoSnapshot.val(), { itemid: itemid, boardName: boardName, boardImg: boardImg, name: (userInfoSnapshot.val().fname ? userInfoSnapshot.val().fname : '') + " " + (userInfoSnapshot.val().lname ? userInfoSnapshot.val().lname : '') }));
                            }
                            if (isHidden == -1 && isContactExists == -1) {
                                this.contacts.push(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, userInfoSnapshot.val(), { itemid: itemid, boardName: boardName, boardImg: boardImg, name: (userInfoSnapshot.val().fname ? userInfoSnapshot.val().fname : '') + " " + (userInfoSnapshot.val().lname ? userInfoSnapshot.val().lname : '') }));
                            }
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        err_2 = _a.sent();
                        console.log(err_2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    HomeShopPage.prototype.viewChatRoom = function (contactid) {
        this.stopActivityListener();
        this.navCtrl.navigateForward('/home/shop/chat/' + this.contacts[contactid].itemid + "/" + this.contacts[contactid].uid);
    };
    HomeShopPage.prototype.onClickMessageControlBtn = function (evt, itemid) {
        evt.stopPropagation();
        this.arrHideLists.push(itemid);
        var index = this.contacts.findIndex(function (x) { return x.itemid == itemid; });
        if (index > -1) {
            this.contacts.splice(index, 1);
        }
        localStorage.setItem("hidelist", this.arrHideLists.join(","));
    };
    HomeShopPage.prototype.onClickPopOver = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                this.router.navigateByUrl('/hidelist');
                return [2 /*return*/];
            });
        });
    };
    HomeShopPage.prototype.ionViewWillLeave = function () {
        this.stopActivityListener();
    };
    HomeShopPage.prototype.stopActivityListener = function () {
        if (this.messageDataListener != null) {
            this.messageDataRef.off("value", this.messageDataListener);
            this.messageDataListener = null;
            this.messageDataRef = null;
        }
    };
    HomeShopPage.prototype.getUserProfile = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _a, uid, userInfoSnapshot, err_3;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.loadingCtrl.create({
                                message: 'Loading...'
                            })];
                    case 1:
                        _a.boardLoader = _b.sent();
                        return [4 /*yield*/, this.boardLoader.present()];
                    case 2:
                        _b.sent();
                        uid = this.firebaseService.getUID();
                        _b.label = 3;
                    case 3:
                        _b.trys.push([3, 5, , 6]);
                        return [4 /*yield*/, this.firebaseService.getUserProfile(uid)];
                    case 4:
                        userInfoSnapshot = _b.sent();
                        this.userInfo = userInfoSnapshot.val();
                        return [3 /*break*/, 6];
                    case 5:
                        err_3 = _b.sent();
                        console.log(err_3);
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    HomeShopPage.prototype.getAllSurfBoards = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var self, allBoardsSnapshot, err_4;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        self = this;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.firebaseService.getAllSurfBoard()];
                    case 2:
                        allBoardsSnapshot = _a.sent();
                        allBoardsSnapshot.forEach(function (items) {
                            console.log(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, items.val()));
                            self.shopItems.push(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, items.val(), { id: items.key }));
                        });
                        self.searchResults = self.shopItems;
                        this.boardLoader.dismiss();
                        this.filterArray();
                        return [3 /*break*/, 4];
                    case 3:
                        err_4 = _a.sent();
                        console.log(err_4);
                        this.boardLoader.dismiss();
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    HomeShopPage.prototype.searchItems = function () {
        var keyWord = this.itemSearchQuery.toLowerCase();
        var arrItems = [];
        if (this.selectedSegment === '0') {
            arrItems = this.shopItems;
        }
        else if (this.selectedSegment === '1') {
            arrItems = this.arrMyItems;
        }
        else if (this.selectedSegment === '2') {
            arrItems = this.arrMyFavItems;
        }
        if (keyWord === '') {
            this.searchResults = arrItems;
        }
        else {
            if (this.shopItems.length > 0) {
                this.searchResults = [];
                for (var i = 0; i < arrItems.length; i++) {
                    var itemDescription = arrItems[i].title.toLowerCase();
                    if (itemDescription.includes(keyWord) === true) {
                        this.searchResults.push(arrItems[i]);
                    }
                }
                this.filterArray();
            }
        }
    };
    HomeShopPage.prototype.viewNewSurfboardPage = function () {
        this.router.navigateByUrl('/home/shop/home-shop-newitem');
    };
    HomeShopPage.prototype.viewItemDetailPage = function (itemid) {
        sessionStorage.setItem("fromHome", "0");
        this.router.navigateByUrl('/home/shop/shopitemdetail/' + itemid + '/shopmain');
    };
    HomeShopPage.prototype.segmentChanged = function () {
        console.log(this.userInfo);
        var arrUserFavItems = this.userInfo.favorite.products.split(',');
        var uid = this.userInfo.uid;
        if (this.selectedSegment === '0') {
            this.searchResults = this.shopItems;
        }
        else if (this.selectedSegment === '1') {
            this.searchResults = this.shopItems.filter(function (x) { return x.ownerid === uid; });
            this.arrMyItems = this.searchResults;
        }
        else if (this.selectedSegment === '2') {
            this.searchResults = this.shopItems.filter(function (x) { return arrUserFavItems.indexOf(x.id) > -1; });
            this.arrMyFavItems = this.searchResults;
        }
        console.log(this.searchResults);
        this.filterArray();
        if (this.selectedSegment === '3') {
            this.initPageC();
        }
    };
    HomeShopPage.prototype.filterArray = function () {
        this.filterArrayinMiles();
    };
    HomeShopPage.prototype.filterArrayinMiles = function () {
        var _this = this;
        console.log(this.currentUserInfo.noti.anywhere);
        if (!this.currentUserInfo.noti.anywhere && this.selectedSegment == 0) {
            this.searchResults = this.searchResults.filter(function (el) {
                return _this.getDistanceFromLatLonInKm(_this.currentUserInfo.location.lat, _this.currentUserInfo.location.long, el.location.lat, el.location.lat) <= (_this.currentUserInfo.noti.range ? _this.currentUserInfo.noti.range : 50);
            });
        }
        setTimeout(function () {
            _this.searchResults.sort(function (x, y) {
                return y.createdat - x.createdat;
            });
        }, 100);
    };
    HomeShopPage.prototype.getDistanceFromLatLonInKm = function (lat1, lon1, lat2, lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = this.deg2rad(lat2 - lat1); // deg2rad below
        var dLon = this.deg2rad(lon2 - lon1);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
                Math.sin(dLon / 2) * Math.sin(dLon / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c; // Distance in km
        return d / 1.6;
    };
    HomeShopPage.prototype.deg2rad = function (deg) {
        return deg * (Math.PI / 180);
    };
    HomeShopPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home-shop',
            template: __webpack_require__(/*! ./home-shop.page.html */ "./src/app/page/mainmenu/shopmenu/home-shop/home-shop.page.html"),
            styles: [__webpack_require__(/*! ./home-shop.page.scss */ "./src/app/page/mainmenu/shopmenu/home-shop/home-shop.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services_firebase_service_firebase_service__WEBPACK_IMPORTED_MODULE_4__["FirebaseService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Events"],
            src_app_services_myutils_myutils_service__WEBPACK_IMPORTED_MODULE_5__["MyutilsService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["PopoverController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]])
    ], HomeShopPage);
    return HomeShopPage;
}());



/***/ })

}]);
//# sourceMappingURL=shopmenu-home-shop-home-shop-module.js.map