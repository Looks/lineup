(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["shopmenu-home-shop-item-detail-home-shop-item-detail-module"],{

/***/ "./src/app/page/mainmenu/shopmenu/home-shop-item-detail/home-shop-item-detail.module.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/page/mainmenu/shopmenu/home-shop-item-detail/home-shop-item-detail.module.ts ***!
  \**********************************************************************************************/
/*! exports provided: HomeShopItemDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeShopItemDetailPageModule", function() { return HomeShopItemDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _home_shop_item_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home-shop-item-detail.page */ "./src/app/page/mainmenu/shopmenu/home-shop-item-detail/home-shop-item-detail.page.ts");
/* harmony import */ var src_app_page_modal_modal_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/page/modal/modal.module */ "./src/app/page/modal/modal.module.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");









var routes = [
    {
        path: '',
        component: _home_shop_item_detail_page__WEBPACK_IMPORTED_MODULE_6__["HomeShopItemDetailPage"]
    }
];
var HomeShopItemDetailPageModule = /** @class */ (function () {
    function HomeShopItemDetailPageModule() {
    }
    HomeShopItemDetailPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                src_app_page_modal_modal_module__WEBPACK_IMPORTED_MODULE_7__["ModalModule"],
                src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_home_shop_item_detail_page__WEBPACK_IMPORTED_MODULE_6__["HomeShopItemDetailPage"]],
        })
    ], HomeShopItemDetailPageModule);
    return HomeShopItemDetailPageModule;
}());



/***/ }),

/***/ "./src/app/page/mainmenu/shopmenu/home-shop-item-detail/home-shop-item-detail.page.html":
/*!**********************************************************************************************!*\
  !*** ./src/app/page/mainmenu/shopmenu/home-shop-item-detail/home-shop-item-detail.page.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button color=\"navback\"></ion-back-button>\n    </ion-buttons>\n    <!-- <ion-icon color=\"navback\" name=\"arrow-back\" (click)=\"onClickBackBtn()\"></ion-icon> -->\n    <ion-title>{{itemTitle}}</ion-title>\n    <ion-icon slot=\"end\" [color]=\"isFavoriteProd?'activestar':'light'\" name=\"star\" (click)=\"onToggleFavoriteBtn()\"></ion-icon>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-slides pager=\"true\" [options]=\"slideOptions\" *ngIf=\"!isCurrentUser\">\n    <ion-slide *ngFor=\"let img of selectedItem.imgs\">\n      <img [src]=\"img\" (click)=\"onClickItemImage()\"/>\n    </ion-slide>\n  </ion-slides>\n  <ion-grid *ngIf=\"isCurrentUser\">\n    <ion-row nowrap id=\"home-shopnewitem-imgrow\">\n      <ion-col *ngFor=\"let itemImg of selectedItem.imgs; let i = index;\" size=\"4\">\n        <div class=\"home-shopnewitem-emptyimg-container\" (click)=\"getFile(i)\">\n          <div class=\"home-shopnewitem-emptyimg\">\n            <img [src]=\"itemImg == ''?'assets/imgs/img-plus.svg': itemImg\" [class]=\"itemImg==''?'home-shopnewitem-noimg':'home-shopnewitem-itemimg'\" />\n            <p *ngIf=\"itemImg==''\">Upload Image</p>\n          </div>\n        </div>\n      </ion-col>\n        <ion-col *ngIf=\"selectedItem.imgs.length<3\" size=\"4\">\n          <div class=\"home-shopnewitem-emptyimg-container\" (click)=\"getFile(selectedItem.imgs.length)\">\n            <div class=\"home-shopnewitem-emptyimg\">\n              <img src=\"assets/imgs/img-plus.svg\" class=\"home-shopnewitem-noimg\" />\n              <p>Upload Image</p>\n            </div>\n          </div>\n        </ion-col>\n      </ion-row>\n  </ion-grid>\n\n  <div id=\"home-shop-item-owner-header\">\n    <p>Owner</p>\n  </div>\n\n  <div id=\"home-shop-item-owner-info\">\n    <img src=\"assets/imgs/img-person.svg\" />\n    <span>{{selectedItem.ownername | formatname}}</span>\n  </div>\n  <ion-item>\n    <ion-label position=\"stacked\">Description</ion-label>\n    <ion-input type=\"text\" [(ngModel)]=\"selectedItem.description\" [disabled]=\"!isCurrentUser\"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label position=\"stacked\">Price($)</ion-label>\n    <ion-input type=\"number\" [(ngModel)]=\"selectedItem.price\" [disabled]=\"!isCurrentUser\"></ion-input>\n  </ion-item>\n\n  <div id=\"home-shop-item-btncontainer\" *ngIf=\"!isCurrentUser\">\n    <div>\n    <ion-button color=\"primary\" (click)=\"sendMessageToOwner()\" style=\"width:100%\">Send Message</ion-button>\n    </div>\n  </div>\n  <div id=\"home-shop-item-btncontainer\" *ngIf=\"isCurrentUser\">\n    <div>\n      <ion-button color=\"primary\" (click)=\"onClickSaveBtn()\">Save</ion-button>\n    </div>\n    <div>\n      <ion-button color=\"primary\" (click)=\"onClickItemDelete()\">Sold</ion-button>\n    </div>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/page/mainmenu/shopmenu/home-shop-item-detail/home-shop-item-detail.page.scss":
/*!**********************************************************************************************!*\
  !*** ./src/app/page/mainmenu/shopmenu/home-shop-item-detail/home-shop-item-detail.page.scss ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-header {\n  --ion-background-color: #84A1B3; }\n\nion-toolbar {\n  position: relative;\n  --background: #84A1B3; }\n\nion-toolbar ion-icon {\n    font-size: 18pt;\n    margin: 0 16px; }\n\nion-toolbar p {\n    font-weight: bold;\n    padding-right: 5px; }\n\n.home-shopnewitem-emptyimg-container {\n  width: 100px;\n  height: 100px;\n  background-color: #d6d6d6;\n  position: relative;\n  overflow: hidden; }\n\n.home-shopnewitem-emptyimg {\n  text-align: center;\n  position: absolute;\n  width: 100px;\n  top: 50%;\n  transform: translate(-50%, -50%);\n  left: 50%; }\n\n.home-shopnewitem-emptyimg p {\n    font-size: 11pt; }\n\n.home-shopnewitem-noimg {\n  width: 14px; }\n\n.home-shopnewitem-itemimg {\n  width: 100%; }\n\nion-title {\n  text-align: center;\n  color: #CAE9F7;\n  font-weight: bold;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%); }\n\nion-slide img {\n  height: 35vh;\n  width: auto; }\n\n#home-shop-item-owner-header {\n  margin-top: 6vh; }\n\n#home-shop-item-owner-header p {\n    font-size: .85em;\n    padding-left: 16px; }\n\n#home-shop-item-owner-info {\n  display: flex;\n  padding: 5px 20px; }\n\n#home-shop-item-owner-info span {\n    margin-left: 10px; }\n\n#home-shop-item-owner-info img {\n    width: 24px;\n    height: 24px; }\n\n.home-shop-item-info-header p {\n  color: #658696;\n  font-weight: bold; }\n\n#home-shop-item-board-description p {\n  margin: 5px 20px; }\n\n#home-shop-item-btnsendmessage {\n  margin-top: 3vh;\n  text-align: center; }\n\n#home-shop-item-btncontainer {\n  display: flex;\n  margin-top: 3vh; }\n\n#home-shop-item-btncontainer div {\n    flex: 1;\n    text-align: center; }\n\n#home-shop-item-btncontainer div ion-button {\n      width: 70%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2xva2VzaC9saW5ldXAgc291cmNlKDA5LTMwKSAoMikvbGluZXVwKDA5LTMwKS9zcmMvYXBwL3BhZ2UvbWFpbm1lbnUvc2hvcG1lbnUvaG9tZS1zaG9wLWl0ZW0tZGV0YWlsL2hvbWUtc2hvcC1pdGVtLWRldGFpbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSwrQkFBdUIsRUFBQTs7QUFHM0I7RUFDSSxrQkFBa0I7RUFDbEIscUJBQWEsRUFBQTs7QUFGakI7SUFJUSxlQUFlO0lBQ2YsY0FBYyxFQUFBOztBQUx0QjtJQVFRLGlCQUFpQjtJQUNqQixrQkFBa0IsRUFBQTs7QUFHMUI7RUFDSSxZQUFZO0VBQ1osYUFBYTtFQUNiLHlCQUF5QjtFQUN6QixrQkFBa0I7RUFDbEIsZ0JBQWdCLEVBQUE7O0FBRXBCO0VBQ0ksa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osUUFBUTtFQUNSLGdDQUFnQztFQUNoQyxTQUFTLEVBQUE7O0FBTmI7SUFRUSxlQUFlLEVBQUE7O0FBSXZCO0VBQ0ksV0FBVyxFQUFBOztBQUdmO0VBQ0ksV0FBVyxFQUFBOztBQUVmO0VBQ0ksa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLFFBQVE7RUFDUixTQUFTO0VBQ1QsZ0NBQWdDLEVBQUE7O0FBR3BDO0VBRVEsWUFBWTtFQUNaLFdBQVcsRUFBQTs7QUFJbkI7RUFDSSxlQUFlLEVBQUE7O0FBRG5CO0lBR1EsZ0JBQWdCO0lBQ2hCLGtCQUFrQixFQUFBOztBQUkxQjtFQUNJLGFBQWE7RUFDYixpQkFBaUIsRUFBQTs7QUFGckI7SUFJUSxpQkFBaUIsRUFBQTs7QUFKekI7SUFPUSxXQUFXO0lBQ1gsWUFBWSxFQUFBOztBQUlwQjtFQUVRLGNBQWM7RUFDZCxpQkFBaUIsRUFBQTs7QUFJekI7RUFFUSxnQkFBZ0IsRUFBQTs7QUFJeEI7RUFDSSxlQUFlO0VBQ2Ysa0JBQWtCLEVBQUE7O0FBR3RCO0VBQ0ksYUFBYTtFQUNiLGVBQWUsRUFBQTs7QUFGbkI7SUFJUSxPQUFPO0lBQ1Asa0JBQWtCLEVBQUE7O0FBTDFCO01BT1ksVUFBVSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZS9tYWlubWVudS9zaG9wbWVudS9ob21lLXNob3AtaXRlbS1kZXRhaWwvaG9tZS1zaG9wLWl0ZW0tZGV0YWlsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1oZWFkZXIge1xuICAgIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICM4NEExQjM7XG59XG5cbmlvbi10b29sYmFyIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgLS1iYWNrZ3JvdW5kOiAjODRBMUIzO1xuICAgIGlvbi1pY29uIHtcbiAgICAgICAgZm9udC1zaXplOiAxOHB0O1xuICAgICAgICBtYXJnaW46IDAgMTZweDtcbiAgICB9XG4gICAgcCB7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG4gICAgfVxufVxuLmhvbWUtc2hvcG5ld2l0ZW0tZW1wdHlpbWctY29udGFpbmVyIHtcbiAgICB3aWR0aDogMTAwcHg7XG4gICAgaGVpZ2h0OiAxMDBweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDZkNmQ2O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLmhvbWUtc2hvcG5ld2l0ZW0tZW1wdHlpbWcge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgd2lkdGg6IDEwMHB4O1xuICAgIHRvcDogNTAlO1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuICAgIGxlZnQ6IDUwJTtcbiAgICBwIHtcbiAgICAgICAgZm9udC1zaXplOiAxMXB0O1xuICAgIH1cbn1cblxuLmhvbWUtc2hvcG5ld2l0ZW0tbm9pbWcge1xuICAgIHdpZHRoOiAxNHB4O1xufVxuXG4uaG9tZS1zaG9wbmV3aXRlbS1pdGVtaW1nIHtcbiAgICB3aWR0aDogMTAwJTtcbn1cbmlvbi10aXRsZSB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGNvbG9yOiAjQ0FFOUY3O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDUwJTtcbiAgICBsZWZ0OiA1MCU7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG59XG5cbmlvbi1zbGlkZSB7XG4gICAgaW1nIHtcbiAgICAgICAgaGVpZ2h0OiAzNXZoO1xuICAgICAgICB3aWR0aDogYXV0bztcbiAgICB9XG59XG5cbiNob21lLXNob3AtaXRlbS1vd25lci1oZWFkZXIge1xuICAgIG1hcmdpbi10b3A6IDZ2aDtcbiAgICBwIHtcbiAgICAgICAgZm9udC1zaXplOiAuODVlbTtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICAgIH1cbn1cblxuI2hvbWUtc2hvcC1pdGVtLW93bmVyLWluZm8ge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgcGFkZGluZzogNXB4IDIwcHg7XG4gICAgc3BhbiB7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgIH1cbiAgICBpbWcge1xuICAgICAgICB3aWR0aDogMjRweDtcbiAgICAgICAgaGVpZ2h0OiAyNHB4O1xuICAgIH1cbn1cblxuLmhvbWUtc2hvcC1pdGVtLWluZm8taGVhZGVyIHtcbiAgICBwIHtcbiAgICAgICAgY29sb3I6ICM2NTg2OTY7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIH1cbn1cblxuI2hvbWUtc2hvcC1pdGVtLWJvYXJkLWRlc2NyaXB0aW9uIHtcbiAgICBwIHtcbiAgICAgICAgbWFyZ2luOiA1cHggMjBweDtcbiAgICB9XG59XG5cbiNob21lLXNob3AtaXRlbS1idG5zZW5kbWVzc2FnZSB7XG4gICAgbWFyZ2luLXRvcDogM3ZoO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuI2hvbWUtc2hvcC1pdGVtLWJ0bmNvbnRhaW5lciB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBtYXJnaW4tdG9wOiAzdmg7XG4gICAgZGl2IHtcbiAgICAgICAgZmxleDogMTtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBpb24tYnV0dG9uIHtcbiAgICAgICAgICAgIHdpZHRoOiA3MCU7XG4gICAgICAgIH1cbiAgICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/page/mainmenu/shopmenu/home-shop-item-detail/home-shop-item-detail.page.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/page/mainmenu/shopmenu/home-shop-item-detail/home-shop-item-detail.page.ts ***!
  \********************************************************************************************/
/*! exports provided: HomeShopItemDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeShopItemDetailPage", function() { return HomeShopItemDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_firebase_service_firebase_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/firebase-service/firebase.service */ "./src/app/services/firebase-service/firebase.service.ts");
/* harmony import */ var src_app_page_modal_gallery_gallery_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/page/modal/gallery/gallery.page */ "./src/app/page/modal/gallery/gallery.page.ts");
/* harmony import */ var src_app_services_showtoast_showtoast_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/showtoast/showtoast.service */ "./src/app/services/showtoast/showtoast.service.ts");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");








var HomeShopItemDetailPage = /** @class */ (function () {
    function HomeShopItemDetailPage(route, toastService, loadingCtrl, modalCtrl, altCtrl, firebaseService, navCtrl, event, camera) {
        this.route = route;
        this.toastService = toastService;
        this.loadingCtrl = loadingCtrl;
        this.modalCtrl = modalCtrl;
        this.altCtrl = altCtrl;
        this.firebaseService = firebaseService;
        this.navCtrl = navCtrl;
        this.event = event;
        this.camera = camera;
        this.isCurrentUser = false;
        this.isFavoriteProd = false;
        this.arrFavProds = [];
        this.itemTitle = '';
        this.detailInfoLoader = null;
        this.backPage = '';
        this.slideOptions = {
            effect: 'flip'
        };
        this.selectedItem = {
            id: '',
            title: '',
            description: '',
            condition: null,
            price: 0,
            imgs: [],
            location: {
                lat: null,
                long: null,
                name: '',
                state: '',
                country: ''
            },
            ownerid: '',
            ownername: ''
        };
    }
    HomeShopItemDetailPage.prototype.ngOnInit = function () {
        this.initPage();
    };
    HomeShopItemDetailPage.prototype.initPage = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.itemid = this.route.snapshot.paramMap.get('itemid');
                        this.backPage = this.route.snapshot.paramMap.get('backpage');
                        this.arrFavProds = [];
                        return [4 /*yield*/, this.checkIfFavProd()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.findItem()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomeShopItemDetailPage.prototype.checkIfFavProd = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _a, currentUserSnapshot, strFavProds, err_1;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.loadingCtrl.create({
                                message: 'Loading...'
                            })];
                    case 1:
                        _a.detailInfoLoader = _b.sent();
                        return [4 /*yield*/, this.detailInfoLoader.present()];
                    case 2:
                        _b.sent();
                        _b.label = 3;
                    case 3:
                        _b.trys.push([3, 5, , 6]);
                        return [4 /*yield*/, this.firebaseService.getUserProfile(this.firebaseService.getUID())];
                    case 4:
                        currentUserSnapshot = _b.sent();
                        strFavProds = currentUserSnapshot.val().favorite.products;
                        if (strFavProds !== '') {
                            this.arrFavProds = strFavProds.split(',');
                            this.isFavoriteProd = this.arrFavProds.indexOf(this.itemid) > -1 ? true : false;
                        }
                        return [3 /*break*/, 6];
                    case 5:
                        err_1 = _b.sent();
                        this.detailInfoLoader.dismiss();
                        console.log(err_1);
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    HomeShopItemDetailPage.prototype.findItem = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var boardInfoSnapshot, err_2;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.firebaseService.getSpecificBoardInfo(this.itemid)];
                    case 1:
                        boardInfoSnapshot = _a.sent();
                        this.selectedItem = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, boardInfoSnapshot.val());
                        this.itemTitle = this.selectedItem.title;
                        this.isCurrentUser = this.firebaseService.checkIfCurrentUser(this.selectedItem.ownerid);
                        this.detailInfoLoader.dismiss();
                        console.log(this.selectedItem);
                        return [3 /*break*/, 3];
                    case 2:
                        err_2 = _a.sent();
                        this.detailInfoLoader.dismiss();
                        console.log(err_2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    HomeShopItemDetailPage.prototype.onClickItemImage = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var galleryModal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: src_app_page_modal_gallery_gallery_page__WEBPACK_IMPORTED_MODULE_5__["GalleryPage"],
                            componentProps: {
                                imgs: this.selectedItem.imgs
                            },
                            cssClass: 'gallery-modal'
                        })];
                    case 1:
                        galleryModal = _a.sent();
                        return [4 /*yield*/, galleryModal.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomeShopItemDetailPage.prototype.sendMessageToOwner = function () {
        var _this = this;
        var preurl;
        if (sessionStorage.getItem("fromHome")) {
            if (sessionStorage.getItem("fromHome") === "1") {
                preurl = "/home/main/";
            }
            else {
                preurl = "/home/shop/";
            }
        }
        this.navCtrl.navigateForward(preurl + 'chat/' + this.itemid + '/' + this.selectedItem.ownerid);
        if (localStorage.getItem("hidelist")) {
            var hideList = localStorage.getItem("hidelist").split(",");
            console.log(hideList);
            var index = hideList.findIndex(function (x) { return x == _this.itemid; });
            if (index > -1) {
                hideList.splice(index, 1);
            }
            console.log(hideList);
            localStorage.setItem("hidelist", hideList.join(","));
        }
    };
    HomeShopItemDetailPage.prototype.onClickItemDelete = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var confirmAlt;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.altCtrl.create({
                            header: 'Delete Item?',
                            subHeader: '',
                            message: 'Do you really want to delete this item?',
                            buttons: [
                                {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    handler: function (data) {
                                    }
                                },
                                {
                                    text: 'Ok',
                                    handler: function (data) {
                                        _this.deleteItem();
                                    }
                                }
                            ]
                        })];
                    case 1:
                        confirmAlt = _a.sent();
                        return [4 /*yield*/, confirmAlt.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomeShopItemDetailPage.prototype.deleteItem = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var deleteLoader, index, err_3;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...'
                        })];
                    case 1:
                        deleteLoader = _a.sent();
                        return [4 /*yield*/, deleteLoader.present()];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        _a.trys.push([3, 6, , 7]);
                        return [4 /*yield*/, this.firebaseService.deleteItem(this.itemid)];
                    case 4:
                        _a.sent();
                        return [4 /*yield*/, this.firebaseService.deleteMessage(this.itemid)];
                    case 5:
                        _a.sent();
                        if (this.isFavoriteProd == true) {
                            index = this.arrFavProds.indexOf(this.itemid);
                            if (index > -1) {
                                this.arrFavProds.splice(index, 1);
                            }
                        }
                        deleteLoader.dismiss();
                        this.toastService.showToast('Successfully Deleted!');
                        return [3 /*break*/, 7];
                    case 6:
                        err_3 = _a.sent();
                        deleteLoader.dismiss();
                        console.log(err_3);
                        return [3 /*break*/, 7];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    HomeShopItemDetailPage.prototype.onClickSaveBtn = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var updateLoader, err_4;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...'
                        })];
                    case 1:
                        updateLoader = _a.sent();
                        return [4 /*yield*/, updateLoader.present()];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        _a.trys.push([3, 5, , 7]);
                        this.firebaseService.updateItemInfo(this.selectedItem);
                        return [4 /*yield*/, updateLoader.dismiss()];
                    case 4:
                        _a.sent();
                        this.toastService.showToast('Updated Successfully!');
                        return [3 /*break*/, 7];
                    case 5:
                        err_4 = _a.sent();
                        console.log(err_4);
                        return [4 /*yield*/, updateLoader.dismiss()];
                    case 6:
                        _a.sent();
                        this.toastService.showToast(err_4.message);
                        return [3 /*break*/, 7];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    HomeShopItemDetailPage.prototype.onToggleFavoriteBtn = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var index;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.isFavoriteProd = !this.isFavoriteProd;
                        if (this.isFavoriteProd == true) {
                            this.arrFavProds.push(this.itemid);
                        }
                        else {
                            index = this.arrFavProds.indexOf(this.itemid);
                            if (index > -1) {
                                this.arrFavProds.splice(index, 1);
                            }
                        }
                        return [4 /*yield*/, this.firebaseService.addFavoriteProducts(this.arrFavProds.join(','))];
                    case 1:
                        _a.sent();
                        this.event.publish('onfavchange');
                        return [2 /*return*/];
                }
            });
        });
    };
    HomeShopItemDetailPage.prototype.onClickBackBtn = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var chatUserId;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.firebaseService.addFavoriteProducts(this.arrFavProds.join(','))];
                    case 1:
                        _a.sent();
                        this.event.publish('onfavchange');
                        if (this.backPage == 'shopmain') {
                            this.navCtrl.navigateBack('/home/shop');
                        }
                        else if (this.backPage == 'main') {
                            this.navCtrl.navigateBack('/home/main');
                        }
                        else if (this.backPage == 'chat') {
                            chatUserId = localStorage.getItem('posterid');
                            localStorage.removeItem('posterid');
                            this.navCtrl.navigateBack('chat/' + this.itemid + '/' + chatUserId);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    HomeShopItemDetailPage.prototype.getFile = function (imgId) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var options, imgSelectLoader, selectedImg, _a, imgref, _b, _c, err_5;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_d) {
                switch (_d.label) {
                    case 0:
                        options = {
                            quality: 100,
                            destinationType: this.camera.DestinationType.DATA_URL,
                            encodingType: this.camera.EncodingType.JPEG,
                            mediaType: this.camera.MediaType.PICTURE,
                            correctOrientation: true
                        };
                        return [4 /*yield*/, this.loadingCtrl.create({
                                message: 'Loading...'
                            })];
                    case 1:
                        imgSelectLoader = _d.sent();
                        _d.label = 2;
                    case 2:
                        _d.trys.push([2, 8, , 9]);
                        _a = 'data:image/jpeg;base64,';
                        return [4 /*yield*/, this.camera.getPicture(options)];
                    case 3:
                        selectedImg = _a + (_d.sent());
                        imgref = 'img' + imgId;
                        return [4 /*yield*/, imgSelectLoader.present()];
                    case 4:
                        _d.sent();
                        return [4 /*yield*/, this.firebaseService.uploadBoardImage(selectedImg, this.selectedItem.ownerid, imgref)];
                    case 5:
                        _d.sent();
                        _b = this.selectedItem.imgs;
                        _c = imgId;
                        return [4 /*yield*/, this.firebaseService.getBoardImgUrl(this.selectedItem.ownerid, imgref)];
                    case 6:
                        _b[_c] = _d.sent();
                        // this.isImageSelected = true;
                        return [4 /*yield*/, imgSelectLoader.dismiss()];
                    case 7:
                        // this.isImageSelected = true;
                        _d.sent();
                        return [3 /*break*/, 9];
                    case 8:
                        err_5 = _d.sent();
                        imgSelectLoader.dismiss();
                        console.error(err_5);
                        return [3 /*break*/, 9];
                    case 9: return [2 /*return*/];
                }
            });
        });
    };
    HomeShopItemDetailPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home-shop-item-detail',
            template: __webpack_require__(/*! ./home-shop-item-detail.page.html */ "./src/app/page/mainmenu/shopmenu/home-shop-item-detail/home-shop-item-detail.page.html"),
            styles: [__webpack_require__(/*! ./home-shop-item-detail.page.scss */ "./src/app/page/mainmenu/shopmenu/home-shop-item-detail/home-shop-item-detail.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            src_app_services_showtoast_showtoast_service__WEBPACK_IMPORTED_MODULE_6__["ShowtoastService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _services_firebase_service_firebase_service__WEBPACK_IMPORTED_MODULE_4__["FirebaseService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Events"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_7__["Camera"]])
    ], HomeShopItemDetailPage);
    return HomeShopItemDetailPage;
}());



/***/ }),

/***/ "./src/app/page/modal/gallery/gallery.page.html":
/*!******************************************************!*\
  !*** ./src/app/page/modal/gallery/gallery.page.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content fullscreen padding>\n  <div id=\"gallery-modal-btnclose\">\n    <ion-icon name=\"close\" (click)=\"closeGalleryModal()\"></ion-icon>\n  </div>\n  <ion-slides [options]=\"sliderOpts\">\n    <ion-slide *ngFor=\"let img of imgs\">\n      <div class=\"swiper-zoom-container\">\n        <img [src]=\"img\" />\n      </div>\n    </ion-slide>\n  </ion-slides>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/page/modal/gallery/gallery.page.scss":
/*!******************************************************!*\
  !*** ./src/app/page/modal/gallery/gallery.page.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background: rgba(19, 19, 19, 0.45); }\n\n#gallery-modal-btnclose {\n  width: 100%;\n  text-align: right; }\n\n#gallery-modal-btnclose ion-icon {\n    font-size: 24pt;\n    color: white; }\n\nion-slides {\n  height: 80%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2xva2VzaC9saW5ldXAgc291cmNlKDA5LTMwKSAoMikvbGluZXVwKDA5LTMwKS9zcmMvYXBwL3BhZ2UvbW9kYWwvZ2FsbGVyeS9nYWxsZXJ5LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG9DQUFhLEVBQUE7O0FBR2pCO0VBQ0ksV0FBVztFQUNYLGlCQUFpQixFQUFBOztBQUZyQjtJQUlRLGVBQWU7SUFDZixZQUFZLEVBQUE7O0FBSXBCO0VBQ0ksV0FBVyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZS9tb2RhbC9nYWxsZXJ5L2dhbGxlcnkucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xuICAgIC0tYmFja2dyb3VuZDogcmdiYSgxOSwgMTksIDE5LCAwLjQ1KTtcbn1cblxuI2dhbGxlcnktbW9kYWwtYnRuY2xvc2Uge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIGlvbi1pY29uIHtcbiAgICAgICAgZm9udC1zaXplOiAyNHB0O1xuICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgfVxufVxuXG5pb24tc2xpZGVzIHtcbiAgICBoZWlnaHQ6IDgwJTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/page/modal/gallery/gallery.page.ts":
/*!****************************************************!*\
  !*** ./src/app/page/modal/gallery/gallery.page.ts ***!
  \****************************************************/
/*! exports provided: GalleryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GalleryPage", function() { return GalleryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var GalleryPage = /** @class */ (function () {
    function GalleryPage(modalCtrl) {
        this.modalCtrl = modalCtrl;
        this.sliderOpts = {
            zoom: {
                maxRatio: 3
            }
        };
    }
    GalleryPage.prototype.ngOnInit = function () {
    };
    GalleryPage.prototype.closeGalleryModal = function () {
        this.modalCtrl.dismiss();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('imgs'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GalleryPage.prototype, "imgs", void 0);
    GalleryPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-gallery',
            template: __webpack_require__(/*! ./gallery.page.html */ "./src/app/page/modal/gallery/gallery.page.html"),
            styles: [__webpack_require__(/*! ./gallery.page.scss */ "./src/app/page/modal/gallery/gallery.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
    ], GalleryPage);
    return GalleryPage;
}());



/***/ }),

/***/ "./src/app/page/modal/modal.module.ts":
/*!********************************************!*\
  !*** ./src/app/page/modal/modal.module.ts ***!
  \********************************************/
/*! exports provided: ModalModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalModule", function() { return ModalModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _gallery_gallery_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./gallery/gallery.page */ "./src/app/page/modal/gallery/gallery.page.ts");
/* harmony import */ var _writemessage_writemessage_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./writemessage/writemessage.page */ "./src/app/page/modal/writemessage/writemessage.page.ts");







var ModalModule = /** @class */ (function () {
    function ModalModule() {
    }
    ModalModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _gallery_gallery_page__WEBPACK_IMPORTED_MODULE_5__["GalleryPage"],
                _writemessage_writemessage_page__WEBPACK_IMPORTED_MODULE_6__["WritemessagePage"],
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"]
            ],
            exports: [
                _gallery_gallery_page__WEBPACK_IMPORTED_MODULE_5__["GalleryPage"],
                _writemessage_writemessage_page__WEBPACK_IMPORTED_MODULE_6__["WritemessagePage"],
            ],
            entryComponents: [
                _gallery_gallery_page__WEBPACK_IMPORTED_MODULE_5__["GalleryPage"],
                _writemessage_writemessage_page__WEBPACK_IMPORTED_MODULE_6__["WritemessagePage"],
            ]
        })
    ], ModalModule);
    return ModalModule;
}());



/***/ }),

/***/ "./src/app/page/modal/writemessage/writemessage.page.html":
/*!****************************************************************!*\
  !*** ./src/app/page/modal/writemessage/writemessage.page.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <ion-header no-border>\n  <ion-toolbar>\n    <ion-title></ion-title>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-content>\n  <div style=\"height: 40px;margin-left: 20px;display: flex;align-items: center;\"><h3>{{title}}</h3></div>\n  <ion-item>\n    <ion-label>\n      To:\n    </ion-label>\n    <ion-input [value]=\"supportEmail\" disabled=true></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-textarea [(ngModel)]=\"reportMessage\" rows=5></ion-textarea>\n  </ion-item>\n\n  <ion-item lines=\"none\" id=\"writemessage-btnsend\" class=\"ion-margin-top\">\n    <div>\n      <ion-button (click)=\"onClickSendBtn()\">Send</ion-button>\n    </div>\n  </ion-item>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/page/modal/writemessage/writemessage.page.scss":
/*!****************************************************************!*\
  !*** ./src/app/page/modal/writemessage/writemessage.page.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#writemessage-btnsend div {\n  width: 100%;\n  text-align: center; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2xva2VzaC9saW5ldXAgc291cmNlKDA5LTMwKSAoMikvbGluZXVwKDA5LTMwKS9zcmMvYXBwL3BhZ2UvbW9kYWwvd3JpdGVtZXNzYWdlL3dyaXRlbWVzc2FnZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFUSxXQUFXO0VBQ1gsa0JBQWtCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlL21vZGFsL3dyaXRlbWVzc2FnZS93cml0ZW1lc3NhZ2UucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI3dyaXRlbWVzc2FnZS1idG5zZW5kIHtcbiAgICBkaXYge1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/page/modal/writemessage/writemessage.page.ts":
/*!**************************************************************!*\
  !*** ./src/app/page/modal/writemessage/writemessage.page.ts ***!
  \**************************************************************/
/*! exports provided: WritemessagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WritemessagePage", function() { return WritemessagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_email_composer_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/email-composer/ngx */ "./node_modules/@ionic-native/email-composer/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_showtoast_showtoast_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/showtoast/showtoast.service */ "./src/app/services/showtoast/showtoast.service.ts");
/* harmony import */ var src_app_constants_constants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/constants/constants */ "./src/app/constants/constants.ts");






var WritemessagePage = /** @class */ (function () {
    function WritemessagePage(modalCtrl, emailComposer, toastService) {
        this.modalCtrl = modalCtrl;
        this.emailComposer = emailComposer;
        this.toastService = toastService;
        this.reportMessage = '';
        this.title = '';
        this.supportEmail = src_app_constants_constants__WEBPACK_IMPORTED_MODULE_5__["SUPPORTEMAIL"];
    }
    WritemessagePage.prototype.ngOnInit = function () {
        var subject = '';
        if (this.reporttype == 0) {
            subject = 'Report Abuse';
        }
        else if (this.reporttype == 1) {
            subject = 'Report User';
        }
        else if (this.reporttype == 2) {
            subject = 'Suggestions';
        }
        else {
            subject = 'Missing Beach?';
        }
        this.title = subject;
    };
    WritemessagePage.prototype.onClickSendBtn = function () {
        var subject = '';
        if (this.reporttype == 0) {
            subject = 'Report Abuse';
        }
        else if (this.reporttype == 1) {
            subject = 'Report User';
        }
        else if (this.reporttype == 2) {
            subject = 'Suggestions';
        }
        else {
            subject = 'Missing Beach?';
        }
        var email = {
            to: this.supportEmail,
            cc: '',
            bcc: [],
            attachments: [],
            subject: subject,
            body: this.reportMessage,
            isHtml: false
        };
        this.emailComposer.open(email);
        this.modalCtrl.dismiss();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('reporttype'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], WritemessagePage.prototype, "reporttype", void 0);
    WritemessagePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-writemessage',
            template: __webpack_require__(/*! ./writemessage.page.html */ "./src/app/page/modal/writemessage/writemessage.page.html"),
            styles: [__webpack_require__(/*! ./writemessage.page.scss */ "./src/app/page/modal/writemessage/writemessage.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
            _ionic_native_email_composer_ngx__WEBPACK_IMPORTED_MODULE_2__["EmailComposer"],
            src_app_services_showtoast_showtoast_service__WEBPACK_IMPORTED_MODULE_4__["ShowtoastService"]])
    ], WritemessagePage);
    return WritemessagePage;
}());



/***/ }),

/***/ "./src/app/services/showtoast/showtoast.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/services/showtoast/showtoast.service.ts ***!
  \*********************************************************/
/*! exports provided: ShowtoastService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShowtoastService", function() { return ShowtoastService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var ShowtoastService = /** @class */ (function () {
    function ShowtoastService(toastCtrl) {
        this.toastCtrl = toastCtrl;
    }
    ShowtoastService.prototype.showToast = function (message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: message,
                            duration: 3000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    ShowtoastService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]])
    ], ShowtoastService);
    return ShowtoastService;
}());



/***/ })

}]);
//# sourceMappingURL=shopmenu-home-shop-item-detail-home-shop-item-detail-module.js.map