import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './home.page';
import { AuthGuard } from 'src/app/services/user/auth.guard';
import { ModalModule } from 'src/app/page/modal/modal.module';

const routes: Routes = [
  {
    path: '',
    component: HomePage,
    children: [
      {
        path: 'main',
        children: [
          {
            path: '',
            loadChildren: '../home-main/home-main.module#HomeMainPageModule',
            canActivate: [ AuthGuard ]
          },
          {
            path: 'locationdetail/:itemid/:name',
            loadChildren: '../exploremenu/home-explore-location-detail/home-explore-location-detail.module#HomeExploreLocationDetailPageModule',
            canActivate: [AuthGuard]
           },
        {
          path: 'explore-report/:locationid/:name/:wavesize/:wavequality',
          loadChildren: '../exploremenu/explore-report/explore-report.module#ExploreReportPageModule',
          canActivate: [AuthGuard]
      },
      {
        path: 'discussion-detail/:discussionid/:discussiontitle/:backpage/:isnew',
        loadChildren: '../discussionmenu/discussion-detail/discussion-detail.module#DiscussionDetailPageModule',
        canActivate: [AuthGuard]
      },
      {
        path: 'chat/:itemid/:poster',
        loadChildren: '../activitymenu/activity-detail/activity-detail.module#ActivityDetailPageModule',
        canActivate: [AuthGuard]
       },
      {
        path: 'shopitemdetail/:itemid/:backpage',
        loadChildren: '../shopmenu/home-shop-item-detail/home-shop-item-detail.module#HomeShopItemDetailPageModule',
        canActivate: [AuthGuard]
       },  
    ]
      },
      {
        path: 'explore',
        children: [
          {
            path: '',
            loadChildren: '../exploremenu/home-explore/home-explore.module#HomeExplorePageModule',
            canActivate: [ AuthGuard ]
          },
          {
            path: 'locationdetail/:itemid/:name',
            loadChildren: '../exploremenu/home-explore-location-detail/home-explore-location-detail.module#HomeExploreLocationDetailPageModule',
            canActivate: [AuthGuard]
        },
        {
          path: 'explore-report/:locationid/:name/:wavesize/:wavequality',
          loadChildren: '../exploremenu/explore-report/explore-report.module#ExploreReportPageModule',
          canActivate: [AuthGuard]
      }
        ]
      },
      {
        path: 'activity',
        children: [
          {
            path: '',
            loadChildren: '../activitymenu/home-activity/home-activity.module#HomeActivityPageModule',
            canActivate: [ AuthGuard ]
          }
        ]
      },
      {
        path: 'shop',
        children: [
          {
            path: '',
            loadChildren: '../shopmenu/home-shop/home-shop.module#HomeShopPageModule',
            canActivate: [ AuthGuard ]
          },
          {
            path: 'chat/:itemid/:poster',
            loadChildren: '../activitymenu/activity-detail/activity-detail.module#ActivityDetailPageModule',
            canActivate: [AuthGuard]
           },
           {
            path: 'shopitemdetail/:itemid/:backpage',
            loadChildren: '../shopmenu/home-shop-item-detail/home-shop-item-detail.module#HomeShopItemDetailPageModule',
            canActivate: [AuthGuard]
           },
           {   
            path: 'discussion-detail/:discussionid/:discussiontitle/:backpage/:isnew',
            loadChildren: '../discussionmenu/discussion-detail/discussion-detail.module#DiscussionDetailPageModule',
            canActivate: [AuthGuard]
          },
          {
            path: 'home-shop-newitem',
            loadChildren: '../shopmenu/home-shop-newitem/home-shop-newitem.module#HomeShopNewitemPageModule',
            canActivate: [AuthGuard]
        },
        ]
      },
      {
        path: 'setting',
        children: [
          {
            path: '',
            loadChildren: '../accountmenu/account/account.module#AccountPageModule',
            canActivate: [ AuthGuard ]
          }
        ]
      },
      {
        path: 'discussion',
        children: [
          {
            path: '',
            loadChildren: '../discussionmenu/discussion/discussion.module#DiscussionPageModule',
            canActivate: [ AuthGuard ]
          },
          {
            path: 'discussion-detail/:discussionid/:discussiontitle/:backpage/:isnew',
            loadChildren: '../discussionmenu/discussion-detail/discussion-detail.module#DiscussionDetailPageModule',
            canActivate: [AuthGuard]
          },
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    ModalModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class HomePageRoutingModule {}
