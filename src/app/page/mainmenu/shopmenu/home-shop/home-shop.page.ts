import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {LoadingController, Events, NavController, PopoverController} from '@ionic/angular';

import {FirebaseService} from '../../../../services/firebase-service/firebase.service';
import {SurfBoard, UserData} from '../../../interface/interface';
import { MyutilsService } from 'src/app/services/myutils/myutils.service';
import { ActivitypopoverPage } from '../../activitymenu/activitypopover/activitypopover.page';

@Component({
    selector: 'app-home-shop',
    templateUrl: './home-shop.page.html',
    styleUrls: ['./home-shop.page.scss'],
})
export class HomeShopPage implements OnInit {

    public itemSearchQuery = '';
    private shopItems: SurfBoard[];
    public searchResults: SurfBoard[];
    private userInfo: UserData = {};
    private boardLoader = null;

    /**
     * selected segments
     *  0: All
     *  1: My Items
     *  2: Saved Items
     */
    public selectedSegment:any = '0';
    private arrMyItems = [];
    private arrMyFavItems = [];


    private currentUserId: string;
  private currentUserInfo: UserData = {};

  private messageDataRef = null;
  private messageDataListener = null;
  private allContacts = [];
  private contacts = [];
  private historyLoader = null;
  private arrHideLists = [];

    constructor(
        private router: Router,
        private firebaseService: FirebaseService,
        private loadingCtrl: LoadingController,
        private event: Events,
        private myUtils: MyutilsService,
        private popoverCtrl: PopoverController,
        private navCtrl:NavController
    ) {
        this.shopItems = [];
        this.event.subscribe('onfavchange', data => {
            this.initPage();
        });
        this.event.subscribe('onchangehidelist', (data) => {
            this.initPageC();
          });
    }

    ngOnInit() {
        this.currentUserId = this.firebaseService.getUID();
        console.log(this.currentUserId);
    }

    ionViewWillEnter() {
        this.initPage();
        this.initPageC();
    }

    async initPage() {
        this.shopItems = [];
        this.searchResults = [];
        this.arrMyItems = [];
        this.arrMyFavItems = [];
        this.selectedSegment = '0';
        await this.getUserProfile();
        this.getAllSurfBoards();
    }


    async initPageC() {
        this.messageDataRef = null;
        this.messageDataListener = null;
    
        this.arrHideLists = [];
        if(localStorage.getItem("hidelist") !== null && localStorage.getItem("hidelist") !== "") {
          const strHideLists = localStorage.getItem("hidelist");
          this.arrHideLists = strHideLists.split(",");
        }
    
        await this.getCurrentUserInfo();
        await this.getChatHistory();
    
        this.event.publish("onReceiveNotification", {isClear: true});
      }

      
    async getCurrentUserInfo() {
        this.historyLoader = await this.loadingCtrl.create({
          message: "Loading..."
        });
        await this.historyLoader.present();
        try {
          const currentUserSnapshot = await this.firebaseService.getUserProfile(this.currentUserId);
          this.currentUserInfo = {...currentUserSnapshot.val()};
          console.log(this.currentUserInfo);
        } catch(err) {
          this.historyLoader.dismiss();
          console.log(err);
        }
      }
    
      async getChatHistory() {
        var self = this;
        this.messageDataRef = this.firebaseService.getAllChatHistoryRef();
        this.messageDataListener = this.messageDataRef.on("value", snapshot => {
          self.contacts = [];
          self.allContacts = [];
          snapshot.forEach(function(childSnapshot) {
            childSnapshot.forEach(function(valueSnapshot) {
              let users = valueSnapshot.key.split("-");
              let tmpCUIndex = users.findIndex(item => item == self.currentUserId);
              let tmpMessageObj = valueSnapshot.val();
              let boardName = "";
              let boardImg = "";
              if(tmpMessageObj !== null) {
                boardName = tmpMessageObj[Object.keys(tmpMessageObj)[0]].boardName;
                boardImg = tmpMessageObj[Object.keys(tmpMessageObj)[0]].contentimg;
              }
              if(tmpCUIndex > -1) {
                let otherUID = tmpCUIndex==0?users[1]:users[0];
                console.log(otherUID);
                let isExists = self.contacts.findIndex(item => item == otherUID);
                if(isExists == -1) {
                  self.getUserData(otherUID, childSnapshot.key, boardName, boardImg);
                }
              }
            });
          });
          self.myUtils.setChatHistory(this.allContacts);
          self.historyLoader.dismiss();
        });
      }
    
      async getUserData(uid, itemid, boardName, boardImg) {
        try {
          const userInfoSnapshot = await this.firebaseService.getUserProfile(uid);
          let isHidden = this.arrHideLists.indexOf(itemid);
          let isAllExists = this.allContacts.findIndex(x => x.itemid === itemid);
          let isContactExists = this.contacts.findIndex(x => x.itemid === itemid);
          console.log({...userInfoSnapshot.val()});
          if(Object.keys({...userInfoSnapshot.val()}).length>0){
            if(isAllExists == -1) {
              this.allContacts.push({...userInfoSnapshot.val(), itemid: itemid, boardName: boardName, boardImg: boardImg, name: (userInfoSnapshot.val().fname?userInfoSnapshot.val().fname:'') + " " + (userInfoSnapshot.val().lname?userInfoSnapshot.val().lname:'')});
            }
            if(isHidden == -1 && isContactExists == -1) {
              this.contacts.push({...userInfoSnapshot.val(), itemid: itemid, boardName: boardName, boardImg: boardImg, name: (userInfoSnapshot.val().fname?userInfoSnapshot.val().fname:'') + " " + (userInfoSnapshot.val().lname?userInfoSnapshot.val().lname:'')});
            }
          }
         
        } catch(err) {
          console.log(err);
        }
      }
    
      viewChatRoom(contactid) {
        this.stopActivityListener();
        this.navCtrl.navigateForward('/home/shop/chat/' + this.contacts[contactid].itemid + "/" + this.contacts[contactid].uid);
      }
    
      onClickMessageControlBtn(evt, itemid) {
        evt.stopPropagation();
        this.arrHideLists.push(itemid);
        let index = this.contacts.findIndex(x=>x.itemid==itemid);
        if(index > -1) {
          this.contacts.splice(index, 1);
        }
    
        localStorage.setItem("hidelist", this.arrHideLists.join(","));
      }
    
      async onClickPopOver() {
        this.router.navigateByUrl('/hidelist');
        // const popover = await this.popoverCtrl.create({
        //   component: ActivitypopoverPage,
        //   event
        // });
        // await popover.present();
      }

      ionViewWillLeave() {
        this.stopActivityListener();
      }
    
      stopActivityListener() {
        if(this.messageDataListener != null) {
          this.messageDataRef.off("value", this.messageDataListener);
          this.messageDataListener = null;
          this.messageDataRef = null;
        }
      }
    async getUserProfile() {
        this.boardLoader = await this.loadingCtrl.create({
            message: 'Loading...'
        });
        await this.boardLoader.present();
        const uid = this.firebaseService.getUID();
        try {
            const userInfoSnapshot = await this.firebaseService.getUserProfile(uid);
            this.userInfo = userInfoSnapshot.val();
        } catch (err) {
            console.log(err);
        }
    }

    async getAllSurfBoards() {
        const self = this;
        try {
            const allBoardsSnapshot = await this.firebaseService.getAllSurfBoard();
            allBoardsSnapshot.forEach(function (items) {
                console.log({...items.val()});
                self.shopItems.push({...items.val(), id: items.key});
            });
            self.searchResults = self.shopItems;
            this.boardLoader.dismiss();
            this.filterArray();
        } catch (err) {
            console.log(err);
            this.boardLoader.dismiss();
        }
    }

    searchItems() {
        const keyWord = this.itemSearchQuery.toLowerCase();
        let arrItems = [];
        if (this.selectedSegment === '0') {
            arrItems = this.shopItems;
        } else if (this.selectedSegment === '1') {
            arrItems = this.arrMyItems;
        } else if (this.selectedSegment === '2') {
            arrItems = this.arrMyFavItems;
        }
        if (keyWord === '') {
            this.searchResults = arrItems;
        } else {
            if (this.shopItems.length > 0) {
                this.searchResults = [];
                for (let i = 0; i < arrItems.length; i++) {
                    const itemDescription = arrItems[i].title.toLowerCase();
                    if (itemDescription.includes(keyWord) === true) {
                        this.searchResults.push(arrItems[i]);
                    }
                }
                this.filterArray();
            }
        }
    }

    viewNewSurfboardPage() {
        this.router.navigateByUrl('/home/shop/home-shop-newitem');
    }

    viewItemDetailPage(itemid) {
      sessionStorage.setItem("fromHome","0");
        this.router.navigateByUrl('/home/shop/shopitemdetail/' + itemid + '/shopmain');
    }

    segmentChanged() {
        console.log(this.userInfo);
        const arrUserFavItems = this.userInfo.favorite.products.split(',');
        
        const uid = this.userInfo.uid;
        if (this.selectedSegment === '0') {
            this.searchResults = this.shopItems;
        } else if (this.selectedSegment === '1') {
            this.searchResults = this.shopItems.filter(x => x.ownerid === uid);
            this.arrMyItems = this.searchResults;
        } else if (this.selectedSegment === '2') {
            this.searchResults = this.shopItems.filter(x => arrUserFavItems.indexOf(x.id) > -1);
            this.arrMyFavItems = this.searchResults;
        }
        console.log(this.searchResults);
        this.filterArray();
        if(this.selectedSegment === '3'){
            this.initPageC();
        }
    }
    filterArray(){
      this.filterArrayinMiles();
        
    }
    filterArrayinMiles(){
      console.log(this.currentUserInfo.noti.anywhere);
      if(!this.currentUserInfo.noti.anywhere && this.selectedSegment == 0){
        this.searchResults = this.searchResults.filter((el)=>{
          return this.getDistanceFromLatLonInKm(this.currentUserInfo.location.lat,this.currentUserInfo.location.long,el.location.lat,el.location.lat) <= (this.currentUserInfo.noti.range ? this.currentUserInfo.noti.range : 50);
        });
      } 
      
      setTimeout(() => {
        this.searchResults.sort(function (x, y) {
          return y.createdat - x.createdat;
        });
      }, 100);
      
    }
    getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
      var R = 6371; // Radius of the earth in km
      var dLat = this.deg2rad(lat2-lat1);  // deg2rad below
      var dLon = this.deg2rad(lon2-lon1); 
      var a = 
        Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) * 
        Math.sin(dLon/2) * Math.sin(dLon/2)
        ; 
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
      var d = R * c; // Distance in km
      return d/1.6;
    }
    
    deg2rad(deg) {
      return deg * (Math.PI/180)
    }

}
