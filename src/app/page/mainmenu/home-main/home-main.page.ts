import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { FirebaseService } from 'src/app/services/firebase-service/firebase.service';
import { WAVEQUALITIES } from 'src/app/constants/constants';
import { UserData } from '../../interface/interface';
import { Events, LoadingController, ModalController } from '@ionic/angular';
import { MyutilsService } from 'src/app/services/myutils/myutils.service';
import { GalleryPage } from '../../modal/gallery/gallery.page';
import { StreamingVideoOptions, StreamingMedia } from '@ionic-native/streaming-media/ngx';

@Component({
  selector: 'app-home-main',
  templateUrl: './home-main.page.html',
  styleUrls: ['./home-main.page.scss'],
})
export class HomeMainPage implements OnInit {

  private currentUserId = '';
  private arrWaveQualities = WAVEQUALITIES;

  /**
   * Weather Reports
   */
  private weatherReportdataRef = null;
  private weatherReportdataListener = null;
  private weatherReports:any;
  private weatherReportsFilter: any;
  readmore = false;
  readmoreClicked = false;
  /**
   * Board Reports
   */
  private boardDataRef = null;
  private boardDataListener = null;
  private allNewSurfBoards = [];

  /**
   * Recent Discussions
   */
  private discussionDetailRef = null;
  private discussionDetailListener = null;
  private arrDiscussions = [];
  private arrDiscussionsFilter = [];
  /**
   * User Info
   */
  private userInfo: UserData = {};
  private arrFavBeachesId = [];

  /**
   * Event Listener
   */
  authListener = null;

  private countLoader = 0;
  private dataLoader = null;

  constructor(
    private firebaseService: FirebaseService,
    private router: Router,
    private event: Events,
    private loadingCtrl: LoadingController,
    private  myUtils: MyutilsService,
    private modalCtrl: ModalController,
    private streamingMedia: StreamingMedia,
    ) {
    this.authListener = this.event.subscribe('onAuth', (data) => {
      console.log('Auth Called!');
      this.initPage();
    });
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    /**
     * Init page
     */
    this.initPage();
  }

  async initPage() {
    this.countLoader = 0;
    this.currentUserId = this.firebaseService.getUID();

    /**
     * Weather
     */
    this.weatherReportdataRef = null;
    this.weatherReportdataListener = null;
    this.weatherReports = [];

    /**
     * Board
     */
    this.boardDataRef = null;
    this.boardDataListener = null;
    this.allNewSurfBoards = [];

    /**
     * Discussion
     */
    this.discussionDetailRef = null;
    this.discussionDetailListener = null;
    await this.getUserInfo();
    this.dataLoader = await this.loadingCtrl.create({
      message: 'Please wait...'
    });
    await this.dataLoader.present();
    this.getAllLocationReports();
    this.getNewBoardItems();
    this.getDiscussionContents();
  }

  async getUserInfo() {
    const userInfoSnapshot = await this.firebaseService.getUserProfile(this.currentUserId);
    this.userInfo = userInfoSnapshot.val();
  }

  async getAllLocationReports() {
    const self = this;
    this.arrFavBeachesId = this.userInfo.favorite.beaches.split(',');
    this.weatherReportdataRef = this.firebaseService.getAllWeatherReports();
    this.weatherReportdataListener = this.weatherReportdataRef.on('value', snapshot => {
      
      self.weatherReports = [];
      const tmpArr = [];
      this.arrFavBeachesId.forEach((element)=>{
      snapshot.forEach(function(childSnapshot) {
        const favIndex = (childSnapshot.key === element);
        if (favIndex) {
          childSnapshot.forEach(function(valueSnapshot) {
            tmpArr.push({...valueSnapshot.val()});
          });
        }
      });
    });
      tmpArr.reverse();
      // console.log(tmpArr);
      
      let tempArray2 = [];
      // if(tmpArr.length>4){
        tmpArr.forEach(function(item){
          var i = tempArray2.findIndex(x => x.location.name == item.location.name && x.locationId == item.locationId);
          if(i <= -1){
            tempArray2.push(item);
          }
        });
      // }  
    //   console.log(tempArray2);     
    //  if(tempArray2.length<4){  
    //   tmpArr.forEach(function(item){
    //     var i = tempArray2.findIndex(x => (x.id == item.id && x.locationId == item.locationId));
    //     if(i <= -1){
    //       tempArray2.push(item);
    //     }
    //   });
    //  }
     
    //  console.log(tempArray2.length);
    if(tempArray2.length>=4){
      self.weatherReports = tempArray2.sort((a,b)=> b.createdat - a.createdat).slice(0, 4);
      this.readmore = (tempArray2.length>4? true : false);
    }else{
      self.weatherReports = tempArray2.slice(0, tempArray2.length).sort((a,b)=> b.createdat - a.createdat);
    }

    self.weatherReportsFilter = tempArray2.sort((a,b)=> b.createdat - a.createdat);
    // tmpArr;     
      console.log(tmpArr);
      console.log(self.weatherReports);
      
      self.countLoader++;
      if (self.countLoader == 2) {
        self.dismissDataLoader();
      }
    });
  }



  async onClickVideoAttachment(ev,path, type) {
    ev.stopPropagation();
    if (type == 0) {
        const galleryModal = await this.modalCtrl.create({
            component: GalleryPage,
            componentProps: {
                imgs: [path]
            },
            cssClass: 'gallery-modal'
        });
        await galleryModal.present();
    } else {
        const streamingOptions: StreamingVideoOptions = {
            successCallback: () => {
                console.log('Video played');
            },
            errorCallback: (e) => {
                console.log('Error streaming');
            },
            orientation: 'portrait',
            shouldAutoClose: true,
            controls: false
        };

        this.streamingMedia.playVideo(path, streamingOptions);
    }
}


  getNewBoardItems() {
    const self = this;
    // const itemRange = Number(this.userInfo.noti.range);
    this.boardDataRef = this.firebaseService.getAllSurfBoardRealtime();
    this.boardDataListener = this.boardDataRef.on('value', snapshot => {
      self.allNewSurfBoards = [];
      snapshot.forEach(function(boardSnapshot) {
        const distance = self.myUtils.distance(self.userInfo.location.lat, self.userInfo.location.long, boardSnapshot.val().location.lat, boardSnapshot.val().location.long, 'N');
        // if (itemRange >= Number(distance)) {
          self.allNewSurfBoards.push({...boardSnapshot.val()});
        // }
      });
      self.countLoader++;
      if (self.countLoader == 2) {
        self.dismissDataLoader();
      }
      self.allNewSurfBoards = self.allNewSurfBoards.sort((a,b)=> (b.createdat - a.createdat))
    });
  }

  setDefault(item){
    console.log(item);
    localStorage.setItem("defaultBeach",JSON.stringify(item));
  }
  getDiscussionContents() {
    const self = this;
    var favDiscussions =[];
    if(this.userInfo.favorite.discussions){
      favDiscussions = this.userInfo.favorite.discussions.split(',');
    }
   
    this.discussionDetailRef = this.firebaseService.getAllDiscussionContents();
    this.discussionDetailListener = this.discussionDetailRef.on('value', snapshot => {
      this.arrDiscussions = [];
      this.arrDiscussionsFilter = [];
      snapshot.forEach(function(childSnapshot) {
        const isFavDiscussion = favDiscussions.indexOf(childSnapshot.key);
        if (isFavDiscussion > -1) {
          const tmpArr = [];
          childSnapshot.forEach(function(valSnapshot) {
            tmpArr.push({...valSnapshot.val(), discussionid: childSnapshot.key});
          });
          if (tmpArr.length > 0) {
            self.arrDiscussions.push(tmpArr[tmpArr.length - 1]);
          }
          
        }    
      });

      if(this.arrDiscussions.length>4){
        this.arrDiscussionsFilter = this.arrDiscussions.slice(0, 4)
      }else{
        this.arrDiscussionsFilter = this.arrDiscussions;
      }
      this.arrDiscussionsFilter.sort(function (x, y) {
        return y.createdat - x.createdat;
    });
    });
  }

  onClickDiscussionItem(item) {
    this.router.navigateByUrl('/home/main/discussion-detail/' + item.discussionid + '/' + item.title + '/home/1');
  }


  showMore(val){
    if(val == 1){
         this.arrDiscussionsFilter = this.arrDiscussions;
    }else{
        this.arrDiscussionsFilter = this.arrDiscussions.slice(0, 4)
    }
    this.arrDiscussionsFilter.sort(function (x, y) {
      return y.createdat - x.createdat;
  });
  }
  dismissDataLoader() {
    if (this.dataLoader !== null) {
      this.dataLoader.dismiss();
      this.dataLoader = null;
    }
  }
  show(val){
   this.readmoreClicked = (val == 1?true:false)
  }
  viewItemDetail(itemid) {
    sessionStorage.setItem("fromHome","1");
    this.router.navigateByUrl('/home/main/shopitemdetail/' + this.allNewSurfBoards[itemid].id + '/main');
  }

  onClickReportItem(report) {
    console.log("Check index",report);
    this.router.navigateByUrl('/home/main/locationdetail/' + report.locationId+ '/' + btoa(report.location.name));
  }

  onClickFavBeach(beachIndex) {
    this.router.navigateByUrl('/locationdetail/' + this.arrFavBeachesId[beachIndex]);
  }
       
  ionViewWillLeave() {
    if (this.weatherReportdataListener !== null) {
      this.weatherReportdataRef.off('value', this.weatherReportdataListener);
      this.weatherReportdataListener = null;
      this.weatherReportdataRef = null;
    }
    if (this.boardDataListener !== null) {
      this.boardDataRef.off('value', this.boardDataListener);
      this.boardDataListener = null;
      this.boardDataRef = null;
    }
    if (this.authListener !== null) {
      this.event.unsubscribe('onAuth', this.authListener);
    }
  }
}


interface ReportsInfo {
  id: string;
  uid: string;
  username: string;
  reporttxt: string;
  location: {
    lat: number,
    long: number,
    name: string
  };
  locationId: string;
  waveHeight: string;
  waveQuality: string;
  createdat: string;
}
