import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController, ActionSheetController } from '@ionic/angular';
import * as firebase from 'firebase/app';
import { File } from '@ionic-native/file/ngx';
import { MediaCapture, CaptureVideoOptions, CaptureImageOptions, MediaFile } from '@ionic-native/media-capture/ngx';
import { Media, MediaObject } from '@ionic-native/media/ngx';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

import { FirebaseService } from '../../../../services/firebase-service/firebase.service';
import { WAVEQUALITIES, MTOFRATIO } from 'src/app/constants/constants';
import { UserData } from '../../../interface/interface';
import { ShowtoastService } from 'src/app/services/showtoast/showtoast.service';
import { MyutilsService } from 'src/app/services/myutils/myutils.service';
import { VideoEditor,CreateThumbnailOptions } from '@ionic-native/video-editor/ngx';
@Component({
  selector: 'app-explore-report',
  templateUrl: './explore-report.page.html',
  styleUrls: ['./explore-report.page.scss'],
})
export class ExploreReportPage implements OnInit {
  @ViewChild('myVideo') myVideo: any;

  private locationId: string;
  private locationName: string;
  private currentUserInfo: UserData = {};
  private waveQualities = WAVEQUALITIES;

  private reportWaveQuality = '';
  private reportWaveSize = '';
  private reportTxt = '';
  private postFile = '';
   postFile2 = '';
  /**
     * type
     *  0: Image
     *  1: Video
     */
  private fileType = 0;
  private videoFile = {
    name: '',
    path: ''
  };

  constructor(
    private route: ActivatedRoute,
    private navCtrl: NavController,
    private firebaseService: FirebaseService,
    private myUtils: MyutilsService,
    private loadingCtrl: LoadingController,
    private actionSheetCtrl: ActionSheetController,
    private toastService: ShowtoastService,
    private file: File,
    private camera: Camera,
    private mediaCapture: MediaCapture,
    private media: Media,
    private streamingMedia: StreamingMedia,
    private changeDetector: ChangeDetectorRef,
    private videoEditor:VideoEditor
  ) {

  }

  ngOnInit() {
    this.locationId = this.route.snapshot.paramMap.get('locationid');
    this.locationName = this.route.snapshot.paramMap.get('name');
    this.reportWaveSize = (Number(this.route.snapshot.paramMap.get('wavesize')) * MTOFRATIO).toString();
    this.reportWaveQuality = this.route.snapshot.paramMap.get('wavequality');
    this.initPage();
  }

  async initPage() {
    const pageLoader = await this.loadingCtrl.create({
      message: 'Loading...'
    });
    await pageLoader.present();
    try {
      // console.log(firebase.auth().currentUser.uid);
      const profileInfoSnapshot = await this.firebaseService.getUserProfile(this.firebaseService.getUID());
      console.log({...profileInfoSnapshot.val()}); 
      this.currentUserInfo = { ...profileInfoSnapshot.val() };
      pageLoader.dismiss();
    } catch (err) {
      pageLoader.dismiss();
      console.log(err);
    }
  }

  async postReport() {
    const reportData = {
      id: '',
      uid: this.currentUserInfo.uid,
      username: this.currentUserInfo.fname + ' ' + this.currentUserInfo.lname,
      reporttxt: this.reportTxt,
      location: {
        lat: this.myUtils.currentUserLocation.lat,
        long: this.myUtils.currentUserLocation.long,
        name: this.locationName,
      },
      locationId: this.locationId,
      reporterLoc: {
        lat: this.myUtils.currentUserLocation.lat,
        long: this.myUtils.currentUserLocation.long
      },
      waveHeight: this.reportWaveSize,
      waveQuality: this.reportWaveQuality,
      createdat: firebase.database.ServerValue.TIMESTAMP,
      postfile: {
        data: '',
        type: this.fileType,
        thumb :''
      },
      review: 0
    };
    console.log(reportData); 
    const reportLoader = await this.loadingCtrl.create({
      message: 'Please wait...'
    });
    await reportLoader.present();

    try {
      await this.firebaseService.setWeatherReport(this.postFile, reportData,this.postFile2);
      this.postFile = '';
      reportLoader.dismiss();
      this.toastService.showToast('Report is posted');
      
      this.navCtrl.pop();
    } catch (err) {
      reportLoader.dismiss();
      this.toastService.showToast(err.message);
    }
  }

  async onClickCameraIcon() {
    this.fileType = 0;
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'File',
      buttons: [{
        text: 'Take Picture',
        icon: 'aperture',
        handler: () => {
          this.fileType = 0;
          this.takePicture();
        }
      }, {
        text: 'Record Video',
        icon: 'videocam',
        handler: () => {
          this.fileType = 1;
          this.videoFile = {
            name: '',
            path: ''
          };
          this.captureVideo();
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  async takePicture() {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    };

    try {
      const imageData = await this.camera.getPicture(options);
      this.postFile = 'data:image/jpeg;base64,' + imageData;
    } catch (err) {
      console.log(err);
    }
  }

  async captureVideo() {
    const videoLoader = await this.loadingCtrl.create({
      message: 'Loading...'
    });
    try {

      var captureSuccess = async (mediaFiles) =>{
        var i, path, len;
         for (i = 0, len = mediaFiles.length; i < len; i += 1) {
            path = mediaFiles[i].fullPath;
             //   const capturedOptions: CaptureVideoOptions = {
    //     limit: 1,
    //     quality: 0
    //   };
    //   const res = await this.mediaCapture.captureVideo(capturedOptions);
      
      await videoLoader.present();
      const capturedFile = path;
      console.log(path);
      console.log(capturedFile.substring(capturedFile.lastIndexOf("/")+1));
      console.log(capturedFile.substring(0,capturedFile.lastIndexOf("/")));
      this.videoFile = {
        name: capturedFile.substring(capturedFile.lastIndexOf("/")+1),
        path: capturedFile.substring(0,capturedFile.lastIndexOf("/"))
      };
      // console.log(capturedFile);
      // const dir = capturedFile['localURL'].split('/');
    //   dir.pop();
    //   const fromDirectory = dir.join('/');
    //   // console.log(this.videoFile);
      this.changeDetector.detectChanges();

    //   const streamingOptions: StreamingVideoOptions = {
    //     successCallback: () => { console.log('Video played'); },
    //     errorCallback: (e) => { console.log('Error streaming'); },
    //     orientation: 'landscape',
    //     shouldAutoClose: true,
    //     controls: false
    //   };
    //   // console.log(fromDirectory);
      this.postFile = await this.file.readAsDataURL(this.videoFile.path, this.videoFile.name);
      await videoLoader.dismiss();
      console.log(this.postFile);
      let vid = this.videoFile.name.split(".");
      const creathumb:CreateThumbnailOptions = {
        fileUri: path,
        width:160, 
        height:206, 
        atTime:1, 
        outputFileName: vid[0] + "_thumb" + vid[1], 
        quality:30 
      } 
      console.log(creathumb);
      this.videoEditor.createThumbnail(creathumb).then(async (result:any) =>{
        this.postFile2 = await this.file.readAsDataURL(this.file.externalDataDirectory+"files/videos", result.substring(result.lastIndexOf("/")+1));    
        console.log(this.postFile2);   
      }).catch(e=>{
          console.log(e);
        // alert('fail video editor');
      }) 

    //   // this.streamingMedia.playVideo(capturedFile.fullPath, streamingOptions);

        }
    };
    
    // capture error callback
    var captureError = function(error) {
      console.log(error)
        // navigator.notification.alert('Error code: ' + error.code, null, 'Capture Error');
    };
    (<any>navigator).device.capture.captureVideo(captureSuccess, captureError, {limit:1,quality: 0});
       } catch (err) {
      await videoLoader.dismiss();
      console.log(err);
    }
  }

  onClickPlayVideo() {
    const streamingOptions: StreamingVideoOptions = {
      successCallback: () => { console.log('Video played'); },
      errorCallback: (e) => { console.log('Error streaming'); },
      orientation: 'portrait',
      shouldAutoClose: true,
      controls: false
    };

    this.streamingMedia.playVideo(this.videoFile.path, streamingOptions);
  }

}
