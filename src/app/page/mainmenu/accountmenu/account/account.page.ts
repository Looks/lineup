import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingController, AlertController, ModalController } from '@ionic/angular';
import * as firebase from "firebase/app";
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

/**
 * import interface
 */
import { UserData } from '../../../interface/interface';

/**
 * import custom services
 */
import { ShowtoastService } from "../../../../services/showtoast/showtoast.service";
import { FirebaseService } from '../../../../services/firebase-service/firebase.service';
import { MyutilsService } from 'src/app/services/myutils/myutils.service';
import { WritemessagePage } from 'src/app/page/modal/writemessage/writemessage.page';
import { findLast } from '@angular/compiler/src/directive_resolver';
import { findIndex } from 'rxjs/operators';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {

  private isCurrentUser: boolean = true;
  private selectedUID: string;
  private userData: UserData = {};
  private userEmail: string = "";
  private newPwd: string;
   d = new Date();
  //  userInfo;
   arrFavBeachesId;
   weatherReportdataRef;
   weatherReportdataListener;
   weatherReports;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private loadingCtrl: LoadingController,
    private altCtrl: AlertController,
    private modalCtrl: ModalController,
    private toastService: ShowtoastService,
    private firebaseService: FirebaseService,
    private myUtil: MyutilsService,
    private camera: Camera
    ) {
    this.selectedUID = this.route.snapshot.paramMap.get("userid");
    this.userData = {};
  }
   
  // toggleReorder() {
  //   const reorderGroup:any = document.getElementById('reorder');
  //   reorderGroup.disabled = !reorderGroup.disabled;
  //   reorderGroup.addEventListener('ionItemReorder', ({detail}) => {
  //     console.log(detail);
  //     detail.complete(true);
  //     let draggedItem = this.weatherReports.splice(detail.from,1)[0];
  //     this.weatherReports.splice(detail.to,0,draggedItem)
  //     console.log(this.weatherReports);
  //   });
  // }
  ngOnInit() {
    
    // console.log(d);
    this.isCurrentUser = this.firebaseService.checkIfCurrentUser(this.selectedUID);
    this.getUserData();
    // setTimeout(() => {
    //   this.toggleReorder();
    // }, 1000);
  }
  // async getUserInfo() {
  //   const userInfoSnapshot = await this.firebaseService.getUserProfile(this.currentUserId);
  //   this.userInfo = userInfoSnapshot.val();
  // }

  async getUserData() {
    const userInfoLoader = await this.loadingCtrl.create({
      message: "Loading..."
    });
    await userInfoLoader.present();
    const userProfileSnapshot = await this.firebaseService.getUserProfile(this.selectedUID);
    this.userData = userProfileSnapshot.val();
    this.userEmail = this.userData.email;
    this.myUtil.setUserData(this.userData);
    userInfoLoader.dismiss();


    // this.arrFavBeachesId = this.userData.favorite.beaches.split(',');
    // this.weatherReportdataRef = this.firebaseService.getAllWeatherReports();
    // this.weatherReportdataListener = this.weatherReportdataRef.on('value', snapshot => {
      
    //   this.weatherReports = [];
    //   const tmpArr = [];
    //   this.arrFavBeachesId.forEach((element)=>{
    //     snapshot.forEach((childSnapshot)=> {
    //       // console.log(childSnapshot);
    //       const favIndex = childSnapshot.key === element;
         
    //       if (favIndex) {
    //         childSnapshot.forEach((valueSnapshot) => {
    //           let value = {...valueSnapshot.val()}
    //           if(!tmpArr.find(key => key.locationId === value.locationId))
    //           tmpArr.push(value);
    //         });
    //       }
    //     });
    //   });
     
    //   console.log(tmpArr);
    //   this.weatherReports = tmpArr;
    // });
  }

  async onClickSaveBtn() {
    let isEmailChanged = this.userData.email!==this.userEmail?true: false;
    if(isEmailChanged) {
      this.openReAuthWindow(0);
    } else {
      this.saveUpdatedValue();
    }
  }


  async setorder(){
    // localStorage.setItem('favoriteLoc', arrFavorites.join(','));
    let order = [];
    this.weatherReports.forEach(element => {
      order.push(element.locationId);
    });
    console.log(order.join(","));
    localStorage.setItem('favoriteLoc', order.join(","));
    let loader = this.loadingCtrl.create({message:"Loading"});
   (await loader).present();
    this.firebaseService.addFavoriteBeaches(localStorage.getItem('favoriteLoc')).then(async (data)=>{
      console.log(data);
      (await loader).dismiss();
    })
  }

  async onClickProfileImg() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: 1,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
    };

    const imgSelectLoader = await this.loadingCtrl.create({
      message: "Loading..."
    });

    const selectedImg = 'data:image/jpeg;base64,' + await this.camera.getPicture(options);
    try {
      await imgSelectLoader.present();
      await this.firebaseService.uploadProfileImage(selectedImg, this.selectedUID);
      this.userData.profileimg = await this.firebaseService.getProfileImage(this.selectedUID);
      await this.firebaseService.updateProfileImageOnDB(this.selectedUID, this.userData.profileimg);
      await imgSelectLoader.dismiss();
    } catch(err) {
      imgSelectLoader.dismiss();
      console.error(err);
    }
  }

  async onClickChangePwd() {
    this.openReAuthWindow(1);
  }

  async updatePassword() {
    const user = this.firebaseService.getAuthUser();
    const pwdUpdateLoader = await this.loadingCtrl.create({
      message: "Please wait..."
    });
    await pwdUpdateLoader.present();
    try {
      await user.updatePassword(this.newPwd);
      pwdUpdateLoader.dismiss();
      this.toastService.showToast("Password updated successfully!");
    } catch(err) {
      console.log(err);
      pwdUpdateLoader.dismiss();
      this.toastService.showToast(err.message);
    }
  }

  async openReAuthWindow(type) {
    let inputs = [];
    let header = "Confirm User";
    if(type == 0) {
      inputs = [
        {
          name: 'value',
          type: 'password',
          placeholder: 'Please input password'
        }
      ]
    } else {
      header = "Change Password";
      inputs = [
        {
          name: 'value',
          type: 'password',
          placeholder: 'Current password'
        },
        {
          name: 'value1',
          type: 'password',
          placeholder: 'New password'
        },
        {
          name: 'value2',
          type: 'password',
          placeholder: 'Confirm New password'
        }
      ]
    }
    const reauthAlt = await this.altCtrl.create({
      header: header,
      inputs: inputs,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {

          }
        }, {
          text: 'Ok',
          handler: (data) => {
            if(type == 1) {
              if(data.value1 == data.value2) {
                this.newPwd = data.value1;
              } else {
                this.toastService.showToast("New password doesn't match!");
                return;
              }
            }
            this.reAuthUser(type, data.value);
          }
        }
      ]
    });
    await reauthAlt.present();
  }

  async reAuthUser(type, pwd) {
    const user = this.firebaseService.getAuthUser();
    try {
      const userCredential = firebase.auth.EmailAuthProvider.credential(user.email, pwd);
      await user.reauthenticateWithCredential(userCredential);
      if(type == 0) {
        this.userData.email = this.userEmail;
        this.saveUpdatedValue();
      } else {
        this.updatePassword();
      }
    } catch(err) {
      console.log(err);
      this.toastService.showToast(err.message);
    }
  }

  onClickNotiSettingBtn() {
    this.router.navigateByUrl("/account-noti-setting");
  }

  async onClickReportBtn(type) {
    /**
     * type
     *  0: abuse
     *  1: user
     *  2: suggestion
     *  3: missing beach
     */
    const reportModal = await this.modalCtrl.create({
      component: WritemessagePage,
      cssClass: "report-modal",
      componentProps: {
        reporttype: type
      }
    });
    await reportModal.present();
  }

  viewSendMessagePage() {
    this.router.navigateByUrl("/chat/" + this.selectedUID);
  }

  async saveUpdatedValue() {
    const saveLoader = await this.loadingCtrl.create({
      message: "Please wait..."
    });

    await saveLoader.present();
    try {
      const authUser = this.firebaseService.getAuthUser();
      await authUser.updateEmail(this.userEmail);
      await firebase.database().ref("users/" + this.userData.uid).update(this.userData);
      await saveLoader.dismiss();
      this.toastService.showToast("Account setting updated!");
    } catch(err) {
      await saveLoader.dismiss();
      this.toastService.showToast(err.message);
    }
  }

  async logOut() {
    await this.firebaseService.logOutUser();
  }

}
