(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["accountmenu-account-account-module"],{

/***/ "./src/app/page/mainmenu/accountmenu/account/account.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/page/mainmenu/accountmenu/account/account.module.ts ***!
  \*********************************************************************/
/*! exports provided: AccountPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountPageModule", function() { return AccountPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _account_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./account.page */ "./src/app/page/mainmenu/accountmenu/account/account.page.ts");
/* harmony import */ var src_app_page_modal_modal_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/page/modal/modal.module */ "./src/app/page/modal/modal.module.ts");








var AccountPageModule = /** @class */ (function () {
    function AccountPageModule() {
    }
    AccountPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                src_app_page_modal_modal_module__WEBPACK_IMPORTED_MODULE_7__["ModalModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild([{ path: '', component: _account_page__WEBPACK_IMPORTED_MODULE_6__["AccountPage"] }])
            ],
            declarations: [_account_page__WEBPACK_IMPORTED_MODULE_6__["AccountPage"]]
        })
    ], AccountPageModule);
    return AccountPageModule;
}());



/***/ }),

/***/ "./src/app/page/mainmenu/accountmenu/account/account.page.html":
/*!*********************************************************************!*\
  !*** ./src/app/page/mainmenu/accountmenu/account/account.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button color=\"navback\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Account Setting</ion-title>\n    <p *ngIf=\"isCurrentUser\" slot=\"end\" (click)=\"onClickSaveBtn()\">Save</p>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <div id=\"account-profileimg\">\n    <div id=\"account-profileimg-container\">\n      <ng-container *ngIf=\"isCurrentUser; else otherUserImg\">\n        <img [src]=\"userData.profileimg!=''?userData.profileimg:'assets/imgs/icon-profile.svg'\" (click)=\"onClickProfileImg()\"/>\n      </ng-container>\n      <ng-template #otherUserImg>\n        <img [src]=\"userData.profileimg!=''?userData.profileimg:'assets/imgs/img-person.svg'\" />\n      </ng-template>\n    </div>\n  </div>\n\n  <ion-grid>\n    <ion-row>\n      <ion-col size=\"6\">\n        <ion-item>\n          <ion-input type=\"text\" [disabled]=\"!isCurrentUser\" placeholder=\"First Name\" [(ngModel)]=\"userData.fname\"></ion-input>\n        </ion-item>\n      </ion-col>\n      <ion-col size=\"6\">\n        <ion-item>\n          <ion-input type=\"text\" [disabled]=\"!isCurrentUser\" placeholder=\"Last Name\" [(ngModel)]=\"userData.lname\"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row *ngIf=\"isCurrentUser\">\n      <ion-col>\n        <ion-item>\n          <ion-input type=\"email\" placeholder=\"Email Address\" [(ngModel)]=\"userEmail\"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n\n\n\n  <ng-container *ngIf=\"isCurrentUser==true; else otheruser\">\n    <div id=\"account-btn-changepwd\">\n      <ion-button expand=\"full\" (click)=\"onClickChangePwd()\">Change Password</ion-button>\n    </div>\n  \n    <div class=\"divider\"></div>\n  \n    <div>\n\n      <ion-button expand=\"full\" (click)=\"onClickNotiSettingBtn()\">Notification Settings</ion-button>\n    </div>\n  \n    <div class=\"divider\"></div>\n    <ion-buttons slot=\"primary\">\n      <ion-button onclick=\"toggleReorder()\">Toggle</ion-button>\n    </ion-buttons>\n    <ion-reorder-group id=\"reorder\">\n      <!-- Default reorder icon, end aligned items -->\n      <ion-item *ngFor=\"let data of weatherReports\">\n        <ion-label>\n          {{data.location.name}}\n        </ion-label>\n        <ion-reorder slot=\"end\"></ion-reorder>\n      </ion-item>\n    </ion-reorder-group>\n    <ion-button expand=\"full\" (click)=\"setorder()\">Set this order</ion-button>\n\n    <div class=\"divider\"></div>\n    <div id=\"account-btn-logout\">\n      <ion-button expand=\"full\" (click)=\"logOut()\">Log Out</ion-button>\n    </div>\n\n    <div class=\"divider\"></div>\n    <div style=\"text-align: center;\"><ion-text> current date is : {{d | date: \"dd-MM-y hh:mm a\" }}</ion-text> </div>\n    <div class=\"account-report\">\n      <p (click)=\"onClickReportBtn(0)\">Report Abuse</p>\n    </div>\n  \n    <div class=\"account-report\">\n      <p (click)=\"onClickReportBtn(1)\">Report User</p>\n    </div>\n\n    <div class=\"account-report\">\n      <p (click)=\"onClickReportBtn(2)\">Suggestions?</p>\n    </div>\n  </ng-container>\n  <ng-template #otheruser>\n    <div id=\"account-btn-send-message\">\n      <ion-button expand=\"full\" (click)=\"viewSendMessagePage()\">Send Message</ion-button>\n    </div>\n\n    <div class=\"divider\"></div>\n\n    <div id=\"account-view-profile-offers\">\n      <ion-grid>\n        <ion-row nowrap>\n          \n        </ion-row>\n      </ion-grid>\n    </div>\n  </ng-template>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/page/mainmenu/accountmenu/account/account.page.scss":
/*!*********************************************************************!*\
  !*** ./src/app/page/mainmenu/accountmenu/account/account.page.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-header {\n  --ion-background-color: #84A1B3; }\n\nion-toolbar {\n  position: relative;\n  --background: #84A1B3; }\n\nion-toolbar p {\n    color: #CAE9F7;\n    font-family: \"Open Sans Bold\";\n    padding-right: 5px; }\n\nion-title {\n  color: #CAE9F7;\n  font-family: \"Open Sans Bold\";\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%); }\n\n#account-profileimg-container {\n  position: relative;\n  width: 100px;\n  height: 100px;\n  border-radius: 100px;\n  overflow: hidden;\n  margin: auto; }\n\n#account-profileimg-container img {\n    position: absolute;\n    top: 50%;\n    left: 50%;\n    transform: translate(-50%, -50%); }\n\n#account-btn-changepwd {\n  margin-bottom: 3vh; }\n\n.account-report {\n  text-align: center; }\n\n.account-report p {\n    text-decoration: underline;\n    color: #427291;\n    font-weight: bold; }\n\n#account-btn-logout {\n  text-align: center; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2xva2VzaC9saW5ldXAgc291cmNlKDA5LTMwKSAoMikvbGluZXVwKDA5LTMwKS9zcmMvYXBwL3BhZ2UvbWFpbm1lbnUvYWNjb3VudG1lbnUvYWNjb3VudC9hY2NvdW50LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLCtCQUF1QixFQUFBOztBQUczQjtFQUNJLGtCQUFrQjtFQUNsQixxQkFBYSxFQUFBOztBQUZqQjtJQUlRLGNBQWM7SUFDZCw2QkFBNkI7SUFDN0Isa0JBQWtCLEVBQUE7O0FBSTFCO0VBQ0ksY0FBYztFQUNkLDZCQUE2QjtFQUM3QixrQkFBa0I7RUFDbEIsUUFBUTtFQUNSLFNBQVM7RUFDVCxnQ0FBZ0MsRUFBQTs7QUFHcEM7RUFDSSxrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLGFBQWE7RUFDYixvQkFBb0I7RUFDcEIsZ0JBQWdCO0VBQ2hCLFlBQVksRUFBQTs7QUFOaEI7SUFRUSxrQkFBa0I7SUFDbEIsUUFBUTtJQUNSLFNBQVM7SUFDVCxnQ0FBZ0MsRUFBQTs7QUFJeEM7RUFDSSxrQkFBa0IsRUFBQTs7QUFHdEI7RUFDSSxrQkFBa0IsRUFBQTs7QUFEdEI7SUFHUSwwQkFBMEI7SUFDMUIsY0FBYztJQUNkLGlCQUFpQixFQUFBOztBQUl6QjtFQUNJLGtCQUFrQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZS9tYWlubWVudS9hY2NvdW50bWVudS9hY2NvdW50L2FjY291bnQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWhlYWRlciB7XG4gICAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjogIzg0QTFCMztcbn1cblxuaW9uLXRvb2xiYXIge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAtLWJhY2tncm91bmQ6ICM4NEExQjM7XG4gICAgcCB7XG4gICAgICAgIGNvbG9yOiAjQ0FFOUY3O1xuICAgICAgICBmb250LWZhbWlseTogXCJPcGVuIFNhbnMgQm9sZFwiO1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG4gICAgfVxufVxuXG5pb24tdGl0bGUge1xuICAgIGNvbG9yOiAjQ0FFOUY3O1xuICAgIGZvbnQtZmFtaWx5OiBcIk9wZW4gU2FucyBCb2xkXCI7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogNTAlO1xuICAgIGxlZnQ6IDUwJTtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbn1cblxuI2FjY291bnQtcHJvZmlsZWltZy1jb250YWluZXIge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB3aWR0aDogMTAwcHg7XG4gICAgaGVpZ2h0OiAxMDBweDtcbiAgICBib3JkZXItcmFkaXVzOiAxMDBweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIG1hcmdpbjogYXV0bztcbiAgICBpbWcge1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIHRvcDogNTAlO1xuICAgICAgICBsZWZ0OiA1MCU7XG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuICAgIH1cbn1cblxuI2FjY291bnQtYnRuLWNoYW5nZXB3ZCB7XG4gICAgbWFyZ2luLWJvdHRvbTogM3ZoO1xufVxuXG4uYWNjb3VudC1yZXBvcnQge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwIHtcbiAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG4gICAgICAgIGNvbG9yOiAjNDI3MjkxO1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICB9XG59XG5cbiNhY2NvdW50LWJ0bi1sb2dvdXQge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/page/mainmenu/accountmenu/account/account.page.ts":
/*!*******************************************************************!*\
  !*** ./src/app/page/mainmenu/accountmenu/account/account.page.ts ***!
  \*******************************************************************/
/*! exports provided: AccountPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountPage", function() { return AccountPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _services_showtoast_showtoast_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../services/showtoast/showtoast.service */ "./src/app/services/showtoast/showtoast.service.ts");
/* harmony import */ var _services_firebase_service_firebase_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../services/firebase-service/firebase.service */ "./src/app/services/firebase-service/firebase.service.ts");
/* harmony import */ var src_app_services_myutils_myutils_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/myutils/myutils.service */ "./src/app/services/myutils/myutils.service.ts");
/* harmony import */ var src_app_page_modal_writemessage_writemessage_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/page/modal/writemessage/writemessage.page */ "./src/app/page/modal/writemessage/writemessage.page.ts");






/**
 * import custom services
 */




var AccountPage = /** @class */ (function () {
    function AccountPage(router, route, loadingCtrl, altCtrl, modalCtrl, toastService, firebaseService, myUtil, camera) {
        this.router = router;
        this.route = route;
        this.loadingCtrl = loadingCtrl;
        this.altCtrl = altCtrl;
        this.modalCtrl = modalCtrl;
        this.toastService = toastService;
        this.firebaseService = firebaseService;
        this.myUtil = myUtil;
        this.camera = camera;
        this.isCurrentUser = true;
        this.userData = {};
        this.userEmail = "";
        this.d = new Date();
        this.selectedUID = this.route.snapshot.paramMap.get("userid");
        this.userData = {};
    }
    AccountPage.prototype.toggleReorder = function () {
        var _this = this;
        var reorderGroup = document.getElementById('reorder');
        reorderGroup.disabled = !reorderGroup.disabled;
        reorderGroup.addEventListener('ionItemReorder', function (_a) {
            var detail = _a.detail;
            console.log(detail);
            detail.complete(true);
            var draggedItem = _this.weatherReports.splice(detail.from, 1)[0];
            _this.weatherReports.splice(detail.to, 0, draggedItem);
            console.log(_this.weatherReports);
        });
    };
    AccountPage.prototype.ngOnInit = function () {
        var _this = this;
        // console.log(d);
        this.isCurrentUser = this.firebaseService.checkIfCurrentUser(this.selectedUID);
        this.getUserData();
        setTimeout(function () {
            _this.toggleReorder();
        }, 1000);
    };
    // async getUserInfo() {
    //   const userInfoSnapshot = await this.firebaseService.getUserProfile(this.currentUserId);
    //   this.userInfo = userInfoSnapshot.val();
    // }
    AccountPage.prototype.getUserData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var userInfoLoader, userProfileSnapshot;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: "Loading..."
                        })];
                    case 1:
                        userInfoLoader = _a.sent();
                        return [4 /*yield*/, userInfoLoader.present()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.firebaseService.getUserProfile(this.selectedUID)];
                    case 3:
                        userProfileSnapshot = _a.sent();
                        this.userData = userProfileSnapshot.val();
                        this.userEmail = this.userData.email;
                        this.myUtil.setUserData(this.userData);
                        userInfoLoader.dismiss();
                        this.arrFavBeachesId = this.userData.favorite.beaches.split(',');
                        this.weatherReportdataRef = this.firebaseService.getAllWeatherReports();
                        this.weatherReportdataListener = this.weatherReportdataRef.on('value', function (snapshot) {
                            _this.weatherReports = [];
                            var tmpArr = [];
                            _this.arrFavBeachesId.forEach(function (element) {
                                snapshot.forEach(function (childSnapshot) {
                                    // console.log(childSnapshot);
                                    var favIndex = childSnapshot.key === element;
                                    if (favIndex) {
                                        childSnapshot.forEach(function (valueSnapshot) {
                                            var value = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, valueSnapshot.val());
                                            if (!tmpArr.find(function (key) { return key.locationId === value.locationId; }))
                                                tmpArr.push(value);
                                        });
                                    }
                                });
                            });
                            console.log(tmpArr);
                            _this.weatherReports = tmpArr;
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    AccountPage.prototype.onClickSaveBtn = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var isEmailChanged;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                isEmailChanged = this.userData.email !== this.userEmail ? true : false;
                if (isEmailChanged) {
                    this.openReAuthWindow(0);
                }
                else {
                    this.saveUpdatedValue();
                }
                return [2 /*return*/];
            });
        });
    };
    AccountPage.prototype.setorder = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var order, loader;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        order = [];
                        this.weatherReports.forEach(function (element) {
                            order.push(element.locationId);
                        });
                        console.log(order.join(","));
                        localStorage.setItem('favoriteLoc', order.join(","));
                        loader = this.loadingCtrl.create({ message: "Loading" });
                        return [4 /*yield*/, loader];
                    case 1:
                        (_a.sent()).present();
                        this.firebaseService.addFavoriteBeaches(localStorage.getItem('favoriteLoc')).then(function (data) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        console.log(data);
                                        return [4 /*yield*/, loader];
                                    case 1:
                                        (_a.sent()).dismiss();
                                        return [2 /*return*/];
                                }
                            });
                        }); });
                        return [2 /*return*/];
                }
            });
        });
    };
    AccountPage.prototype.onClickProfileImg = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var options, imgSelectLoader, selectedImg, _a, _b, err_1;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_c) {
                switch (_c.label) {
                    case 0:
                        options = {
                            quality: 100,
                            destinationType: this.camera.DestinationType.DATA_URL,
                            sourceType: 1,
                            encodingType: this.camera.EncodingType.JPEG,
                            mediaType: this.camera.MediaType.PICTURE,
                            correctOrientation: true,
                        };
                        return [4 /*yield*/, this.loadingCtrl.create({
                                message: "Loading..."
                            })];
                    case 1:
                        imgSelectLoader = _c.sent();
                        _a = 'data:image/jpeg;base64,';
                        return [4 /*yield*/, this.camera.getPicture(options)];
                    case 2:
                        selectedImg = _a + (_c.sent());
                        _c.label = 3;
                    case 3:
                        _c.trys.push([3, 9, , 10]);
                        return [4 /*yield*/, imgSelectLoader.present()];
                    case 4:
                        _c.sent();
                        return [4 /*yield*/, this.firebaseService.uploadProfileImage(selectedImg, this.selectedUID)];
                    case 5:
                        _c.sent();
                        _b = this.userData;
                        return [4 /*yield*/, this.firebaseService.getProfileImage(this.selectedUID)];
                    case 6:
                        _b.profileimg = _c.sent();
                        return [4 /*yield*/, this.firebaseService.updateProfileImageOnDB(this.selectedUID, this.userData.profileimg)];
                    case 7:
                        _c.sent();
                        return [4 /*yield*/, imgSelectLoader.dismiss()];
                    case 8:
                        _c.sent();
                        return [3 /*break*/, 10];
                    case 9:
                        err_1 = _c.sent();
                        imgSelectLoader.dismiss();
                        console.error(err_1);
                        return [3 /*break*/, 10];
                    case 10: return [2 /*return*/];
                }
            });
        });
    };
    AccountPage.prototype.onClickChangePwd = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                this.openReAuthWindow(1);
                return [2 /*return*/];
            });
        });
    };
    AccountPage.prototype.updatePassword = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var user, pwdUpdateLoader, err_2;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        user = this.firebaseService.getAuthUser();
                        return [4 /*yield*/, this.loadingCtrl.create({
                                message: "Please wait..."
                            })];
                    case 1:
                        pwdUpdateLoader = _a.sent();
                        return [4 /*yield*/, pwdUpdateLoader.present()];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        _a.trys.push([3, 5, , 6]);
                        return [4 /*yield*/, user.updatePassword(this.newPwd)];
                    case 4:
                        _a.sent();
                        pwdUpdateLoader.dismiss();
                        this.toastService.showToast("Password updated successfully!");
                        return [3 /*break*/, 6];
                    case 5:
                        err_2 = _a.sent();
                        console.log(err_2);
                        pwdUpdateLoader.dismiss();
                        this.toastService.showToast(err_2.message);
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    AccountPage.prototype.openReAuthWindow = function (type) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var inputs, header, reauthAlt;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        inputs = [];
                        header = "Confirm User";
                        if (type == 0) {
                            inputs = [
                                {
                                    name: 'value',
                                    type: 'password',
                                    placeholder: 'Please input password'
                                }
                            ];
                        }
                        else {
                            header = "Change Password";
                            inputs = [
                                {
                                    name: 'value',
                                    type: 'password',
                                    placeholder: 'Current password'
                                },
                                {
                                    name: 'value1',
                                    type: 'password',
                                    placeholder: 'New password'
                                },
                                {
                                    name: 'value2',
                                    type: 'password',
                                    placeholder: 'Confirm New password'
                                }
                            ];
                        }
                        return [4 /*yield*/, this.altCtrl.create({
                                header: header,
                                inputs: inputs,
                                buttons: [
                                    {
                                        text: 'Cancel',
                                        role: 'cancel',
                                        cssClass: 'secondary',
                                        handler: function () {
                                        }
                                    }, {
                                        text: 'Ok',
                                        handler: function (data) {
                                            if (type == 1) {
                                                if (data.value1 == data.value2) {
                                                    _this.newPwd = data.value1;
                                                }
                                                else {
                                                    _this.toastService.showToast("New password doesn't match!");
                                                    return;
                                                }
                                            }
                                            _this.reAuthUser(type, data.value);
                                        }
                                    }
                                ]
                            })];
                    case 1:
                        reauthAlt = _a.sent();
                        return [4 /*yield*/, reauthAlt.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AccountPage.prototype.reAuthUser = function (type, pwd) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var user, userCredential, err_3;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        user = this.firebaseService.getAuthUser();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        userCredential = firebase_app__WEBPACK_IMPORTED_MODULE_4__["auth"].EmailAuthProvider.credential(user.email, pwd);
                        return [4 /*yield*/, user.reauthenticateWithCredential(userCredential)];
                    case 2:
                        _a.sent();
                        if (type == 0) {
                            this.userData.email = this.userEmail;
                            this.saveUpdatedValue();
                        }
                        else {
                            this.updatePassword();
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        err_3 = _a.sent();
                        console.log(err_3);
                        this.toastService.showToast(err_3.message);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    AccountPage.prototype.onClickNotiSettingBtn = function () {
        this.router.navigateByUrl("/account-noti-setting");
    };
    AccountPage.prototype.onClickReportBtn = function (type) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var reportModal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: src_app_page_modal_writemessage_writemessage_page__WEBPACK_IMPORTED_MODULE_9__["WritemessagePage"],
                            cssClass: "report-modal",
                            componentProps: {
                                reporttype: type
                            }
                        })];
                    case 1:
                        reportModal = _a.sent();
                        return [4 /*yield*/, reportModal.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AccountPage.prototype.viewSendMessagePage = function () {
        this.router.navigateByUrl("/chat/" + this.selectedUID);
    };
    AccountPage.prototype.saveUpdatedValue = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var saveLoader, authUser, err_4;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: "Please wait..."
                        })];
                    case 1:
                        saveLoader = _a.sent();
                        return [4 /*yield*/, saveLoader.present()];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        _a.trys.push([3, 7, , 9]);
                        authUser = this.firebaseService.getAuthUser();
                        return [4 /*yield*/, authUser.updateEmail(this.userEmail)];
                    case 4:
                        _a.sent();
                        return [4 /*yield*/, firebase_app__WEBPACK_IMPORTED_MODULE_4__["database"]().ref("users/" + this.userData.uid).update(this.userData)];
                    case 5:
                        _a.sent();
                        return [4 /*yield*/, saveLoader.dismiss()];
                    case 6:
                        _a.sent();
                        this.toastService.showToast("Account setting updated!");
                        return [3 /*break*/, 9];
                    case 7:
                        err_4 = _a.sent();
                        return [4 /*yield*/, saveLoader.dismiss()];
                    case 8:
                        _a.sent();
                        this.toastService.showToast(err_4.message);
                        return [3 /*break*/, 9];
                    case 9: return [2 /*return*/];
                }
            });
        });
    };
    AccountPage.prototype.logOut = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.firebaseService.logOutUser()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AccountPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-account',
            template: __webpack_require__(/*! ./account.page.html */ "./src/app/page/mainmenu/accountmenu/account/account.page.html"),
            styles: [__webpack_require__(/*! ./account.page.scss */ "./src/app/page/mainmenu/accountmenu/account/account.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
            _services_showtoast_showtoast_service__WEBPACK_IMPORTED_MODULE_6__["ShowtoastService"],
            _services_firebase_service_firebase_service__WEBPACK_IMPORTED_MODULE_7__["FirebaseService"],
            src_app_services_myutils_myutils_service__WEBPACK_IMPORTED_MODULE_8__["MyutilsService"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_5__["Camera"]])
    ], AccountPage);
    return AccountPage;
}());



/***/ }),

/***/ "./src/app/page/modal/gallery/gallery.page.html":
/*!******************************************************!*\
  !*** ./src/app/page/modal/gallery/gallery.page.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content fullscreen padding>\n  <div id=\"gallery-modal-btnclose\">\n    <ion-icon name=\"close\" (click)=\"closeGalleryModal()\"></ion-icon>\n  </div>\n  <ion-slides [options]=\"sliderOpts\">\n    <ion-slide *ngFor=\"let img of imgs\">\n      <div class=\"swiper-zoom-container\">\n        <img [src]=\"img\" />\n      </div>\n    </ion-slide>\n  </ion-slides>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/page/modal/gallery/gallery.page.scss":
/*!******************************************************!*\
  !*** ./src/app/page/modal/gallery/gallery.page.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background: rgba(19, 19, 19, 0.45); }\n\n#gallery-modal-btnclose {\n  width: 100%;\n  text-align: right; }\n\n#gallery-modal-btnclose ion-icon {\n    font-size: 24pt;\n    color: white; }\n\nion-slides {\n  height: 80%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2xva2VzaC9saW5ldXAgc291cmNlKDA5LTMwKSAoMikvbGluZXVwKDA5LTMwKS9zcmMvYXBwL3BhZ2UvbW9kYWwvZ2FsbGVyeS9nYWxsZXJ5LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG9DQUFhLEVBQUE7O0FBR2pCO0VBQ0ksV0FBVztFQUNYLGlCQUFpQixFQUFBOztBQUZyQjtJQUlRLGVBQWU7SUFDZixZQUFZLEVBQUE7O0FBSXBCO0VBQ0ksV0FBVyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZS9tb2RhbC9nYWxsZXJ5L2dhbGxlcnkucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xuICAgIC0tYmFja2dyb3VuZDogcmdiYSgxOSwgMTksIDE5LCAwLjQ1KTtcbn1cblxuI2dhbGxlcnktbW9kYWwtYnRuY2xvc2Uge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIGlvbi1pY29uIHtcbiAgICAgICAgZm9udC1zaXplOiAyNHB0O1xuICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgfVxufVxuXG5pb24tc2xpZGVzIHtcbiAgICBoZWlnaHQ6IDgwJTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/page/modal/gallery/gallery.page.ts":
/*!****************************************************!*\
  !*** ./src/app/page/modal/gallery/gallery.page.ts ***!
  \****************************************************/
/*! exports provided: GalleryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GalleryPage", function() { return GalleryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var GalleryPage = /** @class */ (function () {
    function GalleryPage(modalCtrl) {
        this.modalCtrl = modalCtrl;
        this.sliderOpts = {
            zoom: {
                maxRatio: 3
            }
        };
    }
    GalleryPage.prototype.ngOnInit = function () {
    };
    GalleryPage.prototype.closeGalleryModal = function () {
        this.modalCtrl.dismiss();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('imgs'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GalleryPage.prototype, "imgs", void 0);
    GalleryPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-gallery',
            template: __webpack_require__(/*! ./gallery.page.html */ "./src/app/page/modal/gallery/gallery.page.html"),
            styles: [__webpack_require__(/*! ./gallery.page.scss */ "./src/app/page/modal/gallery/gallery.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
    ], GalleryPage);
    return GalleryPage;
}());



/***/ }),

/***/ "./src/app/page/modal/modal.module.ts":
/*!********************************************!*\
  !*** ./src/app/page/modal/modal.module.ts ***!
  \********************************************/
/*! exports provided: ModalModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalModule", function() { return ModalModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _gallery_gallery_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./gallery/gallery.page */ "./src/app/page/modal/gallery/gallery.page.ts");
/* harmony import */ var _writemessage_writemessage_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./writemessage/writemessage.page */ "./src/app/page/modal/writemessage/writemessage.page.ts");







var ModalModule = /** @class */ (function () {
    function ModalModule() {
    }
    ModalModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _gallery_gallery_page__WEBPACK_IMPORTED_MODULE_5__["GalleryPage"],
                _writemessage_writemessage_page__WEBPACK_IMPORTED_MODULE_6__["WritemessagePage"],
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"]
            ],
            exports: [
                _gallery_gallery_page__WEBPACK_IMPORTED_MODULE_5__["GalleryPage"],
                _writemessage_writemessage_page__WEBPACK_IMPORTED_MODULE_6__["WritemessagePage"],
            ],
            entryComponents: [
                _gallery_gallery_page__WEBPACK_IMPORTED_MODULE_5__["GalleryPage"],
                _writemessage_writemessage_page__WEBPACK_IMPORTED_MODULE_6__["WritemessagePage"],
            ]
        })
    ], ModalModule);
    return ModalModule;
}());



/***/ }),

/***/ "./src/app/page/modal/writemessage/writemessage.page.html":
/*!****************************************************************!*\
  !*** ./src/app/page/modal/writemessage/writemessage.page.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <ion-header no-border>\n  <ion-toolbar>\n    <ion-title></ion-title>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-content>\n  <div style=\"height: 40px;margin-left: 20px;display: flex;align-items: center;\"><h3>{{title}}</h3></div>\n  <ion-item>\n    <ion-label>\n      To:\n    </ion-label>\n    <ion-input [value]=\"supportEmail\" disabled=true></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-textarea [(ngModel)]=\"reportMessage\" rows=5></ion-textarea>\n  </ion-item>\n\n  <ion-item lines=\"none\" id=\"writemessage-btnsend\" class=\"ion-margin-top\">\n    <div>\n      <ion-button (click)=\"onClickSendBtn()\">Send</ion-button>\n    </div>\n  </ion-item>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/page/modal/writemessage/writemessage.page.scss":
/*!****************************************************************!*\
  !*** ./src/app/page/modal/writemessage/writemessage.page.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#writemessage-btnsend div {\n  width: 100%;\n  text-align: center; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2xva2VzaC9saW5ldXAgc291cmNlKDA5LTMwKSAoMikvbGluZXVwKDA5LTMwKS9zcmMvYXBwL3BhZ2UvbW9kYWwvd3JpdGVtZXNzYWdlL3dyaXRlbWVzc2FnZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFUSxXQUFXO0VBQ1gsa0JBQWtCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlL21vZGFsL3dyaXRlbWVzc2FnZS93cml0ZW1lc3NhZ2UucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI3dyaXRlbWVzc2FnZS1idG5zZW5kIHtcbiAgICBkaXYge1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/page/modal/writemessage/writemessage.page.ts":
/*!**************************************************************!*\
  !*** ./src/app/page/modal/writemessage/writemessage.page.ts ***!
  \**************************************************************/
/*! exports provided: WritemessagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WritemessagePage", function() { return WritemessagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_email_composer_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/email-composer/ngx */ "./node_modules/@ionic-native/email-composer/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_showtoast_showtoast_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/showtoast/showtoast.service */ "./src/app/services/showtoast/showtoast.service.ts");
/* harmony import */ var src_app_constants_constants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/constants/constants */ "./src/app/constants/constants.ts");






var WritemessagePage = /** @class */ (function () {
    function WritemessagePage(modalCtrl, emailComposer, toastService) {
        this.modalCtrl = modalCtrl;
        this.emailComposer = emailComposer;
        this.toastService = toastService;
        this.reportMessage = '';
        this.title = '';
        this.supportEmail = src_app_constants_constants__WEBPACK_IMPORTED_MODULE_5__["SUPPORTEMAIL"];
    }
    WritemessagePage.prototype.ngOnInit = function () {
        var subject = '';
        if (this.reporttype == 0) {
            subject = 'Report Abuse';
        }
        else if (this.reporttype == 1) {
            subject = 'Report User';
        }
        else if (this.reporttype == 2) {
            subject = 'Suggestions';
        }
        else {
            subject = 'Missing Beach?';
        }
        this.title = subject;
    };
    WritemessagePage.prototype.onClickSendBtn = function () {
        var subject = '';
        if (this.reporttype == 0) {
            subject = 'Report Abuse';
        }
        else if (this.reporttype == 1) {
            subject = 'Report User';
        }
        else if (this.reporttype == 2) {
            subject = 'Suggestions';
        }
        else {
            subject = 'Missing Beach?';
        }
        var email = {
            to: this.supportEmail,
            cc: '',
            bcc: [],
            attachments: [],
            subject: subject,
            body: this.reportMessage,
            isHtml: false
        };
        this.emailComposer.open(email);
        this.modalCtrl.dismiss();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('reporttype'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], WritemessagePage.prototype, "reporttype", void 0);
    WritemessagePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-writemessage',
            template: __webpack_require__(/*! ./writemessage.page.html */ "./src/app/page/modal/writemessage/writemessage.page.html"),
            styles: [__webpack_require__(/*! ./writemessage.page.scss */ "./src/app/page/modal/writemessage/writemessage.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
            _ionic_native_email_composer_ngx__WEBPACK_IMPORTED_MODULE_2__["EmailComposer"],
            src_app_services_showtoast_showtoast_service__WEBPACK_IMPORTED_MODULE_4__["ShowtoastService"]])
    ], WritemessagePage);
    return WritemessagePage;
}());



/***/ }),

/***/ "./src/app/services/showtoast/showtoast.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/services/showtoast/showtoast.service.ts ***!
  \*********************************************************/
/*! exports provided: ShowtoastService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShowtoastService", function() { return ShowtoastService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var ShowtoastService = /** @class */ (function () {
    function ShowtoastService(toastCtrl) {
        this.toastCtrl = toastCtrl;
    }
    ShowtoastService.prototype.showToast = function (message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: message,
                            duration: 3000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    ShowtoastService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]])
    ], ShowtoastService);
    return ShowtoastService;
}());



/***/ })

}]);
//# sourceMappingURL=accountmenu-account-account-module.js.map