(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-main-home-main-module"],{

/***/ "./src/app/page/mainmenu/home-main/home-main.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/page/mainmenu/home-main/home-main.module.ts ***!
  \*************************************************************/
/*! exports provided: HomeMainPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeMainPageModule", function() { return HomeMainPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _home_main_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./home-main.page */ "./src/app/page/mainmenu/home-main/home-main.page.ts");








var HomeMainPageModule = /** @class */ (function () {
    function HomeMainPageModule() {
    }
    HomeMainPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_5__["PipesModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild([{ path: '', component: _home_main_page__WEBPACK_IMPORTED_MODULE_7__["HomeMainPage"] }])
            ],
            declarations: [_home_main_page__WEBPACK_IMPORTED_MODULE_7__["HomeMainPage"]]
        })
    ], HomeMainPageModule);
    return HomeMainPageModule;
}());



/***/ }),

/***/ "./src/app/page/mainmenu/home-main/home-main.page.html":
/*!*************************************************************!*\
  !*** ./src/app/page/mainmenu/home-main/home-main.page.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-toolbar>\n    <ion-title>\n      <div style=\"display: flex;align-items: center;justify-content: center;\">\n      <span>LINEUP</span><img src=\"assets/imgs/app_icon_2.png\" style=\"width: 50px;\"/>     \n     </div>\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>     \n\n  <div id=\"home-maintab-header\">\n    <p style=\"margin: 5px 5px 5px 0px;\">Latest Reports</p>\n  </div>\n\n  <!-- Weather Reports -->\n  <div id=\"home-maintab-weather-reports-container row\" style=\"padding: 6px;\">\n    <div *ngIf=\"readmoreClicked == false\" style=\"max-height:50%\">\n    <div class=\"home-main-tab-weather-reports\" *ngFor=\"let weatherItem of weatherReports; let i = index;\"  (click)=\"onClickReportItem(weatherItem)\" style=\"display: flex;\">\n      <div style=\"padding: 5px;width: 80px;height: 80px;position: relative;\" *ngIf=\"weatherItem.postfile && weatherItem.postfile.data!=''\"\n            class=\"home-location-detail-reports-img\">\n          <img *ngIf=\"weatherItem.postfile.type==0\" [src]=\"weatherItem.postfile.data\"\n                (click)=\"onClickVideoAttachment($event,weatherItem.postfile.data, weatherItem.postfile.type)\" style=\"width: 100%;height: 100%;object-fit: cover;\" />\n          <img *ngIf=\"weatherItem.postfile.type==1\" [src]=\"weatherItem.postfile.thumb\"\n                (click)=\"onClickVideoAttachment($event,weatherItem.postfile.data, weatherItem.postfile.type)\" style=\"width: 100%;height: 100%;object-fit: contain;\" />\n                <ion-icon *ngIf=\"weatherItem.postfile.type==1\" name=\"play\" style=\"position: absolute;\n                color: white;\n                left: calc(50% - 10px);\n                top: calc(50% - 10px);\"></ion-icon>\n      </div>\n      <div style=\"width: 100%;position: relative;\">\n        <div class=\"home-main-tab-weather-reports-location\" style=\"padding-bottom: 0;\">\n          <span>{{weatherItem.location.name}}</span>\n          <p class=\"home-main-tab-weather-reports-waveheight\" style=\"font-size: 10px;\">\n            Height: {{weatherItem.waveHeight | round: true}} ft  \n            Form: {{arrWaveQualities[weatherItem.waveQuality]}}\n          </p>\n        </div>\n        <!-- <ion-icon name=\"more\" class=\"iconStyle\" (click)=\"setDefault(weatherItem)\"></ion-icon> -->\n\n        <div class=\"home-main-tab-weather-reports-body\">\n          <div>\n            <p class=\"home-main-tab-weather-reports-reporttxt\" *ngIf=\"weatherItem.reporttxt!=''\" >\n              &quot;{{(weatherItem.reporttxt.length>250)? (weatherItem.reporttxt | slice:0:250)+' ...':(weatherItem.reporttxt)}}&quot;\n            </p>\n          </div>\n          <div class=\"home-main-tab-weather-reports-reporttime\" style=\"position: absolute;right: 6px;bottom: 0px;\">\n            <p>\n              {{weatherItem.username | formatname}}.&nbsp;{{weatherItem.createdat | millitotime: true}}\n            </p>\n          </div>\n        </div>\n      </div>\n    </div>\n    </div>\n   \n    <div *ngIf=\"readmoreClicked\"  style=\"max-height:50%\">\n    <div class=\"home-main-tab-weather-reports\" *ngFor=\"let weatherItem of weatherReportsFilter; let i = index;\" (click)=\"onClickReportItem(weatherItem)\" style=\"display: flex;\">\n      <div style=\"padding: 5px;width: 80px;height: 80px;\" *ngIf=\"weatherItem.postfile && weatherItem.postfile.data!=''\"\n      class=\"home-location-detail-reports-img\">\n    <img *ngIf=\"weatherItem.postfile.type==0\" [src]=\"weatherItem.postfile.data\"\n          (click)=\"onClickVideoAttachment(weatherItem.postfile.data, weatherItem.postfile.type)\" style=\"width: 100%;height: 100%;object-fit: cover;\"/>\n    <img *ngIf=\"weatherItem.postfile.type==1\" [src]=\"weatherItem.postfile.thumb\"\n          (click)=\"onClickVideoAttachment(weatherItem.postfile.data, weatherItem.postfile.type)\" style=\"width: 100%;height: 100%;object-fit: contain;\"/>\n</div>\n<div style=\"width: 100%;position: relative;\">\n  <div class=\"home-main-tab-weather-reports-location\" style=\"padding-bottom: 0;\">\n    <span>{{weatherItem.location.name}}</span>\n    <p class=\"home-main-tab-weather-reports-waveheight\" style=\"font-size: 10px;\">\n      Height: {{weatherItem.waveHeight | round: true}}ft  \n      Form: {{arrWaveQualities[weatherItem.waveQuality]}}\n    </p>\n  </div>\n  <div class=\"home-main-tab-weather-reports-body\" >\n    \n    <div>\n      <p class=\"home-main-tab-weather-reports-reporttxt\" *ngIf=\"weatherItem.reporttxt!=''\" >\n        &quot;{{(weatherItem.reporttxt.length>250)? (weatherItem.reporttxt | slice:0:250)+' ...':(weatherItem.reporttxt)}}&quot;\n      </p>\n    </div>\n    <div class=\"home-main-tab-weather-reports-reporttime\" style=\"position: absolute;right: 6px;bottom: 0px;\">\n      <p>\n        {{weatherItem.username | formatname}}.&nbsp;{{weatherItem.createdat | millitotime: true}}\n      </p>\n    </div>\n  </div>\n</div>\n    </div>\n    </div>\n    <div>\n      <p *ngIf=\"weatherReportsFilter && weatherReportsFilter.length > 4 && readmoreClicked == false\" (click)=\"show(1)\" style=\"margin:0px;text-align: end;\n      font-size: 12px;\n      color: blue;\">show more</p>\n      <p *ngIf=\"weatherReportsFilter &&  weatherReportsFilter.length > 4 && readmoreClicked\" (click)=\"show(0)\" style=\"margin:0px;text-align: end;\n      font-size: 12px;\n      color: blue;\">show less</p>\n    </div>     \n  </div>\n\n  <!-- Shop Reports -->\n  <div id=\"home-maintab-shop-reports-container\">\n    <div id=\"home-maintab-shop-reports-header\">\n      <p style=\"margin:0px;\">Recent Offers</p>\n    </div>\n    <div id=\"home-maintab-shop-reports-body\" *ngIf=\"allNewSurfBoards.length\">\n      <div class=\"home-maintab-shop-reports-item\">\n        <ion-grid style=\"padding:0px;\">\n          <ion-row nowrap id=\"home-maintab-shop-reports-img-container\">\n            <ion-col size=\"3\" *ngFor=\"let boardItem of allNewSurfBoards; let i = index;\"  [style.margin]=\"(i==0?'0px 0px 0px 0px':'0px 0px 0px 3px')\">\n              <div class=\"home-maintab-shop-reports-itemimg\" (click)=\"viewItemDetail(i)\">\n                <img src=\"{{boardItem.imgs?boardItem.imgs[0]:'assets/imgs/img-surfboard.svg'}}\" />\n                <div class=\"home-maintab-shop-reports-itemimg-description\">\n                  <p>\n                    {{boardItem.title}}\n                  </p>\n                </div>\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </div>\n    </div>\n  </div>\n\n  <!-- Recent Discussions -->\n  <div id=\"home-maintab-discussions-container\">\n    <div id=\"home-maintab-discussions-header\">\n      <p style=\"margin:10px 0px 0px 0px;\">Discussions</p>\n    </div>\n    <div id=\"home-maintab-discussions-body\">\n      <div class=\"home-maintab-discussion-item\" *ngFor=\"let discussion of arrDiscussionsFilter\" (click)=\"onClickDiscussionItem(discussion)\">\n        <div class=\"home-maintab-discussion-item-header\">\n          <span>\n            {{discussion.title}}\n          </span>\n        </div>\n        <div class=\"home-maintab-discussion-item-body\">\n          <p>\n            &quot;{{discussion.content}}&quot;\n          </p>\n        </div>\n        <div class=\"home-maintab-discussion-item-footer\">\n          <p>\n            {{discussion.postername | formatname}}.&nbsp;{{discussion.createdat | millitotime: true}}\n          </p>\n        </div>\n      </div>\n\n      <div>\n        <p *ngIf=\"arrDiscussionsFilter && arrDiscussionsFilter.length <= 4 && arrDiscussions.length > 4\" (click)=\"showMore(1)\" style=\"margin:0px;text-align: end;\n        font-size: 12px;\n        color: blue;\">show more</p>\n        <p *ngIf=\"arrDiscussionsFilter &&  arrDiscussionsFilter.length > 4\" (click)=\"showMore(0)\" style=\"margin:0px;text-align: end;\n        font-size: 12px;\n        color: blue;\">show less</p>\n      </div>  \n    </div>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/page/mainmenu/home-main/home-main.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/page/mainmenu/home-main/home-main.page.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content, ion-header {\n  --ion-background-color: white; }\n\nion-toolbar {\n  --background: #BCD9E6;\n  height: 60px;\n  border-radius: 0px 0px 30px 30px; }\n\nion-title {\n  text-align: center; }\n\nion-title > span {\n  color: #00B4F9;\n  font-family: \"Open Sans Bold\"; }\n\n#home-maintab-fav-beaches-body {\n  padding: 0 16px; }\n\n#home-maintab-header p {\n  font-size: 14pt;\n  font-weight: bold;\n  color: #426675; }\n\n.home-main-tab-weather-reports {\n  background-color: #F5F5F5;\n  margin-bottom: 10px;\n  box-shadow: 0 0 8px #a4a4a4;\n  background-color: white;\n  border-radius: 10px; }\n\n.home-main-tab-weather-reports-location {\n  padding: 5px;\n  background-color: white;\n  font-weight: bold;\n  font-size: 18px;\n  border-radius: 10px 10px 0px 0px; }\n\n.home-main-tab-weather-reports-location span {\n    color: #2b2b2b; }\n\n.home-main-tab-weather-reports-body {\n  padding: 0px 5px; }\n\n.home-main-tab-weather-reports-reporttxt {\n  color: #626262;\n  font-size: 12pt;\n  margin: 5px 0 20px;\n  font-style: italic; }\n\n.iconStyle {\n  position: absolute;\n  top: 5px;\n  right: 5px;\n  z-index: 10000; }\n\n.home-main-tab-weather-reports-weatherinfo {\n  display: flex; }\n\n.home-main-tab-weather-reports-weatherinfo div {\n    flex: 1; }\n\n.home-main-tab-weather-reports-waveheight {\n  text-align: left;\n  color: #626262;\n  margin: 2px; }\n\n.home-main-tab-weather-reports-wavequality {\n  text-align: right;\n  color: #626262;\n  margin: 5px 0; }\n\n.home-main-tab-weather-reports-reporttime {\n  flex: 1;\n  text-align: right; }\n\n.home-main-tab-weather-reports-reporttime p {\n    font-size: 6pt;\n    color: #2e556e;\n    margin: 5px 0; }\n\n#home-maintab-shop-reports-header p, #home-maintab-fav-beachhes-header p, #home-maintab-discussions-header p {\n  font-size: 14pt;\n  font-weight: bold;\n  color: #426675; }\n\n#home-maintab-shop-reports-img-container {\n  overflow-x: scroll; }\n\n#home-maintab-shop-reports-img-container ion-col {\n    padding: 0;\n    margin: 10px; }\n\n.home-maintab-shop-reports-itemimg {\n  width: 100%;\n  height: 120px;\n  overflow: hidden;\n  position: relative; }\n\n.home-maintab-shop-reports-itemimg img {\n    width: 100%;\n    min-height: 100%; }\n\n.home-maintab-shop-reports-itemimg-description {\n  position: absolute;\n  z-index: 2;\n  bottom: 0;\n  left: 0;\n  width: 100%;\n  background-color: rgba(0, 0, 0, 0.4); }\n\n.home-maintab-shop-reports-itemimg-description p {\n    color: white;\n    text-align: center;\n    font-weight: bold;\n    font-size: 9pt; }\n\n.home-maintab-discussion-item {\n  margin-bottom: 10px;\n  box-shadow: 0 0 8px #a4a4a4;\n  border-radius: 10px; }\n\n.home-maintab-discussion-item-header {\n  padding: 5px 5px 0px 5px;\n  background-color: #dadada;\n  background-color: white;\n  border-radius: 10px;\n  font-weight: bold;\n  font-size: 18px; }\n\n.home-maintab-discussion-item-header span {\n    color: #2b2b2b; }\n\n.home-maintab-discussion-item-body, .home-maintab-discussion-item-footer {\n  padding: 0px; }\n\n.home-maintab-discussion-item-body p, .home-maintab-discussion-item-footer p {\n    margin: 5px 0;\n    padding: 2px 5px;\n    border-radius: 10px; }\n\n.home-maintab-discussion-item-body p {\n  color: #565656;\n  font-style: italic;\n  background-color: white; }\n\n.home-maintab-discussion-item-footer p {\n  text-align: right;\n  font-size: 6pt;\n  color: #2e556e;\n  background-color: white; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2xva2VzaC9saW5ldXAgc291cmNlKDA5LTMwKSAoMikvbGluZXVwKDA5LTMwKS9zcmMvYXBwL3BhZ2UvbWFpbm1lbnUvaG9tZS1tYWluL2hvbWUtbWFpbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFSSw2QkFBdUIsRUFBQTs7QUFHM0I7RUFDSSxxQkFBYTtFQUNiLFlBQVk7RUFDWixnQ0FBZ0MsRUFBQTs7QUFLcEM7RUFDSSxrQkFBa0IsRUFBQTs7QUFJdEI7RUFDSSxjQUFjO0VBQ2QsNkJBQTZCLEVBQUE7O0FBR2pDO0VBQ0ksZUFBZSxFQUFBOztBQUduQjtFQUVRLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsY0FBYyxFQUFBOztBQUl0QjtFQUNJLHlCQUF5QjtFQUN6QixtQkFBbUI7RUFDbkIsMkJBQTJCO0VBQzNCLHVCQUF1QjtFQUN2QixtQkFBbUIsRUFBQTs7QUFHdkI7RUFDSSxZQUFZO0VBRVosdUJBQXVCO0VBQ3ZCLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsZ0NBQWdDLEVBQUE7O0FBTnBDO0lBUVEsY0FBYyxFQUFBOztBQUl0QjtFQUNJLGdCQUFnQixFQUFBOztBQUlwQjtFQUNJLGNBQWM7RUFDZCxlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLGtCQUFrQixFQUFBOztBQUV0QjtFQUNJLGtCQUFrQjtFQUNsQixRQUFRO0VBQ1IsVUFBVTtFQUNWLGNBQWMsRUFBQTs7QUFFbEI7RUFDSSxhQUFhLEVBQUE7O0FBRGpCO0lBR1EsT0FBTyxFQUFBOztBQUlmO0VBQ0ksZ0JBQWdCO0VBQ2hCLGNBQWM7RUFDZCxXQUFXLEVBQUE7O0FBR2Y7RUFDSSxpQkFBaUI7RUFDakIsY0FBYztFQUNkLGFBQWEsRUFBQTs7QUFHakI7RUFDSSxPQUFPO0VBQ1AsaUJBQWlCLEVBQUE7O0FBRnJCO0lBSVEsY0FBYztJQUNkLGNBQWM7SUFDZCxhQUFhLEVBQUE7O0FBSXJCO0VBRVEsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixjQUFjLEVBQUE7O0FBSXRCO0VBQ0ksa0JBQWtCLEVBQUE7O0FBRHRCO0lBSVEsVUFBVTtJQUNWLFlBQVksRUFBQTs7QUFJcEI7RUFDSSxXQUFXO0VBQ1gsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixrQkFBa0IsRUFBQTs7QUFKdEI7SUFNUSxXQUFXO0lBQ1gsZ0JBQWdCLEVBQUE7O0FBSXhCO0VBQ0ksa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixTQUFTO0VBQ1QsT0FBTztFQUNQLFdBQVc7RUFDWCxvQ0FBb0MsRUFBQTs7QUFOeEM7SUFRUSxZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQixjQUFjLEVBQUE7O0FBSXRCO0VBRUksbUJBQW1CO0VBQ25CLDJCQUEyQjtFQUMzQixtQkFBbUIsRUFBQTs7QUFJdkI7RUFDSSx3QkFBd0I7RUFDeEIseUJBQXlCO0VBQ3pCLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIsaUJBQWlCO0VBQ2pCLGVBQWUsRUFBQTs7QUFObkI7SUFRUSxjQUFjLEVBQUE7O0FBSXRCO0VBQ0ksWUFBWSxFQUFBOztBQURoQjtJQUdRLGFBQWE7SUFDYixnQkFBZ0I7SUFDaEIsbUJBQW1CLEVBQUE7O0FBSTNCO0VBRVEsY0FBYztFQUNkLGtCQUFrQjtFQUNsQix1QkFBdUIsRUFBQTs7QUFJL0I7RUFFUSxpQkFBaUI7RUFDakIsY0FBYztFQUNkLGNBQWM7RUFDZCx1QkFBdUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2UvbWFpbm1lbnUvaG9tZS1tYWluL2hvbWUtbWFpbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCwgaW9uLWhlYWRlciB7XG4gICAgLy8gLS1pb24tYmFja2dyb3VuZC1jb2xvcjogI0JDRDlFNjtcbiAgICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cblxuaW9uLXRvb2xiYXIge1xuICAgIC0tYmFja2dyb3VuZDogI0JDRDlFNjtcbiAgICBoZWlnaHQ6IDYwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMHB4IDBweCAzMHB4IDMwcHg7XG4gICAgXG59XG5cblxuaW9uLXRpdGxlIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgLy8gbWFyZ2luOiAyNnB4O1xufVxuXG5pb24tdGl0bGUgPiBzcGFuIHtcbiAgICBjb2xvcjogIzAwQjRGOTtcbiAgICBmb250LWZhbWlseTogXCJPcGVuIFNhbnMgQm9sZFwiO1xufVxuXG4jaG9tZS1tYWludGFiLWZhdi1iZWFjaGVzLWJvZHkge1xuICAgIHBhZGRpbmc6IDAgMTZweDtcbn1cblxuI2hvbWUtbWFpbnRhYi1oZWFkZXIge1xuICAgIHAge1xuICAgICAgICBmb250LXNpemU6IDE0cHQ7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICBjb2xvcjogIzQyNjY3NTtcbiAgICB9XG59XG5cbi5ob21lLW1haW4tdGFiLXdlYXRoZXItcmVwb3J0cyB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0Y1RjVGNTtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgIGJveC1zaGFkb3c6IDAgMCA4cHggI2E0YTRhNDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xufVxuXG4uaG9tZS1tYWluLXRhYi13ZWF0aGVyLXJlcG9ydHMtbG9jYXRpb24ge1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiAjZGFkYWRhO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4IDEwcHggMHB4IDBweDtcbiAgICBzcGFuIHtcbiAgICAgICAgY29sb3I6ICMyYjJiMmI7XG4gICAgfVxufVxuXG4uaG9tZS1tYWluLXRhYi13ZWF0aGVyLXJlcG9ydHMtYm9keSB7XG4gICAgcGFkZGluZzogMHB4IDVweDtcbiAgICBcbn1cblxuLmhvbWUtbWFpbi10YWItd2VhdGhlci1yZXBvcnRzLXJlcG9ydHR4dCB7XG4gICAgY29sb3I6ICM2MjYyNjI7XG4gICAgZm9udC1zaXplOiAxMnB0O1xuICAgIG1hcmdpbjogNXB4IDAgMjBweDtcbiAgICBmb250LXN0eWxlOiBpdGFsaWM7XG59XG4uaWNvblN0eWxle1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDVweDtcbiAgICByaWdodDogNXB4O1xuICAgIHotaW5kZXg6IDEwMDAwO1xufVxuLmhvbWUtbWFpbi10YWItd2VhdGhlci1yZXBvcnRzLXdlYXRoZXJpbmZvIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGRpdiB7XG4gICAgICAgIGZsZXg6IDE7XG4gICAgfVxufVxuXG4uaG9tZS1tYWluLXRhYi13ZWF0aGVyLXJlcG9ydHMtd2F2ZWhlaWdodCB7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICBjb2xvcjogIzYyNjI2MjtcbiAgICBtYXJnaW46IDJweDtcbn1cblxuLmhvbWUtbWFpbi10YWItd2VhdGhlci1yZXBvcnRzLXdhdmVxdWFsaXR5IHtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICBjb2xvcjogIzYyNjI2MjtcbiAgICBtYXJnaW46IDVweCAwO1xufVxuXG4uaG9tZS1tYWluLXRhYi13ZWF0aGVyLXJlcG9ydHMtcmVwb3J0dGltZSB7XG4gICAgZmxleDogMTtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICBwIHtcbiAgICAgICAgZm9udC1zaXplOiA2cHQ7XG4gICAgICAgIGNvbG9yOiAjMmU1NTZlO1xuICAgICAgICBtYXJnaW46IDVweCAwO1xuICAgIH1cbn1cblxuI2hvbWUtbWFpbnRhYi1zaG9wLXJlcG9ydHMtaGVhZGVyLCAjaG9tZS1tYWludGFiLWZhdi1iZWFjaGhlcy1oZWFkZXIsICNob21lLW1haW50YWItZGlzY3Vzc2lvbnMtaGVhZGVyIHtcbiAgICBwIHtcbiAgICAgICAgZm9udC1zaXplOiAxNHB0O1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgY29sb3I6ICM0MjY2NzU7XG4gICAgfVxufVxuXG4jaG9tZS1tYWludGFiLXNob3AtcmVwb3J0cy1pbWctY29udGFpbmVyIHtcbiAgICBvdmVyZmxvdy14OiBzY3JvbGw7XG4vLyAgICBoZWlnaHQ6OTBweDtcbiAgICBpb24tY29sIHtcbiAgICAgICAgcGFkZGluZzogMDtcbiAgICAgICAgbWFyZ2luOiAxMHB4O1xuICAgIH1cbn1cblxuLmhvbWUtbWFpbnRhYi1zaG9wLXJlcG9ydHMtaXRlbWltZyB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMjBweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBpbWcge1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgbWluLWhlaWdodDogMTAwJTtcbiAgICB9XG59XG5cbi5ob21lLW1haW50YWItc2hvcC1yZXBvcnRzLWl0ZW1pbWctZGVzY3JpcHRpb24ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB6LWluZGV4OiAyO1xuICAgIGJvdHRvbTogMDtcbiAgICBsZWZ0OiAwO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC40KTtcbiAgICBwIHtcbiAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICBmb250LXNpemU6IDlwdDtcbiAgICB9XG59XG5cbi5ob21lLW1haW50YWItZGlzY3Vzc2lvbi1pdGVtIHtcbiAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiAjRjVGNUY1O1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgYm94LXNoYWRvdzogMCAwIDhweCAjYTRhNGE0O1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgXG59XG5cbi5ob21lLW1haW50YWItZGlzY3Vzc2lvbi1pdGVtLWhlYWRlciB7XG4gICAgcGFkZGluZzogNXB4IDVweCAwcHggNXB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNkYWRhZGE7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgc3BhbiB7XG4gICAgICAgIGNvbG9yOiAjMmIyYjJiO1xuICAgIH1cbn1cblxuLmhvbWUtbWFpbnRhYi1kaXNjdXNzaW9uLWl0ZW0tYm9keSwgLmhvbWUtbWFpbnRhYi1kaXNjdXNzaW9uLWl0ZW0tZm9vdGVyIHtcbiAgICBwYWRkaW5nOiAwcHg7XG4gICAgcCB7XG4gICAgICAgIG1hcmdpbjogNXB4IDA7XG4gICAgICAgIHBhZGRpbmc6IDJweCA1cHg7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgfVxufVxuXG4uaG9tZS1tYWludGFiLWRpc2N1c3Npb24taXRlbS1ib2R5IHtcbiAgICBwIHtcbiAgICAgICAgY29sb3I6ICM1NjU2NTY7XG4gICAgICAgIGZvbnQtc3R5bGU6IGl0YWxpYztcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgfVxufVxuXG4uaG9tZS1tYWludGFiLWRpc2N1c3Npb24taXRlbS1mb290ZXIge1xuICAgIHAge1xuICAgICAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICAgICAgZm9udC1zaXplOiA2cHQ7XG4gICAgICAgIGNvbG9yOiAjMmU1NTZlO1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICB9XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/page/mainmenu/home-main/home-main.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/page/mainmenu/home-main/home-main.page.ts ***!
  \***********************************************************/
/*! exports provided: HomeMainPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeMainPage", function() { return HomeMainPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_firebase_service_firebase_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/firebase-service/firebase.service */ "./src/app/services/firebase-service/firebase.service.ts");
/* harmony import */ var src_app_constants_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/constants/constants */ "./src/app/constants/constants.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_myutils_myutils_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/myutils/myutils.service */ "./src/app/services/myutils/myutils.service.ts");
/* harmony import */ var _modal_gallery_gallery_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../modal/gallery/gallery.page */ "./src/app/page/modal/gallery/gallery.page.ts");
/* harmony import */ var _ionic_native_streaming_media_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/streaming-media/ngx */ "./node_modules/@ionic-native/streaming-media/ngx/index.js");









var HomeMainPage = /** @class */ (function () {
    function HomeMainPage(firebaseService, router, event, loadingCtrl, myUtils, modalCtrl, streamingMedia) {
        var _this = this;
        this.firebaseService = firebaseService;
        this.router = router;
        this.event = event;
        this.loadingCtrl = loadingCtrl;
        this.myUtils = myUtils;
        this.modalCtrl = modalCtrl;
        this.streamingMedia = streamingMedia;
        this.currentUserId = '';
        this.arrWaveQualities = src_app_constants_constants__WEBPACK_IMPORTED_MODULE_4__["WAVEQUALITIES"];
        /**
         * Weather Reports
         */
        this.weatherReportdataRef = null;
        this.weatherReportdataListener = null;
        this.readmore = false;
        this.readmoreClicked = false;
        /**
         * Board Reports
         */
        this.boardDataRef = null;
        this.boardDataListener = null;
        this.allNewSurfBoards = [];
        /**
         * Recent Discussions
         */
        this.discussionDetailRef = null;
        this.discussionDetailListener = null;
        this.arrDiscussions = [];
        this.arrDiscussionsFilter = [];
        /**
         * User Info
         */
        this.userInfo = {};
        this.arrFavBeachesId = [];
        /**
         * Event Listener
         */
        this.authListener = null;
        this.countLoader = 0;
        this.dataLoader = null;
        this.authListener = this.event.subscribe('onAuth', function (data) {
            console.log('Auth Called!');
            _this.initPage();
        });
    }
    HomeMainPage.prototype.ngOnInit = function () {
    };
    HomeMainPage.prototype.ionViewWillEnter = function () {
        /**
         * Init page
         */
        this.initPage();
    };
    HomeMainPage.prototype.initPage = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _a;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0:
                        this.countLoader = 0;
                        this.currentUserId = this.firebaseService.getUID();
                        /**
                         * Weather
                         */
                        this.weatherReportdataRef = null;
                        this.weatherReportdataListener = null;
                        this.weatherReports = [];
                        /**
                         * Board
                         */
                        this.boardDataRef = null;
                        this.boardDataListener = null;
                        this.allNewSurfBoards = [];
                        /**
                         * Discussion
                         */
                        this.discussionDetailRef = null;
                        this.discussionDetailListener = null;
                        return [4 /*yield*/, this.getUserInfo()];
                    case 1:
                        _b.sent();
                        _a = this;
                        return [4 /*yield*/, this.loadingCtrl.create({
                                message: 'Please wait...'
                            })];
                    case 2:
                        _a.dataLoader = _b.sent();
                        return [4 /*yield*/, this.dataLoader.present()];
                    case 3:
                        _b.sent();
                        this.getAllLocationReports();
                        this.getNewBoardItems();
                        this.getDiscussionContents();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomeMainPage.prototype.getUserInfo = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var userInfoSnapshot;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.firebaseService.getUserProfile(this.currentUserId)];
                    case 1:
                        userInfoSnapshot = _a.sent();
                        this.userInfo = userInfoSnapshot.val();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomeMainPage.prototype.getAllLocationReports = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var self;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                self = this;
                this.arrFavBeachesId = this.userInfo.favorite.beaches.split(',');
                this.weatherReportdataRef = this.firebaseService.getAllWeatherReports();
                this.weatherReportdataListener = this.weatherReportdataRef.on('value', function (snapshot) {
                    self.weatherReports = [];
                    var tmpArr = [];
                    _this.arrFavBeachesId.forEach(function (element) {
                        snapshot.forEach(function (childSnapshot) {
                            var favIndex = (childSnapshot.key === element);
                            if (favIndex) {
                                childSnapshot.forEach(function (valueSnapshot) {
                                    tmpArr.push(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, valueSnapshot.val()));
                                });
                            }
                        });
                    });
                    // tmpArr.reverse();
                    // console.log(tmpArr);
                    var tempArray2 = [];
                    // if(tmpArr.length>4){
                    tmpArr.forEach(function (item) {
                        var i = tempArray2.findIndex(function (x) { return x.location.name == item.location.name && x.locationId == item.locationId; });
                        if (i <= -1) {
                            tempArray2.push(item);
                        }
                    });
                    // }  
                    //   console.log(tempArray2);     
                    //  if(tempArray2.length<4){  
                    //   tmpArr.forEach(function(item){
                    //     var i = tempArray2.findIndex(x => (x.id == item.id && x.locationId == item.locationId));
                    //     if(i <= -1){
                    //       tempArray2.push(item);
                    //     }
                    //   });
                    //  }
                    //  console.log(tempArray2.length);
                    if (tempArray2.length >= 4) {
                        self.weatherReports = tempArray2.sort(function (a, b) { return b.createdat - a.createdat; }).slice(0, 4);
                        _this.readmore = (tempArray2.length > 4 ? true : false);
                    }
                    else {
                        self.weatherReports = tempArray2.slice(0, tempArray2.length).sort(function (a, b) { return b.createdat - a.createdat; });
                    }
                    self.weatherReportsFilter = tempArray2.sort(function (a, b) { return b.createdat - a.createdat; });
                    // tmpArr;     
                    console.log(tmpArr);
                    console.log(self.weatherReports);
                    self.countLoader++;
                    if (self.countLoader == 2) {
                        self.dismissDataLoader();
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    HomeMainPage.prototype.onClickVideoAttachment = function (ev, path, type) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var galleryModal, streamingOptions;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ev.stopPropagation();
                        if (!(type == 0)) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.modalCtrl.create({
                                component: _modal_gallery_gallery_page__WEBPACK_IMPORTED_MODULE_7__["GalleryPage"],
                                componentProps: {
                                    imgs: [path]
                                },
                                cssClass: 'gallery-modal'
                            })];
                    case 1:
                        galleryModal = _a.sent();
                        return [4 /*yield*/, galleryModal.present()];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        streamingOptions = {
                            successCallback: function () {
                                console.log('Video played');
                            },
                            errorCallback: function (e) {
                                console.log('Error streaming');
                            },
                            orientation: 'portrait',
                            shouldAutoClose: true,
                            controls: false
                        };
                        this.streamingMedia.playVideo(path, streamingOptions);
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    HomeMainPage.prototype.getNewBoardItems = function () {
        var self = this;
        // const itemRange = Number(this.userInfo.noti.range);
        this.boardDataRef = this.firebaseService.getAllSurfBoardRealtime();
        this.boardDataListener = this.boardDataRef.on('value', function (snapshot) {
            self.allNewSurfBoards = [];
            snapshot.forEach(function (boardSnapshot) {
                var distance = self.myUtils.distance(self.userInfo.location.lat, self.userInfo.location.long, boardSnapshot.val().location.lat, boardSnapshot.val().location.long, 'N');
                // if (itemRange >= Number(distance)) {
                self.allNewSurfBoards.push(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, boardSnapshot.val()));
                // }
            });
            self.countLoader++;
            if (self.countLoader == 2) {
                self.dismissDataLoader();
            }
            self.allNewSurfBoards = self.allNewSurfBoards.sort(function (a, b) { return (b.createdat - a.createdat); });
        });
    };
    HomeMainPage.prototype.setDefault = function (item) {
        console.log(item);
        localStorage.setItem("defaultBeach", JSON.stringify(item));
    };
    HomeMainPage.prototype.getDiscussionContents = function () {
        var _this = this;
        var self = this;
        var favDiscussions = [];
        if (this.userInfo.favorite.discussions) {
            favDiscussions = this.userInfo.favorite.discussions.split(',');
        }
        this.discussionDetailRef = this.firebaseService.getAllDiscussionContents();
        this.discussionDetailListener = this.discussionDetailRef.on('value', function (snapshot) {
            _this.arrDiscussions = [];
            _this.arrDiscussionsFilter = [];
            snapshot.forEach(function (childSnapshot) {
                var isFavDiscussion = favDiscussions.indexOf(childSnapshot.key);
                if (isFavDiscussion > -1) {
                    var tmpArr_1 = [];
                    childSnapshot.forEach(function (valSnapshot) {
                        tmpArr_1.push(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, valSnapshot.val(), { discussionid: childSnapshot.key }));
                    });
                    if (tmpArr_1.length > 0) {
                        self.arrDiscussions.push(tmpArr_1[tmpArr_1.length - 1]);
                    }
                }
            });
            if (_this.arrDiscussions.length > 4) {
                _this.arrDiscussionsFilter = _this.arrDiscussions.slice(0, 4);
            }
            else {
                _this.arrDiscussionsFilter = _this.arrDiscussions;
            }
            _this.arrDiscussionsFilter.sort(function (x, y) {
                return y.createdat - x.createdat;
            });
        });
    };
    HomeMainPage.prototype.onClickDiscussionItem = function (item) {
        this.router.navigateByUrl('/home/main/discussion-detail/' + item.discussionid + '/' + item.title + '/home/1');
    };
    HomeMainPage.prototype.showMore = function (val) {
        if (val == 1) {
            this.arrDiscussionsFilter = this.arrDiscussions;
        }
        else {
            this.arrDiscussionsFilter = this.arrDiscussions.slice(0, 4);
        }
        this.arrDiscussionsFilter.sort(function (x, y) {
            return y.createdat - x.createdat;
        });
    };
    HomeMainPage.prototype.dismissDataLoader = function () {
        if (this.dataLoader !== null) {
            this.dataLoader.dismiss();
            this.dataLoader = null;
        }
    };
    HomeMainPage.prototype.show = function (val) {
        this.readmoreClicked = (val == 1 ? true : false);
    };
    HomeMainPage.prototype.viewItemDetail = function (itemid) {
        sessionStorage.setItem("fromHome", "1");
        this.router.navigateByUrl('/home/main/shopitemdetail/' + this.allNewSurfBoards[itemid].id + '/main');
    };
    HomeMainPage.prototype.onClickReportItem = function (report) {
        console.log("Check index", report);
        this.router.navigateByUrl('/home/main/locationdetail/' + report.locationId + '/' + btoa(report.location.name));
    };
    HomeMainPage.prototype.onClickFavBeach = function (beachIndex) {
        this.router.navigateByUrl('/locationdetail/' + this.arrFavBeachesId[beachIndex]);
    };
    HomeMainPage.prototype.ionViewWillLeave = function () {
        if (this.weatherReportdataListener !== null) {
            this.weatherReportdataRef.off('value', this.weatherReportdataListener);
            this.weatherReportdataListener = null;
            this.weatherReportdataRef = null;
        }
        if (this.boardDataListener !== null) {
            this.boardDataRef.off('value', this.boardDataListener);
            this.boardDataListener = null;
            this.boardDataRef = null;
        }
        if (this.authListener !== null) {
            this.event.unsubscribe('onAuth', this.authListener);
        }
    };
    HomeMainPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home-main',
            template: __webpack_require__(/*! ./home-main.page.html */ "./src/app/page/mainmenu/home-main/home-main.page.html"),
            styles: [__webpack_require__(/*! ./home-main.page.scss */ "./src/app/page/mainmenu/home-main/home-main.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_firebase_service_firebase_service__WEBPACK_IMPORTED_MODULE_3__["FirebaseService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Events"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"],
            src_app_services_myutils_myutils_service__WEBPACK_IMPORTED_MODULE_6__["MyutilsService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"],
            _ionic_native_streaming_media_ngx__WEBPACK_IMPORTED_MODULE_8__["StreamingMedia"]])
    ], HomeMainPage);
    return HomeMainPage;
}());



/***/ }),

/***/ "./src/app/page/modal/gallery/gallery.page.html":
/*!******************************************************!*\
  !*** ./src/app/page/modal/gallery/gallery.page.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content fullscreen padding>\n  <div id=\"gallery-modal-btnclose\">\n    <ion-icon name=\"close\" (click)=\"closeGalleryModal()\"></ion-icon>\n  </div>\n  <ion-slides [options]=\"sliderOpts\">\n    <ion-slide *ngFor=\"let img of imgs\">\n      <div class=\"swiper-zoom-container\">\n        <img [src]=\"img\" />\n      </div>\n    </ion-slide>\n  </ion-slides>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/page/modal/gallery/gallery.page.scss":
/*!******************************************************!*\
  !*** ./src/app/page/modal/gallery/gallery.page.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background: rgba(19, 19, 19, 0.45); }\n\n#gallery-modal-btnclose {\n  width: 100%;\n  text-align: right; }\n\n#gallery-modal-btnclose ion-icon {\n    font-size: 24pt;\n    color: white; }\n\nion-slides {\n  height: 80%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2xva2VzaC9saW5ldXAgc291cmNlKDA5LTMwKSAoMikvbGluZXVwKDA5LTMwKS9zcmMvYXBwL3BhZ2UvbW9kYWwvZ2FsbGVyeS9nYWxsZXJ5LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG9DQUFhLEVBQUE7O0FBR2pCO0VBQ0ksV0FBVztFQUNYLGlCQUFpQixFQUFBOztBQUZyQjtJQUlRLGVBQWU7SUFDZixZQUFZLEVBQUE7O0FBSXBCO0VBQ0ksV0FBVyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZS9tb2RhbC9nYWxsZXJ5L2dhbGxlcnkucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xuICAgIC0tYmFja2dyb3VuZDogcmdiYSgxOSwgMTksIDE5LCAwLjQ1KTtcbn1cblxuI2dhbGxlcnktbW9kYWwtYnRuY2xvc2Uge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIGlvbi1pY29uIHtcbiAgICAgICAgZm9udC1zaXplOiAyNHB0O1xuICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgfVxufVxuXG5pb24tc2xpZGVzIHtcbiAgICBoZWlnaHQ6IDgwJTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/page/modal/gallery/gallery.page.ts":
/*!****************************************************!*\
  !*** ./src/app/page/modal/gallery/gallery.page.ts ***!
  \****************************************************/
/*! exports provided: GalleryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GalleryPage", function() { return GalleryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var GalleryPage = /** @class */ (function () {
    function GalleryPage(modalCtrl) {
        this.modalCtrl = modalCtrl;
        this.sliderOpts = {
            zoom: {
                maxRatio: 3
            }
        };
    }
    GalleryPage.prototype.ngOnInit = function () {
    };
    GalleryPage.prototype.closeGalleryModal = function () {
        this.modalCtrl.dismiss();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('imgs'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GalleryPage.prototype, "imgs", void 0);
    GalleryPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-gallery',
            template: __webpack_require__(/*! ./gallery.page.html */ "./src/app/page/modal/gallery/gallery.page.html"),
            styles: [__webpack_require__(/*! ./gallery.page.scss */ "./src/app/page/modal/gallery/gallery.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
    ], GalleryPage);
    return GalleryPage;
}());



/***/ })

}]);
//# sourceMappingURL=home-main-home-main-module.js.map