(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["activitymenu-activity-detail-activity-detail-module"],{

/***/ "./src/app/page/mainmenu/activitymenu/activity-detail/activity-detail.module.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/page/mainmenu/activitymenu/activity-detail/activity-detail.module.ts ***!
  \**************************************************************************************/
/*! exports provided: ActivityDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActivityDetailPageModule", function() { return ActivityDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _component_component_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../component/component.module */ "./src/app/component/component.module.ts");
/* harmony import */ var _activity_detail_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./activity-detail.page */ "./src/app/page/mainmenu/activitymenu/activity-detail/activity-detail.page.ts");
/* harmony import */ var src_app_page_modal_modal_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/page/modal/modal.module */ "./src/app/page/modal/modal.module.ts");









var routes = [
    {
        path: '',
        component: _activity_detail_page__WEBPACK_IMPORTED_MODULE_7__["ActivityDetailPage"]
    }
];
var ActivityDetailPageModule = /** @class */ (function () {
    function ActivityDetailPageModule() {
    }
    ActivityDetailPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _component_component_module__WEBPACK_IMPORTED_MODULE_6__["ComponentModule"],
                src_app_page_modal_modal_module__WEBPACK_IMPORTED_MODULE_8__["ModalModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_activity_detail_page__WEBPACK_IMPORTED_MODULE_7__["ActivityDetailPage"]],
        })
    ], ActivityDetailPageModule);
    return ActivityDetailPageModule;
}());



/***/ }),

/***/ "./src/app/page/mainmenu/activitymenu/activity-detail/activity-detail.page.html":
/*!**************************************************************************************!*\
  !*** ./src/app/page/mainmenu/activitymenu/activity-detail/activity-detail.page.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button color=\"navback\"></ion-back-button>\n    </ion-buttons>\n    <!-- <ion-icon color=\"navback\" slot=\"start\" name=\"arrow-back\" (click)=\"onClickBackBtn()\"></ion-icon> -->\n    <ion-title>{{boardInfo?.title}}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <div id=\"activity-detail-board-imgs\">\n    <ng-container *ngIf=\"boardInfo?.imgs.length > 0; else noBoardImage\">\n      <div id=\"activity-detail-board-img-container\">\n        <img [src]=\"boardInfo?.imgs[0]\" />\n      </div>\n    </ng-container>\n    <ng-template #noBoardImage>\n      <p>\n        No Image\n      </p>\n    </ng-template>\n  </div>\n\n  <div id=\"activity-detail-message-container\">\n    <ng-container *ngFor=\"let message of detailMessages\">\n      <messagebox [message]=\"message\" [currentUser]=\"currentUserInfo.uid\" [isDisableProfileClick]=\"true\"></messagebox>\n    </ng-container>\n  </div>\n</ion-content>\n\n<ion-footer>\n  <ion-toolbar>\n    <ion-item>\n      <ion-input type=\"text\" [(ngModel)]=\"messageTxt\" placeholder=\"Your Message\"></ion-input>\n      <ion-icon slot=\"end\" name=\"mail\" (click)=\"sendMessage()\"></ion-icon>\n    </ion-item>\n  </ion-toolbar>\n</ion-footer>\n"

/***/ }),

/***/ "./src/app/page/mainmenu/activitymenu/activity-detail/activity-detail.page.scss":
/*!**************************************************************************************!*\
  !*** ./src/app/page/mainmenu/activitymenu/activity-detail/activity-detail.page.scss ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-header {\n  --ion-background-color: #84A1B3; }\n  ion-header ion-toolbar {\n    --background: #84A1B3; }\n  ion-header ion-toolbar ion-icon {\n      font-size: 16pt;\n      margin-left: 16px; }\n  ion-header ion-toolbar ion-title {\n      position: absolute;\n      left: 50%;\n      top: 50%;\n      transform: translate(-50%, -50%);\n      color: #CAE9F7; }\n  #activity-detail-board-imgs {\n  text-align: center; }\n  #activity-detail-board-img-container {\n  height: 100px;\n  overflow: hidden;\n  position: relative;\n  margin: 2vh 0 3vh 0; }\n  #activity-detail-board-img-container img {\n    position: absolute;\n    top: 50%;\n    left: 50%;\n    transform: translate(-50%, -50%);\n    max-width: 100px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9uaHovRGVza3RvcC9Mb2tlc2gvbnZoZ2doL3NyYy9hcHAvcGFnZS9tYWlubWVudS9hY3Rpdml0eW1lbnUvYWN0aXZpdHktZGV0YWlsL2FjdGl2aXR5LWRldGFpbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSwrQkFBdUIsRUFBQTtFQUQzQjtJQUdRLHFCQUFhLEVBQUE7RUFIckI7TUFLWSxlQUFlO01BQ2YsaUJBQWlCLEVBQUE7RUFON0I7TUFTWSxrQkFBa0I7TUFDbEIsU0FBUztNQUNULFFBQVE7TUFDUixnQ0FBZ0M7TUFDaEMsY0FBYyxFQUFBO0VBSzFCO0VBQ0ksa0JBQWtCLEVBQUE7RUFHdEI7RUFDSSxhQUFhO0VBQ2IsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixtQkFBbUIsRUFBQTtFQUp2QjtJQU1RLGtCQUFrQjtJQUNsQixRQUFRO0lBQ1IsU0FBUztJQUNULGdDQUFnQztJQUNoQyxnQkFBZ0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2UvbWFpbm1lbnUvYWN0aXZpdHltZW51L2FjdGl2aXR5LWRldGFpbC9hY3Rpdml0eS1kZXRhaWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWhlYWRlciB7XG4gICAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjogIzg0QTFCMztcbiAgICBpb24tdG9vbGJhciB7XG4gICAgICAgIC0tYmFja2dyb3VuZDogIzg0QTFCMztcbiAgICAgICAgaW9uLWljb24ge1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNnB0O1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDE2cHg7XG4gICAgICAgIH1cbiAgICAgICAgaW9uLXRpdGxlIHtcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgIGxlZnQ6IDUwJTtcbiAgICAgICAgICAgIHRvcDogNTAlO1xuICAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gICAgICAgICAgICBjb2xvcjogI0NBRTlGNztcbiAgICAgICAgfVxuICAgIH1cbn1cblxuI2FjdGl2aXR5LWRldGFpbC1ib2FyZC1pbWdzIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbiNhY3Rpdml0eS1kZXRhaWwtYm9hcmQtaW1nLWNvbnRhaW5lciB7XG4gICAgaGVpZ2h0OiAxMDBweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBtYXJnaW46IDJ2aCAwIDN2aCAwO1xuICAgIGltZyB7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgdG9wOiA1MCU7XG4gICAgICAgIGxlZnQ6IDUwJTtcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gICAgICAgIG1heC13aWR0aDogMTAwcHg7XG4gICAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/page/mainmenu/activitymenu/activity-detail/activity-detail.page.ts":
/*!************************************************************************************!*\
  !*** ./src/app/page/mainmenu/activitymenu/activity-detail/activity-detail.page.ts ***!
  \************************************************************************************/
/*! exports provided: ActivityDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActivityDetailPage", function() { return ActivityDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var src_app_services_firebase_service_firebase_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/firebase-service/firebase.service */ "./src/app/services/firebase-service/firebase.service.ts");
/* harmony import */ var src_app_services_myutils_myutils_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/myutils/myutils.service */ "./src/app/services/myutils/myutils.service.ts");







var ActivityDetailPage = /** @class */ (function () {
    function ActivityDetailPage(router, route, firebaseService, myUtils, loadingCtrl) {
        this.router = router;
        this.route = route;
        this.firebaseService = firebaseService;
        this.myUtils = myUtils;
        this.loadingCtrl = loadingCtrl;
        this.boardInfo = null;
        this.itemId = '';
        this.chatDataRef = null;
        this.chatDataListener = null;
        this.currentUserInfo = {};
        this.messageTxt = '';
        this.chatRoom = '';
        this.detailMessages = [];
    }
    ActivityDetailPage.prototype.ngOnInit = function () { };
    ActivityDetailPage.prototype.ionViewWillEnter = function () {
        this.initPage();
    };
    ActivityDetailPage.prototype.initPage = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.itemId = this.route.snapshot.paramMap.get('itemid');
                        this.chatUserId = this.route.snapshot.paramMap.get('poster');
                        return [4 /*yield*/, this.getChatHistory()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ActivityDetailPage.prototype.getChatHistory = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var messageLoader, cuid, userProfileSnapshot, self, boardInfoSnapshot, err_1;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Loading...'
                        })];
                    case 1:
                        messageLoader = _a.sent();
                        return [4 /*yield*/, messageLoader.present()];
                    case 2:
                        _a.sent();
                        cuid = this.firebaseService.getUID();
                        return [4 /*yield*/, this.firebaseService.getUserProfile(cuid)];
                    case 3:
                        userProfileSnapshot = _a.sent();
                        this.currentUserInfo = userProfileSnapshot.val();
                        if (cuid >= this.chatUserId) {
                            this.chatRoom = this.chatUserId + '-' + cuid;
                        }
                        else {
                            this.chatRoom = cuid + '-' + this.chatUserId;
                        }
                        console.log(cuid, this.chatUserId, this.chatRoom);
                        self = this;
                        _a.label = 4;
                    case 4:
                        _a.trys.push([4, 6, , 7]);
                        return [4 /*yield*/, this.firebaseService.getSpecificBoardInfo(this.itemId)];
                    case 5:
                        boardInfoSnapshot = _a.sent();
                        this.boardInfo = boardInfoSnapshot.val();
                        return [3 /*break*/, 7];
                    case 6:
                        err_1 = _a.sent();
                        messageLoader.dismiss();
                        console.log(err_1);
                        return [3 /*break*/, 7];
                    case 7:
                        this.chatDataRef = this.firebaseService.getMessageDataRef(this.itemId, this.chatRoom);
                        this.chatDataListener = this.chatDataRef.on('value', function (snapshot) {
                            self.detailMessages = [];
                            snapshot.forEach(function (chidlsnapshot) {
                                self.detailMessages.push(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, chidlsnapshot.val()));
                            });
                            if (messageLoader !== null) {
                                messageLoader.dismiss();
                            }
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ActivityDetailPage.prototype.onClickBoardImage = function () {
        this.clearDataListener();
        localStorage.setItem('posterid', this.chatUserId);
        this.router.navigateByUrl('/shopitemdetail/' + this.itemId + '/chat');
    };
    ActivityDetailPage.prototype.sendMessage = function () {
        if (this.messageTxt !== '') {
            var messageItem = {
                uid: this.firebaseService.getUID(),
                name: this.currentUserInfo.fname + ' ' + this.currentUserInfo.lname,
                photo: '',
                content: this.messageTxt,
                time: firebase_app__WEBPACK_IMPORTED_MODULE_4__["database"].ServerValue.TIMESTAMP,
                contentimg: this.boardInfo.imgs[0],
                boardName: this.boardInfo.title,
                isRead: false,
                location: {
                    lat: this.myUtils.currentUserLocation.lat,
                    long: this.myUtils.currentUserLocation.long
                },
                review: 0
            };
            this.firebaseService.sendMessage(this.itemId, this.chatRoom, messageItem);
            this.messageTxt = '';
        }
    };
    ActivityDetailPage.prototype.clearDataListener = function () {
        if (this.chatDataListener !== null) {
            this.chatDataRef.off('value', this.chatDataListener);
            this.chatDataListener = null;
            this.chatDataRef = null;
        }
    };
    ActivityDetailPage.prototype.onClickBackBtn = function () {
        this.clearDataListener();
        this.router.navigateByUrl('/home/activity');
    };
    ActivityDetailPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-activity-detail',
            template: __webpack_require__(/*! ./activity-detail.page.html */ "./src/app/page/mainmenu/activitymenu/activity-detail/activity-detail.page.html"),
            styles: [__webpack_require__(/*! ./activity-detail.page.scss */ "./src/app/page/mainmenu/activitymenu/activity-detail/activity-detail.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            src_app_services_firebase_service_firebase_service__WEBPACK_IMPORTED_MODULE_5__["FirebaseService"],
            src_app_services_myutils_myutils_service__WEBPACK_IMPORTED_MODULE_6__["MyutilsService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]])
    ], ActivityDetailPage);
    return ActivityDetailPage;
}());



/***/ }),

/***/ "./src/app/page/modal/gallery/gallery.page.html":
/*!******************************************************!*\
  !*** ./src/app/page/modal/gallery/gallery.page.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content fullscreen padding>\n  <div id=\"gallery-modal-btnclose\">\n    <ion-icon name=\"close\" (click)=\"closeGalleryModal()\"></ion-icon>\n  </div>\n  <ion-slides [options]=\"sliderOpts\">\n    <ion-slide *ngFor=\"let img of imgs\">\n      <div class=\"swiper-zoom-container\">\n        <img [src]=\"img\" />\n      </div>\n    </ion-slide>\n  </ion-slides>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/page/modal/gallery/gallery.page.scss":
/*!******************************************************!*\
  !*** ./src/app/page/modal/gallery/gallery.page.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background: rgba(19, 19, 19, 0.45); }\n\n#gallery-modal-btnclose {\n  width: 100%;\n  text-align: right; }\n\n#gallery-modal-btnclose ion-icon {\n    font-size: 24pt;\n    color: white; }\n\nion-slides {\n  height: 80%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9uaHovRGVza3RvcC9Mb2tlc2gvbnZoZ2doL3NyYy9hcHAvcGFnZS9tb2RhbC9nYWxsZXJ5L2dhbGxlcnkucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksb0NBQWEsRUFBQTs7QUFHakI7RUFDSSxXQUFXO0VBQ1gsaUJBQWlCLEVBQUE7O0FBRnJCO0lBSVEsZUFBZTtJQUNmLFlBQVksRUFBQTs7QUFJcEI7RUFDSSxXQUFXLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlL21vZGFsL2dhbGxlcnkvZ2FsbGVyeS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XG4gICAgLS1iYWNrZ3JvdW5kOiByZ2JhKDE5LCAxOSwgMTksIDAuNDUpO1xufVxuXG4jZ2FsbGVyeS1tb2RhbC1idG5jbG9zZSB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgaW9uLWljb24ge1xuICAgICAgICBmb250LXNpemU6IDI0cHQ7XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB9XG59XG5cbmlvbi1zbGlkZXMge1xuICAgIGhlaWdodDogODAlO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/page/modal/gallery/gallery.page.ts":
/*!****************************************************!*\
  !*** ./src/app/page/modal/gallery/gallery.page.ts ***!
  \****************************************************/
/*! exports provided: GalleryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GalleryPage", function() { return GalleryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var GalleryPage = /** @class */ (function () {
    function GalleryPage(modalCtrl) {
        this.modalCtrl = modalCtrl;
        this.sliderOpts = {
            zoom: {
                maxRatio: 3
            }
        };
    }
    GalleryPage.prototype.ngOnInit = function () {
    };
    GalleryPage.prototype.closeGalleryModal = function () {
        this.modalCtrl.dismiss();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('imgs'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GalleryPage.prototype, "imgs", void 0);
    GalleryPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-gallery',
            template: __webpack_require__(/*! ./gallery.page.html */ "./src/app/page/modal/gallery/gallery.page.html"),
            styles: [__webpack_require__(/*! ./gallery.page.scss */ "./src/app/page/modal/gallery/gallery.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
    ], GalleryPage);
    return GalleryPage;
}());



/***/ }),

/***/ "./src/app/page/modal/modal.module.ts":
/*!********************************************!*\
  !*** ./src/app/page/modal/modal.module.ts ***!
  \********************************************/
/*! exports provided: ModalModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalModule", function() { return ModalModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _gallery_gallery_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./gallery/gallery.page */ "./src/app/page/modal/gallery/gallery.page.ts");
/* harmony import */ var _writemessage_writemessage_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./writemessage/writemessage.page */ "./src/app/page/modal/writemessage/writemessage.page.ts");







var ModalModule = /** @class */ (function () {
    function ModalModule() {
    }
    ModalModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _gallery_gallery_page__WEBPACK_IMPORTED_MODULE_5__["GalleryPage"],
                _writemessage_writemessage_page__WEBPACK_IMPORTED_MODULE_6__["WritemessagePage"],
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"]
            ],
            exports: [
                _gallery_gallery_page__WEBPACK_IMPORTED_MODULE_5__["GalleryPage"],
                _writemessage_writemessage_page__WEBPACK_IMPORTED_MODULE_6__["WritemessagePage"],
            ],
            entryComponents: [
                _gallery_gallery_page__WEBPACK_IMPORTED_MODULE_5__["GalleryPage"],
                _writemessage_writemessage_page__WEBPACK_IMPORTED_MODULE_6__["WritemessagePage"],
            ]
        })
    ], ModalModule);
    return ModalModule;
}());



/***/ }),

/***/ "./src/app/page/modal/writemessage/writemessage.page.html":
/*!****************************************************************!*\
  !*** ./src/app/page/modal/writemessage/writemessage.page.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <ion-header no-border>\n  <ion-toolbar>\n    <ion-title></ion-title>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-content>\n  <div style=\"height: 40px;margin-left: 20px;display: flex;align-items: center;\"><h3>{{title}}</h3></div>\n  <ion-item>\n    <ion-label>\n      To:\n    </ion-label>\n    <ion-input [value]=\"supportEmail\" disabled=true></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-textarea [(ngModel)]=\"reportMessage\" rows=5></ion-textarea>\n  </ion-item>\n\n  <ion-item lines=\"none\" id=\"writemessage-btnsend\" class=\"ion-margin-top\">\n    <div>\n      <ion-button (click)=\"onClickSendBtn()\">Send</ion-button>\n    </div>\n  </ion-item>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/page/modal/writemessage/writemessage.page.scss":
/*!****************************************************************!*\
  !*** ./src/app/page/modal/writemessage/writemessage.page.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#writemessage-btnsend div {\n  width: 100%;\n  text-align: center; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9uaHovRGVza3RvcC9Mb2tlc2gvbnZoZ2doL3NyYy9hcHAvcGFnZS9tb2RhbC93cml0ZW1lc3NhZ2Uvd3JpdGVtZXNzYWdlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVRLFdBQVc7RUFDWCxrQkFBa0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2UvbW9kYWwvd3JpdGVtZXNzYWdlL3dyaXRlbWVzc2FnZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjd3JpdGVtZXNzYWdlLWJ0bnNlbmQge1xuICAgIGRpdiB7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/page/modal/writemessage/writemessage.page.ts":
/*!**************************************************************!*\
  !*** ./src/app/page/modal/writemessage/writemessage.page.ts ***!
  \**************************************************************/
/*! exports provided: WritemessagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WritemessagePage", function() { return WritemessagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_email_composer_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/email-composer/ngx */ "./node_modules/@ionic-native/email-composer/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_showtoast_showtoast_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/showtoast/showtoast.service */ "./src/app/services/showtoast/showtoast.service.ts");
/* harmony import */ var src_app_constants_constants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/constants/constants */ "./src/app/constants/constants.ts");






var WritemessagePage = /** @class */ (function () {
    function WritemessagePage(modalCtrl, emailComposer, toastService) {
        this.modalCtrl = modalCtrl;
        this.emailComposer = emailComposer;
        this.toastService = toastService;
        this.reportMessage = '';
        this.title = '';
        this.supportEmail = src_app_constants_constants__WEBPACK_IMPORTED_MODULE_5__["SUPPORTEMAIL"];
    }
    WritemessagePage.prototype.ngOnInit = function () {
        var subject = '';
        if (this.reporttype == 0) {
            subject = 'Report Abuse';
        }
        else if (this.reporttype == 1) {
            subject = 'Report User';
        }
        else if (this.reporttype == 2) {
            subject = 'Suggestions';
        }
        else {
            subject = 'Missing Beach?';
        }
        this.title = subject;
    };
    WritemessagePage.prototype.onClickSendBtn = function () {
        var subject = '';
        if (this.reporttype == 0) {
            subject = 'Report Abuse';
        }
        else if (this.reporttype == 1) {
            subject = 'Report User';
        }
        else if (this.reporttype == 2) {
            subject = 'Suggestions';
        }
        else {
            subject = 'Missing Beach?';
        }
        var email = {
            to: this.supportEmail,
            cc: '',
            bcc: [],
            attachments: [],
            subject: subject,
            body: this.reportMessage,
            isHtml: false
        };
        this.emailComposer.open(email);
        this.modalCtrl.dismiss();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('reporttype'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], WritemessagePage.prototype, "reporttype", void 0);
    WritemessagePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-writemessage',
            template: __webpack_require__(/*! ./writemessage.page.html */ "./src/app/page/modal/writemessage/writemessage.page.html"),
            styles: [__webpack_require__(/*! ./writemessage.page.scss */ "./src/app/page/modal/writemessage/writemessage.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
            _ionic_native_email_composer_ngx__WEBPACK_IMPORTED_MODULE_2__["EmailComposer"],
            src_app_services_showtoast_showtoast_service__WEBPACK_IMPORTED_MODULE_4__["ShowtoastService"]])
    ], WritemessagePage);
    return WritemessagePage;
}());



/***/ }),

/***/ "./src/app/services/showtoast/showtoast.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/services/showtoast/showtoast.service.ts ***!
  \*********************************************************/
/*! exports provided: ShowtoastService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShowtoastService", function() { return ShowtoastService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var ShowtoastService = /** @class */ (function () {
    function ShowtoastService(toastCtrl) {
        this.toastCtrl = toastCtrl;
    }
    ShowtoastService.prototype.showToast = function (message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: message,
                            duration: 3000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    ShowtoastService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]])
    ], ShowtoastService);
    return ShowtoastService;
}());



/***/ })

}]);
//# sourceMappingURL=activitymenu-activity-detail-activity-detail-module.js.map