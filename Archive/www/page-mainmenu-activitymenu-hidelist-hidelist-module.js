(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["page-mainmenu-activitymenu-hidelist-hidelist-module"],{

/***/ "./src/app/page/mainmenu/activitymenu/hidelist/hidelist.module.ts":
/*!************************************************************************!*\
  !*** ./src/app/page/mainmenu/activitymenu/hidelist/hidelist.module.ts ***!
  \************************************************************************/
/*! exports provided: HidelistPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HidelistPageModule", function() { return HidelistPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");
/* harmony import */ var _hidelist_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./hidelist.page */ "./src/app/page/mainmenu/activitymenu/hidelist/hidelist.page.ts");








var routes = [
    {
        path: '',
        component: _hidelist_page__WEBPACK_IMPORTED_MODULE_7__["HidelistPage"]
    }
];
var HidelistPageModule = /** @class */ (function () {
    function HidelistPageModule() {
    }
    HidelistPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_6__["PipesModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_hidelist_page__WEBPACK_IMPORTED_MODULE_7__["HidelistPage"]]
        })
    ], HidelistPageModule);
    return HidelistPageModule;
}());



/***/ }),

/***/ "./src/app/page/mainmenu/activitymenu/hidelist/hidelist.page.html":
/*!************************************************************************!*\
  !*** ./src/app/page/mainmenu/activitymenu/hidelist/hidelist.page.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\n  <div id=\"hidelist-btnclose\">\n    <ion-icon name=\"close\" (click)=\"onClickBackBtn()\"></ion-icon>\n  </div>\n\n  <ion-list padding>\n    <div id=\"hidelist-contact-container\">\n      <div class=\"home-activity-contact-item\" *ngFor=\"let contact of arrHideItems; let i = index;\">\n        <div class=\"home-activity-contact-img\">\n          <div class=\"home-activity-contact-boardimg\">\n            <img [src]=\"contact.boardImg\" />\n          </div>\n        </div>\n        <div class=\"home-activity-contact-main\">\n          <div>\n            <p>\n              {{contact.boardName}}\n            </p>\n            <p>\n              Item: {{contact.name | formatname}}\n            </p>\n            <p (click)=\"onClickMessageControlBtn($event, contact.itemid)\">\n              UnHide\n            </p>\n          </div>\n        </div>\n      </div>\n      \n    </div>\n  </ion-list>\n  <div *ngIf=\"arrHideItems.length === 0\" style=\"text-align: center;color:rgba(0,0,0,0.2)\">\n    No hidden conversation\n </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/page/mainmenu/activitymenu/hidelist/hidelist.page.scss":
/*!************************************************************************!*\
  !*** ./src/app/page/mainmenu/activitymenu/hidelist/hidelist.page.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#hidelist-btnclose {\n  text-align: right;\n  width: 100%;\n  padding: 24px 16px 16px 16px; }\n  #hidelist-btnclose ion-icon {\n    font-size: 16pt; }\n  .home-activity-contact-item {\n  display: flex;\n  margin-bottom: 2vh;\n  padding: 10px;\n  box-shadow: 0 0 8px #eaeaea; }\n  .home-activity-contact-img {\n  display: flex;\n  align-items: center; }\n  .home-activity-contact-boardimg {\n  width: 100px;\n  height: 100px;\n  overflow: hidden;\n  position: relative; }\n  .home-activity-contact-boardimg img {\n    position: absolute;\n    top: 50%;\n    left: 50%;\n    transform: translate(-50%, -50%); }\n  .home-activity-contact-main {\n  flex: 6;\n  padding-left: 16px;\n  display: flex;\n  align-items: center; }\n  .home-activity-contact-main p {\n    margin: 5px 0; }\n  .home-activity-contact-main > div > p:first-child {\n  color: #5a5a5a;\n  font-size: 12pt; }\n  .home-activity-contact-main > div > p:nth-child(2) {\n  color: #5a5a5a;\n  font-style: italic;\n  font-size: 10pt; }\n  .home-activity-contact-main > div > p:nth-child(3) {\n  font-size: 10pt;\n  color: #3594d0;\n  text-decoration: underline; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9uaHovRGVza3RvcC9Mb2tlc2gvbnZoZ2doL3NyYy9hcHAvcGFnZS9tYWlubWVudS9hY3Rpdml0eW1lbnUvaGlkZWxpc3QvaGlkZWxpc3QucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQWlCO0VBQ2pCLFdBQVc7RUFDWCw0QkFBNEIsRUFBQTtFQUhoQztJQUtRLGVBQWUsRUFBQTtFQUl2QjtFQUNJLGFBQWE7RUFDYixrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLDJCQUEyQixFQUFBO0VBRy9CO0VBQ0ksYUFBYTtFQUNiLG1CQUFtQixFQUFBO0VBR3ZCO0VBQ0ksWUFBWTtFQUNaLGFBQWE7RUFDYixnQkFBZ0I7RUFDaEIsa0JBQWtCLEVBQUE7RUFKdEI7SUFNUSxrQkFBa0I7SUFDbEIsUUFBUTtJQUNSLFNBQVM7SUFDVCxnQ0FBZ0MsRUFBQTtFQUl4QztFQUNJLE9BQU87RUFDUCxrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLG1CQUFtQixFQUFBO0VBSnZCO0lBTVEsYUFBYSxFQUFBO0VBSXJCO0VBQ0ksY0FBYztFQUNkLGVBQWUsRUFBQTtFQUduQjtFQUNJLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsZUFBZSxFQUFBO0VBR25CO0VBQ0ksZUFBZTtFQUNmLGNBQWM7RUFDZCwwQkFBMEIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2UvbWFpbm1lbnUvYWN0aXZpdHltZW51L2hpZGVsaXN0L2hpZGVsaXN0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNoaWRlbGlzdC1idG5jbG9zZSB7XG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcGFkZGluZzogMjRweCAxNnB4IDE2cHggMTZweDtcbiAgICBpb24taWNvbiB7XG4gICAgICAgIGZvbnQtc2l6ZTogMTZwdDtcbiAgICB9XG59XG5cbi5ob21lLWFjdGl2aXR5LWNvbnRhY3QtaXRlbSB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBtYXJnaW4tYm90dG9tOiAydmg7XG4gICAgcGFkZGluZzogMTBweDtcbiAgICBib3gtc2hhZG93OiAwIDAgOHB4ICNlYWVhZWE7XG59XG5cbi5ob21lLWFjdGl2aXR5LWNvbnRhY3QtaW1nIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG5cbi5ob21lLWFjdGl2aXR5LWNvbnRhY3QtYm9hcmRpbWcge1xuICAgIHdpZHRoOiAxMDBweDtcbiAgICBoZWlnaHQ6IDEwMHB4O1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGltZyB7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgdG9wOiA1MCU7XG4gICAgICAgIGxlZnQ6IDUwJTtcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gICAgfVxufVxuXG4uaG9tZS1hY3Rpdml0eS1jb250YWN0LW1haW4ge1xuICAgIGZsZXg6IDY7XG4gICAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBwIHtcbiAgICAgICAgbWFyZ2luOiA1cHggMDtcbiAgICB9XG59XG5cbi5ob21lLWFjdGl2aXR5LWNvbnRhY3QtbWFpbiA+IGRpdiA+IHA6Zmlyc3QtY2hpbGQge1xuICAgIGNvbG9yOiAjNWE1YTVhO1xuICAgIGZvbnQtc2l6ZTogMTJwdDtcbn1cblxuLmhvbWUtYWN0aXZpdHktY29udGFjdC1tYWluID4gZGl2ID4gcDpudGgtY2hpbGQoMikge1xuICAgIGNvbG9yOiAjNWE1YTVhO1xuICAgIGZvbnQtc3R5bGU6IGl0YWxpYztcbiAgICBmb250LXNpemU6IDEwcHQ7XG59XG5cbi5ob21lLWFjdGl2aXR5LWNvbnRhY3QtbWFpbiA+IGRpdiA+IHA6bnRoLWNoaWxkKDMpIHtcbiAgICBmb250LXNpemU6IDEwcHQ7XG4gICAgY29sb3I6ICMzNTk0ZDA7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG59Il19 */"

/***/ }),

/***/ "./src/app/page/mainmenu/activitymenu/hidelist/hidelist.page.ts":
/*!**********************************************************************!*\
  !*** ./src/app/page/mainmenu/activitymenu/hidelist/hidelist.page.ts ***!
  \**********************************************************************/
/*! exports provided: HidelistPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HidelistPage", function() { return HidelistPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_myutils_myutils_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/myutils/myutils.service */ "./src/app/services/myutils/myutils.service.ts");




var HidelistPage = /** @class */ (function () {
    function HidelistPage(navCtrl, myUtils, event) {
        this.navCtrl = navCtrl;
        this.myUtils = myUtils;
        this.event = event;
        this.arrContacts = [];
        this.arrHideLists = [];
        this.arrHideItems = [];
    }
    HidelistPage.prototype.ngOnInit = function () {
    };
    HidelistPage.prototype.ionViewWillEnter = function () {
        this.initPage();
    };
    HidelistPage.prototype.initPage = function () {
        var _this = this;
        this.arrHideLists = [];
        this.arrContacts = this.myUtils.getChatHistory();
        if (localStorage.getItem("hidelist") !== null && localStorage.getItem("hidelist") !== "") {
            var strHideList = localStorage.getItem("hidelist");
            this.arrHideLists = strHideList.split(",");
        }
        this.arrHideItems = this.arrContacts.filter(function (x) {
            return _this.arrHideLists.indexOf(x.itemid) > -1;
        });
    };
    HidelistPage.prototype.onClickMessageControlBtn = function (evt, itemid) {
        var arrIndex = this.arrHideLists.indexOf(itemid);
        var itemIndex = this.arrHideItems.findIndex(function (x) { return x.itemid == itemid; });
        if (arrIndex > -1) {
            this.arrHideLists.splice(arrIndex, 1);
        }
        if (itemIndex > -1) {
            this.arrHideItems.splice(itemIndex, 1);
        }
        if (this.arrHideLists.length > 0) {
            localStorage.setItem("hidelist", this.arrHideLists.join(","));
        }
        else {
            localStorage.setItem("hidelist", "");
        }
    };
    HidelistPage.prototype.onClickBackBtn = function () {
        this.event.publish("onchangehidelist");
        this.navCtrl.back();
    };
    HidelistPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-hidelist',
            template: __webpack_require__(/*! ./hidelist.page.html */ "./src/app/page/mainmenu/activitymenu/hidelist/hidelist.page.html"),
            styles: [__webpack_require__(/*! ./hidelist.page.scss */ "./src/app/page/mainmenu/activitymenu/hidelist/hidelist.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], src_app_services_myutils_myutils_service__WEBPACK_IMPORTED_MODULE_3__["MyutilsService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Events"]])
    ], HidelistPage);
    return HidelistPage;
}());



/***/ })

}]);
//# sourceMappingURL=page-mainmenu-activitymenu-hidelist-hidelist-module.js.map