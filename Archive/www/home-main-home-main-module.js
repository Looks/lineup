(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-main-home-main-module"],{

/***/ "./src/app/page/mainmenu/home-main/home-main.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/page/mainmenu/home-main/home-main.module.ts ***!
  \*************************************************************/
/*! exports provided: HomeMainPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeMainPageModule", function() { return HomeMainPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _home_main_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./home-main.page */ "./src/app/page/mainmenu/home-main/home-main.page.ts");








var HomeMainPageModule = /** @class */ (function () {
    function HomeMainPageModule() {
    }
    HomeMainPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_5__["PipesModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild([{ path: '', component: _home_main_page__WEBPACK_IMPORTED_MODULE_7__["HomeMainPage"] }])
            ],
            declarations: [_home_main_page__WEBPACK_IMPORTED_MODULE_7__["HomeMainPage"]]
        })
    ], HomeMainPageModule);
    return HomeMainPageModule;
}());



/***/ }),

/***/ "./src/app/page/mainmenu/home-main/home-main.page.html":
/*!*************************************************************!*\
  !*** ./src/app/page/mainmenu/home-main/home-main.page.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-toolbar>\n    <ion-title>\n      <div style=\"display: flex;align-items: center;justify-content: center;\">\n      <span>LINEUP</span><img src=\"assets/imgs/app_icon_2.png\" style=\"width: 40px;\"/>     \n     </div>\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>     \n\n  <div id=\"home-maintab-header\">\n    <p style=\"margin: 5px 5px 5px 0px;\">Latest Reports</p>\n  </div>\n\n  <!-- Weather Reports -->\n  <div id=\"home-maintab-weather-reports-container row\" style=\"padding: 6px;\">\n    <div *ngIf=\"readmoreClicked == false\" style=\"max-height:50%\">\n    <div class=\"home-main-tab-weather-reports\" *ngFor=\"let weatherItem of weatherReports; let i = index;\"  (click)=\"onClickReportItem(weatherItem)\" style=\"display: flex;\">\n      <div style=\"padding: 5px;width: 80px;height: 80px;position: relative;\" *ngIf=\"weatherItem.postfile && weatherItem.postfile.data!=''\"\n            class=\"home-location-detail-reports-img\">\n          <img *ngIf=\"weatherItem.postfile.type==0\" [src]=\"weatherItem.postfile.data\"\n                (click)=\"onClickVideoAttachment($event,weatherItem.postfile.data, weatherItem.postfile.type)\" style=\"width: 100%;height: 100%;object-fit: cover;\" />\n          <img *ngIf=\"weatherItem.postfile.type==1\" [src]=\"weatherItem.postfile.thumb\"\n                (click)=\"onClickVideoAttachment($event,weatherItem.postfile.data, weatherItem.postfile.type)\" style=\"width: 100%;height: 100%;object-fit: contain;\" />\n                <ion-icon *ngIf=\"weatherItem.postfile.type==1\" name=\"play\" style=\"position: absolute;\n                color: white;\n                left: calc(50% - 10px);\n                top: calc(50% - 10px);\"></ion-icon>\n      </div>\n      <div style=\"width: 100%;position: relative;\">\n        <div class=\"home-main-tab-weather-reports-location\" style=\"padding-bottom: 0;\">\n          <span>{{weatherItem.location.name}}</span>\n          <p class=\"home-main-tab-weather-reports-waveheight\" style=\"font-size: 10px;\">\n            Height: {{weatherItem.waveHeight | round: true}} ft  \n            Form: {{arrWaveQualities[weatherItem.waveQuality]}}\n          </p>\n        </div>\n        <!-- <ion-icon name=\"more\" class=\"iconStyle\" (click)=\"setDefault(weatherItem)\"></ion-icon> -->\n\n        <div class=\"home-main-tab-weather-reports-body\">\n          <div>\n            <p class=\"home-main-tab-weather-reports-reporttxt\" *ngIf=\"weatherItem.reporttxt!=''\" >\n              &quot;{{(weatherItem.reporttxt.length>250)? (weatherItem.reporttxt | slice:0:250)+' ...':(weatherItem.reporttxt)}}&quot;\n            </p>\n          </div>\n          <div class=\"home-main-tab-weather-reports-reporttime\" style=\"position: absolute;right: 6px;bottom: 0px;\">\n            <p>\n              {{weatherItem.username | formatname}}.&nbsp;{{weatherItem.createdat | millitotime: true}}\n            </p>\n          </div>\n        </div>\n      </div>\n    </div>\n    </div>\n   \n    <div *ngIf=\"readmoreClicked\"  style=\"max-height:50%\">\n    <div class=\"home-main-tab-weather-reports\" *ngFor=\"let weatherItem of weatherReportsFilter; let i = index;\" (click)=\"onClickReportItem(weatherItem)\" style=\"display: flex;\">\n      <div style=\"padding: 5px;width: 80px;height: 80px;\" *ngIf=\"weatherItem.postfile && weatherItem.postfile.data!=''\"\n      class=\"home-location-detail-reports-img\">\n    <img *ngIf=\"weatherItem.postfile.type==0\" [src]=\"weatherItem.postfile.data\"\n          (click)=\"onClickVideoAttachment(weatherItem.postfile.data, weatherItem.postfile.type)\" style=\"width: 100%;height: 100%;object-fit: cover;\"/>\n    <img *ngIf=\"weatherItem.postfile.type==1\" [src]=\"weatherItem.postfile.thumb\"\n          (click)=\"onClickVideoAttachment(weatherItem.postfile.data, weatherItem.postfile.type)\" style=\"width: 100%;height: 100%;object-fit: contain;\"/>\n</div>\n<div style=\"width: 100%;position: relative;\">\n  <div class=\"home-main-tab-weather-reports-location\" style=\"padding-bottom: 0;\">\n    <span>{{weatherItem.location.name}}</span>\n    <p class=\"home-main-tab-weather-reports-waveheight\" style=\"font-size: 10px;\">\n      Height: {{weatherItem.waveHeight | round: true}}ft  \n      Form: {{arrWaveQualities[weatherItem.waveQuality]}}\n    </p>\n  </div>\n  <div class=\"home-main-tab-weather-reports-body\" >\n    \n    <div>\n      <p class=\"home-main-tab-weather-reports-reporttxt\" *ngIf=\"weatherItem.reporttxt!=''\" >\n        &quot;{{(weatherItem.reporttxt.length>250)? (weatherItem.reporttxt | slice:0:250)+' ...':(weatherItem.reporttxt)}}&quot;\n      </p>\n    </div>\n    <div class=\"home-main-tab-weather-reports-reporttime\" style=\"position: absolute;right: 6px;bottom: 0px;\">\n      <p>\n        {{weatherItem.username | formatname}}.&nbsp;{{weatherItem.createdat | millitotime: true}}\n      </p>\n    </div>\n  </div>\n</div>\n    </div>\n    </div>\n    <div>\n      <p *ngIf=\"weatherReportsFilter && weatherReportsFilter.length > 4 && readmoreClicked == false\" (click)=\"show(1)\" style=\"margin:0px;text-align: end;\n      font-size: 12px;\n      color: blue;\">show more</p>\n      <p *ngIf=\"weatherReportsFilter &&  weatherReportsFilter.length > 4 && readmoreClicked\" (click)=\"show(0)\" style=\"margin:0px;text-align: end;\n      font-size: 12px;\n      color: blue;\">show less</p>\n    </div>     \n  </div>\n\n  <!-- Shop Reports -->\n  <div id=\"home-maintab-shop-reports-container\">\n    <div id=\"home-maintab-shop-reports-header\">\n      <p style=\"margin:0px;\">Recent Offers</p>\n    </div>\n    <div id=\"home-maintab-shop-reports-body\" *ngIf=\"allNewSurfBoards.length\">\n      <div class=\"home-maintab-shop-reports-item\">\n        <ion-grid style=\"padding:0px;\">\n          <ion-row nowrap id=\"home-maintab-shop-reports-img-container\">\n            <ion-col size=\"3\" *ngFor=\"let boardItem of allNewSurfBoards; let i = index;\"  [style.margin]=\"(i==0?'0px 0px 0px 0px':'0px 0px 0px 3px')\">\n              <div class=\"home-maintab-shop-reports-itemimg\" (click)=\"viewItemDetail(i)\">\n                <img src=\"{{boardItem.imgs?boardItem.imgs[0]:'assets/imgs/img-surfboard.svg'}}\" />\n                <div class=\"home-maintab-shop-reports-itemimg-description\">\n                  <p>\n                    {{boardItem.title}}\n                  </p>\n                </div>\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </div>\n    </div>\n  </div>\n\n  <!-- Recent Discussions -->\n  <div id=\"home-maintab-discussions-container\">\n    <div id=\"home-maintab-discussions-header\">\n      <p style=\"margin:10px 0px 0px 0px;\">Discussions</p>\n    </div>\n    <div id=\"home-maintab-discussions-body\">\n      <div class=\"home-maintab-discussion-item\" *ngFor=\"let discussion of arrDiscussionsFilter\" (click)=\"onClickDiscussionItem(discussion)\">\n        <div class=\"home-maintab-discussion-item-header\">\n          <span>\n            {{discussion.title}}\n          </span>\n        </div>\n        <div class=\"home-maintab-discussion-item-body\">\n          <p>\n            &quot;{{discussion.content}}&quot;\n          </p>\n        </div>\n        <div class=\"home-maintab-discussion-item-footer\">\n          <p>\n            {{discussion.postername | formatname}}.&nbsp;{{discussion.createdat | millitotime: true}}\n          </p>\n        </div>\n      </div>\n\n      <div>\n        <p *ngIf=\"arrDiscussionsFilter && arrDiscussionsFilter.length <= 4 && arrDiscussions.length > 4\" (click)=\"showMore(1)\" style=\"margin:0px;text-align: end;\n        font-size: 12px;\n        color: blue;\">show more</p>\n        <p *ngIf=\"arrDiscussionsFilter &&  arrDiscussionsFilter.length > 4\" (click)=\"showMore(0)\" style=\"margin:0px;text-align: end;\n        font-size: 12px;\n        color: blue;\">show less</p>\n      </div>  \n    </div>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/page/mainmenu/home-main/home-main.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/page/mainmenu/home-main/home-main.page.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content, ion-header {\n  --ion-background-color: white; }\n\nion-toolbar {\n  --background: #BCD9E6;\n  padding: 0px !important;\n  border-radius: 0px 0px 30px 30px !important; }\n\nion-title {\n  text-align: center; }\n\nion-title > span {\n  color: #00B4F9;\n  font-family: \"Open Sans Bold\"; }\n\n#home-maintab-fav-beaches-body {\n  padding: 0 16px; }\n\n#home-maintab-header p {\n  font-size: 14pt;\n  font-weight: bold;\n  color: #426675; }\n\n.home-main-tab-weather-reports {\n  background-color: #F5F5F5;\n  margin-bottom: 10px;\n  box-shadow: 0 0 8px #a4a4a4;\n  background-color: white;\n  border-radius: 10px; }\n\n.home-main-tab-weather-reports-location {\n  padding: 5px;\n  background-color: white;\n  font-weight: bold;\n  font-size: 18px;\n  border-radius: 10px 10px 0px 0px; }\n\n.home-main-tab-weather-reports-location span {\n    color: #2b2b2b; }\n\n.home-main-tab-weather-reports-body {\n  padding: 0px 5px; }\n\n.home-main-tab-weather-reports-reporttxt {\n  color: #626262;\n  font-size: 12pt;\n  margin: 5px 0 20px;\n  font-style: italic; }\n\n.iconStyle {\n  position: absolute;\n  top: 5px;\n  right: 5px;\n  z-index: 10000; }\n\n.home-main-tab-weather-reports-weatherinfo {\n  display: flex; }\n\n.home-main-tab-weather-reports-weatherinfo div {\n    flex: 1; }\n\n.home-main-tab-weather-reports-waveheight {\n  text-align: left;\n  color: #626262;\n  margin: 2px; }\n\n.home-main-tab-weather-reports-wavequality {\n  text-align: right;\n  color: #626262;\n  margin: 5px 0; }\n\n.home-main-tab-weather-reports-reporttime {\n  flex: 1;\n  text-align: right; }\n\n.home-main-tab-weather-reports-reporttime p {\n    font-size: 6pt;\n    color: #2e556e;\n    margin: 5px 0; }\n\n#home-maintab-shop-reports-header p, #home-maintab-fav-beachhes-header p, #home-maintab-discussions-header p {\n  font-size: 14pt;\n  font-weight: bold;\n  color: #426675; }\n\n#home-maintab-shop-reports-img-container {\n  overflow-x: scroll; }\n\n#home-maintab-shop-reports-img-container ion-col {\n    padding: 0;\n    margin: 10px; }\n\n.home-maintab-shop-reports-itemimg {\n  width: 100%;\n  height: 120px;\n  overflow: hidden;\n  position: relative; }\n\n.home-maintab-shop-reports-itemimg img {\n    width: 100%;\n    min-height: 100%; }\n\n.home-maintab-shop-reports-itemimg-description {\n  position: absolute;\n  z-index: 2;\n  bottom: 0;\n  left: 0;\n  width: 100%;\n  background-color: rgba(0, 0, 0, 0.4); }\n\n.home-maintab-shop-reports-itemimg-description p {\n    color: white;\n    text-align: center;\n    font-weight: bold;\n    font-size: 9pt; }\n\n.home-maintab-discussion-item {\n  margin-bottom: 10px;\n  box-shadow: 0 0 8px #a4a4a4;\n  border-radius: 10px; }\n\n.home-maintab-discussion-item-header {\n  padding: 5px 5px 0px 5px;\n  background-color: #dadada;\n  background-color: white;\n  border-radius: 10px;\n  font-weight: bold;\n  font-size: 18px; }\n\n.home-maintab-discussion-item-header span {\n    color: #2b2b2b; }\n\n.home-maintab-discussion-item-body, .home-maintab-discussion-item-footer {\n  padding: 0px; }\n\n.home-maintab-discussion-item-body p, .home-maintab-discussion-item-footer p {\n    margin: 5px 0;\n    padding: 2px 5px;\n    border-radius: 10px; }\n\n.home-maintab-discussion-item-body p {\n  color: #565656;\n  font-style: italic;\n  background-color: white; }\n\n.home-maintab-discussion-item-footer p {\n  text-align: right;\n  font-size: 6pt;\n  color: #2e556e;\n  background-color: white; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9uaHovRGVza3RvcC9Mb2tlc2gvbnZoZ2doL3NyYy9hcHAvcGFnZS9tYWlubWVudS9ob21lLW1haW4vaG9tZS1tYWluLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVJLDZCQUF1QixFQUFBOztBQUczQjtFQUNJLHFCQUFhO0VBR2IsdUJBQXVCO0VBRXZCLDJDQUEyQyxFQUFBOztBQU0vQztFQUNJLGtCQUFrQixFQUFBOztBQUl0QjtFQUNJLGNBQWM7RUFDZCw2QkFBNkIsRUFBQTs7QUFLakM7RUFDSSxlQUFlLEVBQUE7O0FBR25CO0VBRVEsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixjQUFjLEVBQUE7O0FBSXRCO0VBQ0kseUJBQXlCO0VBQ3pCLG1CQUFtQjtFQUNuQiwyQkFBMkI7RUFDM0IsdUJBQXVCO0VBQ3ZCLG1CQUFtQixFQUFBOztBQUd2QjtFQUNJLFlBQVk7RUFFWix1QkFBdUI7RUFDdkIsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixnQ0FBZ0MsRUFBQTs7QUFOcEM7SUFRUSxjQUFjLEVBQUE7O0FBSXRCO0VBQ0ksZ0JBQWdCLEVBQUE7O0FBSXBCO0VBQ0ksY0FBYztFQUNkLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsa0JBQWtCLEVBQUE7O0FBRXRCO0VBQ0ksa0JBQWtCO0VBQ2xCLFFBQVE7RUFDUixVQUFVO0VBQ1YsY0FBYyxFQUFBOztBQUVsQjtFQUNJLGFBQWEsRUFBQTs7QUFEakI7SUFHUSxPQUFPLEVBQUE7O0FBSWY7RUFDSSxnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLFdBQVcsRUFBQTs7QUFHZjtFQUNJLGlCQUFpQjtFQUNqQixjQUFjO0VBQ2QsYUFBYSxFQUFBOztBQUdqQjtFQUNJLE9BQU87RUFDUCxpQkFBaUIsRUFBQTs7QUFGckI7SUFJUSxjQUFjO0lBQ2QsY0FBYztJQUNkLGFBQWEsRUFBQTs7QUFJckI7RUFFUSxlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLGNBQWMsRUFBQTs7QUFJdEI7RUFDSSxrQkFBa0IsRUFBQTs7QUFEdEI7SUFJUSxVQUFVO0lBQ1YsWUFBWSxFQUFBOztBQUlwQjtFQUNJLFdBQVc7RUFDWCxhQUFhO0VBQ2IsZ0JBQWdCO0VBQ2hCLGtCQUFrQixFQUFBOztBQUp0QjtJQU1RLFdBQVc7SUFDWCxnQkFBZ0IsRUFBQTs7QUFJeEI7RUFDSSxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLFNBQVM7RUFDVCxPQUFPO0VBQ1AsV0FBVztFQUNYLG9DQUFvQyxFQUFBOztBQU54QztJQVFRLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsaUJBQWlCO0lBQ2pCLGNBQWMsRUFBQTs7QUFJdEI7RUFFSSxtQkFBbUI7RUFDbkIsMkJBQTJCO0VBQzNCLG1CQUFtQixFQUFBOztBQUl2QjtFQUNJLHdCQUF3QjtFQUN4Qix5QkFBeUI7RUFDekIsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakIsZUFBZSxFQUFBOztBQU5uQjtJQVFRLGNBQWMsRUFBQTs7QUFJdEI7RUFDSSxZQUFZLEVBQUE7O0FBRGhCO0lBR1EsYUFBYTtJQUNiLGdCQUFnQjtJQUNoQixtQkFBbUIsRUFBQTs7QUFJM0I7RUFFUSxjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLHVCQUF1QixFQUFBOztBQUkvQjtFQUVRLGlCQUFpQjtFQUNqQixjQUFjO0VBQ2QsY0FBYztFQUNkLHVCQUF1QixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZS9tYWlubWVudS9ob21lLW1haW4vaG9tZS1tYWluLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50LCBpb24taGVhZGVyIHtcbiAgICAvLyAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjQkNEOUU2O1xuICAgIC0taW9uLWJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xufVxuXG5pb24tdG9vbGJhciB7XG4gICAgLS1iYWNrZ3JvdW5kOiAjQkNEOUU2O1xuICAgIC8vIGhlaWdodDogNjBweDtcbiAgICAvLyBib3JkZXItcmFkaXVzOiAwcHggMHB4IDMwcHggMzBweDtcbiAgICBwYWRkaW5nOiAwcHggIWltcG9ydGFudDtcbiAgICAvLyAudG9vbGJhci1iYWNrZ3JvdW5ke1xuICAgIGJvcmRlci1yYWRpdXM6IDBweCAwcHggMzBweCAzMHB4ICFpbXBvcnRhbnQ7XG4gICAgLy8gfVxuICAgIFxufVxuXG5cbmlvbi10aXRsZSB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIC8vIG1hcmdpbjogMjZweDtcbn1cblxuaW9uLXRpdGxlID4gc3BhbiB7XG4gICAgY29sb3I6ICMwMEI0Rjk7XG4gICAgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zIEJvbGRcIjtcbn1cblxuXG5cbiNob21lLW1haW50YWItZmF2LWJlYWNoZXMtYm9keSB7XG4gICAgcGFkZGluZzogMCAxNnB4O1xufVxuXG4jaG9tZS1tYWludGFiLWhlYWRlciB7XG4gICAgcCB7XG4gICAgICAgIGZvbnQtc2l6ZTogMTRwdDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgIGNvbG9yOiAjNDI2Njc1O1xuICAgIH1cbn1cblxuLmhvbWUtbWFpbi10YWItd2VhdGhlci1yZXBvcnRzIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjVGNUY1O1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgYm94LXNoYWRvdzogMCAwIDhweCAjYTRhNGE0O1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG59XG5cbi5ob21lLW1haW4tdGFiLXdlYXRoZXItcmVwb3J0cy1sb2NhdGlvbiB7XG4gICAgcGFkZGluZzogNXB4O1xuICAgIC8vIGJhY2tncm91bmQtY29sb3I6ICNkYWRhZGE7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHggMTBweCAwcHggMHB4O1xuICAgIHNwYW4ge1xuICAgICAgICBjb2xvcjogIzJiMmIyYjtcbiAgICB9XG59XG5cbi5ob21lLW1haW4tdGFiLXdlYXRoZXItcmVwb3J0cy1ib2R5IHtcbiAgICBwYWRkaW5nOiAwcHggNXB4O1xuICAgIFxufVxuXG4uaG9tZS1tYWluLXRhYi13ZWF0aGVyLXJlcG9ydHMtcmVwb3J0dHh0IHtcbiAgICBjb2xvcjogIzYyNjI2MjtcbiAgICBmb250LXNpemU6IDEycHQ7XG4gICAgbWFyZ2luOiA1cHggMCAyMHB4O1xuICAgIGZvbnQtc3R5bGU6IGl0YWxpYztcbn1cbi5pY29uU3R5bGV7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogNXB4O1xuICAgIHJpZ2h0OiA1cHg7XG4gICAgei1pbmRleDogMTAwMDA7XG59XG4uaG9tZS1tYWluLXRhYi13ZWF0aGVyLXJlcG9ydHMtd2VhdGhlcmluZm8ge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZGl2IHtcbiAgICAgICAgZmxleDogMTtcbiAgICB9XG59XG5cbi5ob21lLW1haW4tdGFiLXdlYXRoZXItcmVwb3J0cy13YXZlaGVpZ2h0IHtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIGNvbG9yOiAjNjI2MjYyO1xuICAgIG1hcmdpbjogMnB4O1xufVxuXG4uaG9tZS1tYWluLXRhYi13ZWF0aGVyLXJlcG9ydHMtd2F2ZXF1YWxpdHkge1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIGNvbG9yOiAjNjI2MjYyO1xuICAgIG1hcmdpbjogNXB4IDA7XG59XG5cbi5ob21lLW1haW4tdGFiLXdlYXRoZXItcmVwb3J0cy1yZXBvcnR0aW1lIHtcbiAgICBmbGV4OiAxO1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIHAge1xuICAgICAgICBmb250LXNpemU6IDZwdDtcbiAgICAgICAgY29sb3I6ICMyZTU1NmU7XG4gICAgICAgIG1hcmdpbjogNXB4IDA7XG4gICAgfVxufVxuXG4jaG9tZS1tYWludGFiLXNob3AtcmVwb3J0cy1oZWFkZXIsICNob21lLW1haW50YWItZmF2LWJlYWNoaGVzLWhlYWRlciwgI2hvbWUtbWFpbnRhYi1kaXNjdXNzaW9ucy1oZWFkZXIge1xuICAgIHAge1xuICAgICAgICBmb250LXNpemU6IDE0cHQ7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICBjb2xvcjogIzQyNjY3NTtcbiAgICB9XG59XG5cbiNob21lLW1haW50YWItc2hvcC1yZXBvcnRzLWltZy1jb250YWluZXIge1xuICAgIG92ZXJmbG93LXg6IHNjcm9sbDtcbi8vICAgIGhlaWdodDo5MHB4O1xuICAgIGlvbi1jb2wge1xuICAgICAgICBwYWRkaW5nOiAwO1xuICAgICAgICBtYXJnaW46IDEwcHg7XG4gICAgfVxufVxuXG4uaG9tZS1tYWludGFiLXNob3AtcmVwb3J0cy1pdGVtaW1nIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEyMHB4O1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGltZyB7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBtaW4taGVpZ2h0OiAxMDAlO1xuICAgIH1cbn1cblxuLmhvbWUtbWFpbnRhYi1zaG9wLXJlcG9ydHMtaXRlbWltZy1kZXNjcmlwdGlvbiB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHotaW5kZXg6IDI7XG4gICAgYm90dG9tOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjQpO1xuICAgIHAge1xuICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgIGZvbnQtc2l6ZTogOXB0O1xuICAgIH1cbn1cblxuLmhvbWUtbWFpbnRhYi1kaXNjdXNzaW9uLWl0ZW0ge1xuICAgIC8vIGJhY2tncm91bmQtY29sb3I6ICNGNUY1RjU7XG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICBib3gtc2hhZG93OiAwIDAgOHB4ICNhNGE0YTQ7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBcbn1cblxuLmhvbWUtbWFpbnRhYi1kaXNjdXNzaW9uLWl0ZW0taGVhZGVyIHtcbiAgICBwYWRkaW5nOiA1cHggNXB4IDBweCA1cHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2RhZGFkYTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBzcGFuIHtcbiAgICAgICAgY29sb3I6ICMyYjJiMmI7XG4gICAgfVxufVxuXG4uaG9tZS1tYWludGFiLWRpc2N1c3Npb24taXRlbS1ib2R5LCAuaG9tZS1tYWludGFiLWRpc2N1c3Npb24taXRlbS1mb290ZXIge1xuICAgIHBhZGRpbmc6IDBweDtcbiAgICBwIHtcbiAgICAgICAgbWFyZ2luOiA1cHggMDtcbiAgICAgICAgcGFkZGluZzogMnB4IDVweDtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICB9XG59XG5cbi5ob21lLW1haW50YWItZGlzY3Vzc2lvbi1pdGVtLWJvZHkge1xuICAgIHAge1xuICAgICAgICBjb2xvcjogIzU2NTY1NjtcbiAgICAgICAgZm9udC1zdHlsZTogaXRhbGljO1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICB9XG59XG5cbi5ob21lLW1haW50YWItZGlzY3Vzc2lvbi1pdGVtLWZvb3RlciB7XG4gICAgcCB7XG4gICAgICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgICAgICBmb250LXNpemU6IDZwdDtcbiAgICAgICAgY29sb3I6ICMyZTU1NmU7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAgIH1cbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/page/mainmenu/home-main/home-main.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/page/mainmenu/home-main/home-main.page.ts ***!
  \***********************************************************/
/*! exports provided: HomeMainPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeMainPage", function() { return HomeMainPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_firebase_service_firebase_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/firebase-service/firebase.service */ "./src/app/services/firebase-service/firebase.service.ts");
/* harmony import */ var src_app_constants_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/constants/constants */ "./src/app/constants/constants.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_myutils_myutils_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/myutils/myutils.service */ "./src/app/services/myutils/myutils.service.ts");
/* harmony import */ var _modal_gallery_gallery_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../modal/gallery/gallery.page */ "./src/app/page/modal/gallery/gallery.page.ts");
/* harmony import */ var _ionic_native_streaming_media_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/streaming-media/ngx */ "./node_modules/@ionic-native/streaming-media/ngx/index.js");









var HomeMainPage = /** @class */ (function () {
    function HomeMainPage(firebaseService, router, event, loadingCtrl, myUtils, modalCtrl, streamingMedia) {
        var _this = this;
        this.firebaseService = firebaseService;
        this.router = router;
        this.event = event;
        this.loadingCtrl = loadingCtrl;
        this.myUtils = myUtils;
        this.modalCtrl = modalCtrl;
        this.streamingMedia = streamingMedia;
        this.currentUserId = '';
        this.arrWaveQualities = src_app_constants_constants__WEBPACK_IMPORTED_MODULE_4__["WAVEQUALITIES"];
        /**
         * Weather Reports
         */
        this.weatherReportdataRef = null;
        this.weatherReportdataListener = null;
        this.readmore = false;
        this.readmoreClicked = false;
        /**
         * Board Reports
         */
        this.boardDataRef = null;
        this.boardDataListener = null;
        this.allNewSurfBoards = [];
        /**
         * Recent Discussions
         */
        this.discussionDetailRef = null;
        this.discussionDetailListener = null;
        this.arrDiscussions = [];
        this.arrDiscussionsFilter = [];
        /**
         * User Info
         */
        this.userInfo = {};
        this.arrFavBeachesId = [];
        /**
         * Event Listener
         */
        this.authListener = null;
        this.countLoader = 0;
        this.dataLoader = null;
        this.authListener = this.event.subscribe('onAuth', function (data) {
            console.log('Auth Called!');
            _this.initPage();
        });
    }
    HomeMainPage.prototype.ngOnInit = function () {
    };
    HomeMainPage.prototype.ionViewWillEnter = function () {
        /**
         * Init page
         */
        this.initPage();
    };
    HomeMainPage.prototype.initPage = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _a;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0:
                        this.countLoader = 0;
                        this.currentUserId = this.firebaseService.getUID();
                        /**
                         * Weather
                         */
                        this.weatherReportdataRef = null;
                        this.weatherReportdataListener = null;
                        this.weatherReports = [];
                        /**
                         * Board
                         */
                        this.boardDataRef = null;
                        this.boardDataListener = null;
                        this.allNewSurfBoards = [];
                        /**
                         * Discussion
                         */
                        this.discussionDetailRef = null;
                        this.discussionDetailListener = null;
                        return [4 /*yield*/, this.getUserInfo()];
                    case 1:
                        _b.sent();
                        _a = this;
                        return [4 /*yield*/, this.loadingCtrl.create({
                                message: 'Please wait...'
                            })];
                    case 2:
                        _a.dataLoader = _b.sent();
                        return [4 /*yield*/, this.dataLoader.present()];
                    case 3:
                        _b.sent();
                        this.getAllLocationReports();
                        this.getNewBoardItems();
                        this.getDiscussionContents();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomeMainPage.prototype.getUserInfo = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var userInfoSnapshot;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.firebaseService.getUserProfile(this.currentUserId)];
                    case 1:
                        userInfoSnapshot = _a.sent();
                        this.userInfo = userInfoSnapshot.val();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomeMainPage.prototype.getAllLocationReports = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var self;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                self = this;
                this.arrFavBeachesId = this.userInfo.favorite.beaches.split(',');
                this.weatherReportdataRef = this.firebaseService.getAllWeatherReports();
                this.weatherReportdataListener = this.weatherReportdataRef.on('value', function (snapshot) {
                    self.weatherReports = [];
                    var tmpArr = [];
                    _this.arrFavBeachesId.forEach(function (element) {
                        snapshot.forEach(function (childSnapshot) {
                            var favIndex = (childSnapshot.key === element);
                            if (favIndex) {
                                childSnapshot.forEach(function (valueSnapshot) {
                                    tmpArr.push(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, valueSnapshot.val()));
                                });
                            }
                        });
                    });
                    tmpArr.reverse();
                    // console.log(tmpArr);
                    var tempArray2 = [];
                    // if(tmpArr.length>4){
                    tmpArr.forEach(function (item) {
                        var i = tempArray2.findIndex(function (x) { return x.location.name == item.location.name && x.locationId == item.locationId; });
                        if (i <= -1) {
                            tempArray2.push(item);
                        }
                    });
                    // }  
                    //   console.log(tempArray2);     
                    //  if(tempArray2.length<4){  
                    //   tmpArr.forEach(function(item){
                    //     var i = tempArray2.findIndex(x => (x.id == item.id && x.locationId == item.locationId));
                    //     if(i <= -1){
                    //       tempArray2.push(item);
                    //     }
                    //   });
                    //  }
                    //  console.log(tempArray2.length);
                    if (tempArray2.length >= 4) {
                        self.weatherReports = tempArray2.sort(function (a, b) { return b.createdat - a.createdat; }).slice(0, 4);
                        _this.readmore = (tempArray2.length > 4 ? true : false);
                    }
                    else {
                        self.weatherReports = tempArray2.slice(0, tempArray2.length).sort(function (a, b) { return b.createdat - a.createdat; });
                    }
                    self.weatherReportsFilter = tempArray2.sort(function (a, b) { return b.createdat - a.createdat; });
                    // tmpArr;     
                    console.log(tmpArr);
                    console.log(self.weatherReports);
                    self.countLoader++;
                    if (self.countLoader == 2) {
                        self.dismissDataLoader();
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    HomeMainPage.prototype.onClickVideoAttachment = function (ev, path, type) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var galleryModal, streamingOptions;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ev.stopPropagation();
                        if (!(type == 0)) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.modalCtrl.create({
                                component: _modal_gallery_gallery_page__WEBPACK_IMPORTED_MODULE_7__["GalleryPage"],
                                componentProps: {
                                    imgs: [path]
                                },
                                cssClass: 'gallery-modal'
                            })];
                    case 1:
                        galleryModal = _a.sent();
                        return [4 /*yield*/, galleryModal.present()];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        streamingOptions = {
                            successCallback: function () {
                                console.log('Video played');
                            },
                            errorCallback: function (e) {
                                console.log('Error streaming');
                            },
                            orientation: 'portrait',
                            shouldAutoClose: true,
                            controls: false
                        };
                        this.streamingMedia.playVideo(path, streamingOptions);
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    HomeMainPage.prototype.getNewBoardItems = function () {
        var self = this;
        // const itemRange = Number(this.userInfo.noti.range);
        this.boardDataRef = this.firebaseService.getAllSurfBoardRealtime();
        this.boardDataListener = this.boardDataRef.on('value', function (snapshot) {
            self.allNewSurfBoards = [];
            snapshot.forEach(function (boardSnapshot) {
                var distance = self.myUtils.distance(self.userInfo.location.lat, self.userInfo.location.long, boardSnapshot.val().location.lat, boardSnapshot.val().location.long, 'N');
                // if (itemRange >= Number(distance)) {
                self.allNewSurfBoards.push(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, boardSnapshot.val()));
                // }
            });
            self.countLoader++;
            if (self.countLoader == 2) {
                self.dismissDataLoader();
            }
            self.allNewSurfBoards = self.allNewSurfBoards.sort(function (a, b) { return (b.createdat - a.createdat); });
        });
    };
    HomeMainPage.prototype.setDefault = function (item) {
        console.log(item);
        localStorage.setItem("defaultBeach", JSON.stringify(item));
    };
    HomeMainPage.prototype.getDiscussionContents = function () {
        var _this = this;
        var self = this;
        var favDiscussions = [];
        if (this.userInfo.favorite.discussions) {
            favDiscussions = this.userInfo.favorite.discussions.split(',');
        }
        this.discussionDetailRef = this.firebaseService.getAllDiscussionContents();
        this.discussionDetailListener = this.discussionDetailRef.on('value', function (snapshot) {
            _this.arrDiscussions = [];
            _this.arrDiscussionsFilter = [];
            snapshot.forEach(function (childSnapshot) {
                var isFavDiscussion = favDiscussions.indexOf(childSnapshot.key);
                if (isFavDiscussion > -1) {
                    var tmpArr_1 = [];
                    childSnapshot.forEach(function (valSnapshot) {
                        tmpArr_1.push(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, valSnapshot.val(), { discussionid: childSnapshot.key }));
                    });
                    if (tmpArr_1.length > 0) {
                        self.arrDiscussions.push(tmpArr_1[tmpArr_1.length - 1]);
                    }
                }
            });
            if (_this.arrDiscussions.length > 4) {
                _this.arrDiscussionsFilter = _this.arrDiscussions.slice(0, 4);
            }
            else {
                _this.arrDiscussionsFilter = _this.arrDiscussions;
            }
            _this.arrDiscussionsFilter.sort(function (x, y) {
                return y.createdat - x.createdat;
            });
        });
    };
    HomeMainPage.prototype.onClickDiscussionItem = function (item) {
        this.router.navigateByUrl('/home/main/discussion-detail/' + item.discussionid + '/' + item.title + '/home/1');
    };
    HomeMainPage.prototype.showMore = function (val) {
        if (val == 1) {
            this.arrDiscussionsFilter = this.arrDiscussions;
        }
        else {
            this.arrDiscussionsFilter = this.arrDiscussions.slice(0, 4);
        }
        this.arrDiscussionsFilter.sort(function (x, y) {
            return y.createdat - x.createdat;
        });
    };
    HomeMainPage.prototype.dismissDataLoader = function () {
        if (this.dataLoader !== null) {
            this.dataLoader.dismiss();
            this.dataLoader = null;
        }
    };
    HomeMainPage.prototype.show = function (val) {
        this.readmoreClicked = (val == 1 ? true : false);
    };
    HomeMainPage.prototype.viewItemDetail = function (itemid) {
        sessionStorage.setItem("fromHome", "1");
        this.router.navigateByUrl('/home/main/shopitemdetail/' + this.allNewSurfBoards[itemid].id + '/main');
    };
    HomeMainPage.prototype.onClickReportItem = function (report) {
        console.log("Check index", report);
        this.router.navigateByUrl('/home/main/locationdetail/' + report.locationId + '/' + btoa(report.location.name));
    };
    HomeMainPage.prototype.onClickFavBeach = function (beachIndex) {
        this.router.navigateByUrl('/locationdetail/' + this.arrFavBeachesId[beachIndex]);
    };
    HomeMainPage.prototype.ionViewWillLeave = function () {
        if (this.weatherReportdataListener !== null) {
            this.weatherReportdataRef.off('value', this.weatherReportdataListener);
            this.weatherReportdataListener = null;
            this.weatherReportdataRef = null;
        }
        if (this.boardDataListener !== null) {
            this.boardDataRef.off('value', this.boardDataListener);
            this.boardDataListener = null;
            this.boardDataRef = null;
        }
        if (this.authListener !== null) {
            this.event.unsubscribe('onAuth', this.authListener);
        }
    };
    HomeMainPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home-main',
            template: __webpack_require__(/*! ./home-main.page.html */ "./src/app/page/mainmenu/home-main/home-main.page.html"),
            styles: [__webpack_require__(/*! ./home-main.page.scss */ "./src/app/page/mainmenu/home-main/home-main.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_firebase_service_firebase_service__WEBPACK_IMPORTED_MODULE_3__["FirebaseService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Events"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"],
            src_app_services_myutils_myutils_service__WEBPACK_IMPORTED_MODULE_6__["MyutilsService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"],
            _ionic_native_streaming_media_ngx__WEBPACK_IMPORTED_MODULE_8__["StreamingMedia"]])
    ], HomeMainPage);
    return HomeMainPage;
}());



/***/ }),

/***/ "./src/app/page/modal/gallery/gallery.page.html":
/*!******************************************************!*\
  !*** ./src/app/page/modal/gallery/gallery.page.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content fullscreen padding>\n  <div id=\"gallery-modal-btnclose\">\n    <ion-icon name=\"close\" (click)=\"closeGalleryModal()\"></ion-icon>\n  </div>\n  <ion-slides [options]=\"sliderOpts\">\n    <ion-slide *ngFor=\"let img of imgs\">\n      <div class=\"swiper-zoom-container\">\n        <img [src]=\"img\" />\n      </div>\n    </ion-slide>\n  </ion-slides>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/page/modal/gallery/gallery.page.scss":
/*!******************************************************!*\
  !*** ./src/app/page/modal/gallery/gallery.page.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background: rgba(19, 19, 19, 0.45); }\n\n#gallery-modal-btnclose {\n  width: 100%;\n  text-align: right; }\n\n#gallery-modal-btnclose ion-icon {\n    font-size: 24pt;\n    color: white; }\n\nion-slides {\n  height: 80%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9uaHovRGVza3RvcC9Mb2tlc2gvbnZoZ2doL3NyYy9hcHAvcGFnZS9tb2RhbC9nYWxsZXJ5L2dhbGxlcnkucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksb0NBQWEsRUFBQTs7QUFHakI7RUFDSSxXQUFXO0VBQ1gsaUJBQWlCLEVBQUE7O0FBRnJCO0lBSVEsZUFBZTtJQUNmLFlBQVksRUFBQTs7QUFJcEI7RUFDSSxXQUFXLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlL21vZGFsL2dhbGxlcnkvZ2FsbGVyeS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XG4gICAgLS1iYWNrZ3JvdW5kOiByZ2JhKDE5LCAxOSwgMTksIDAuNDUpO1xufVxuXG4jZ2FsbGVyeS1tb2RhbC1idG5jbG9zZSB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgaW9uLWljb24ge1xuICAgICAgICBmb250LXNpemU6IDI0cHQ7XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB9XG59XG5cbmlvbi1zbGlkZXMge1xuICAgIGhlaWdodDogODAlO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/page/modal/gallery/gallery.page.ts":
/*!****************************************************!*\
  !*** ./src/app/page/modal/gallery/gallery.page.ts ***!
  \****************************************************/
/*! exports provided: GalleryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GalleryPage", function() { return GalleryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var GalleryPage = /** @class */ (function () {
    function GalleryPage(modalCtrl) {
        this.modalCtrl = modalCtrl;
        this.sliderOpts = {
            zoom: {
                maxRatio: 3
            }
        };
    }
    GalleryPage.prototype.ngOnInit = function () {
    };
    GalleryPage.prototype.closeGalleryModal = function () {
        this.modalCtrl.dismiss();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('imgs'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GalleryPage.prototype, "imgs", void 0);
    GalleryPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-gallery',
            template: __webpack_require__(/*! ./gallery.page.html */ "./src/app/page/modal/gallery/gallery.page.html"),
            styles: [__webpack_require__(/*! ./gallery.page.scss */ "./src/app/page/modal/gallery/gallery.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
    ], GalleryPage);
    return GalleryPage;
}());



/***/ })

}]);
//# sourceMappingURL=home-main-home-main-module.js.map