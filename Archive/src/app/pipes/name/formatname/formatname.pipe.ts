import { Pipe, PipeTransform } from '@angular/core';
import { isNull } from 'util';

@Pipe({
  name: 'formatname'
})
export class FormatnamePipe implements PipeTransform {

  transform(value: any): any {
    let result = "";

    if(value.trim() !== '') {
     
      let arrTmpName = value.trim().split(" ");
      console.log(arrTmpName);
      if(arrTmpName.length > 0) {
        result = arrTmpName[0] + " " + arrTmpName[1].substring(0, 1);
      } else {
        result = arrTmpName[0];
      }
    }
    return result;
  }

}
