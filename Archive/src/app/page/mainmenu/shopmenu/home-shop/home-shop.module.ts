import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HomeShopPage } from './home-shop.page';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { ActivitypopoverPage } from '../../activitymenu/activitypopover/activitypopover.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PipesModule,
    RouterModule.forChild([{ path: '', component: HomeShopPage }])
  ],
  declarations: [HomeShopPage,ActivitypopoverPage],
  entryComponents: [ActivitypopoverPage]
})
export class HomeShopPageModule {}
