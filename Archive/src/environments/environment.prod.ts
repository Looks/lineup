export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCbRIt7U0jKT-dTGoMq7wvGH9TIlRrnICc",
    authDomain: "lineup-2300.firebaseapp.com",
    databaseURL: "https://lineup-2300.firebaseio.com",
    projectId: "lineup-2300",
    storageBucket: "lineup-2300.appspot.com",
    messagingSenderId: "1025568450201",
    appId: "1:1025568450201:web:8a0a2f1ccadfe763"
  }
};
